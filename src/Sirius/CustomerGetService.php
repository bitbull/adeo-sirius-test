<?php
/**
 * Proxy for CustomerGetService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerGetService as CustomerGetServiceBase;
use \Adeo\Sirius\CustomerGetService\Type\GetCustomer;
use \Adeo\Sirius\CustomerGetService\Type\GetSegmentations;
use \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiers;
use \Adeo\Sirius\CustomerGetService\Type\GetChildren;
use \Adeo\Sirius\CustomerGetService\Type\GetCommunities;
use \Adeo\Sirius\CustomerGetService\Type\GetNotePads;
use \Adeo\Sirius\CustomerGetService\Type\GetLeisures;
use \Adeo\Sirius\CustomerGetService\Type\GetUsagePreference;
use \Adeo\Sirius\CustomerGetService\Type\GetSlaveMaster;
use \Adeo\Sirius\CustomerGetService\Type\GetHousehold;
use \Adeo\Sirius\CustomerGetService\Type\CustomerIdentificationDTO;

class CustomerGetService
    extends CustomerGetServiceBase
{
    
    /**
     * Method getCustomer
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\CustomerIdentificationDTO $identification Identification model
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\CustomerOutDTO|null
     */
    public function getCustomer(CustomerIdentificationDTO $identification)
    {
        $getCustomer = new GetCustomer();
        $getCustomer->setArg0($identification);
        $response = parent::_getCustomer($getCustomer);
        return $response->getReturn();
    }
    
    /**
     * Method getSegmentations
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetSegmentations $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetSegmentationsResponse
     */
    public function getSegmentations(GetSegmentations $parameters)
    {
        $response = parent::_getSegmentations($parameters);
        return $response;
    }
    
    /**
     * Method getExternalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiersResponse
     */
    public function getExternalIdentifiers(GetExternalIdentifiers $parameters)
    {
        $response = parent::_getExternalIdentifiers($parameters);
        return $response;
    }
    
    /**
     * Method getChildren
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetChildren $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetChildrenResponse
     */
    public function getChildren(GetChildren $parameters)
    {
        $response = parent::_getChildren($parameters);
        return $response;
    }
    
    /**
     * Method getCommunities
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetCommunities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetCommunitiesResponse
     */
    public function getCommunities(GetCommunities $parameters)
    {
        $response = parent::_getCommunities($parameters);
        return $response;
    }
    
    /**
     * Method getNotePads
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetNotePads $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetNotePadsResponse
     */
    public function getNotePads(GetNotePads $parameters)
    {
        $response = parent::_getNotePads($parameters);
        return $response;
    }
    
    /**
     * Method getLeisures
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetLeisures $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetLeisuresResponse
     */
    public function getLeisures(GetLeisures $parameters)
    {
        $response = parent::_getLeisures($parameters);
        return $response;
    }
    
    /**
     * Method getUsagePreference
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetUsagePreference $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetUsagePreferenceResponse
     */
    public function getUsagePreference(GetUsagePreference $parameters)
    {
        $response = parent::_getUsagePreference($parameters);
        return $response;
    }
    
    /**
     * Method getSlaveMaster
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetSlaveMaster $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetSlaveMasterResponse
     */
    public function getSlaveMaster(GetSlaveMaster $parameters)
    {
        $response = parent::_getSlaveMaster($parameters);
        return $response;
    }
    
    /**
     * Method getHousehold
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetHouseholdResponse
     */
    public function getHousehold(GetHousehold $parameters)
    {
        $response = parent::_getHousehold($parameters);
        return $response;
    }
}
