<?php
/**
 * Model for GetCommunitiesResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\GetCommunitiesResponse as GetCommunitiesResponseBase;

class GetCommunitiesResponse
    extends getCommunitiesResponseBase
{
}
