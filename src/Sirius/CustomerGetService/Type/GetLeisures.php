<?php
/**
 * Model for GetLeisures
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\GetLeisures as GetLeisuresBase;

class GetLeisures
    extends getLeisuresBase
{
}
