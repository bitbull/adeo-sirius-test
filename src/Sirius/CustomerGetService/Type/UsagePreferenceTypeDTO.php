<?php
/**
 * Model for UsagePreferenceTypeDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceTypeDTO as UsagePreferenceTypeDTOBase;

class UsagePreferenceTypeDTO
    extends usagePreferenceTypeDTOBase
{
}
