<?php
/**
 * Model for CustomerInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO as CustomerInformationOutDTOBase;

class CustomerInformationOutDTO
    extends customerInformationOutDTOBase
{
}
