<?php
/**
 * Model for CustomerHousingOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO as CustomerHousingOutDTOBase;

class CustomerHousingOutDTO
    extends customerHousingOutDTOBase
{
}
