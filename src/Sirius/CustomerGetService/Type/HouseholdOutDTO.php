<?php
/**
 * Model for HouseholdOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO as HouseholdOutDTOBase;

class HouseholdOutDTO
    extends householdOutDTOBase
{
}
