<?php
/**
 * Model for UsagePreferenceOtherOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOtherOutDTO as UsagePreferenceOtherOutDTOBase;

class UsagePreferenceOtherOutDTO
    extends usagePreferenceOtherOutDTOBase
{
}
