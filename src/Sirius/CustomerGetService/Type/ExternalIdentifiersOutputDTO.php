<?php
/**
 * Model for ExternalIdentifiersOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type;

use \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO as ExternalIdentifiersOutputDTOBase;

class ExternalIdentifiersOutputDTO
    extends externalIdentifiersOutputDTOBase
{
}
