<?php
/**
 * Parent model for SlaveMasterDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class SlaveMasterDTO
{
    
    /**
     * SlaveIdentifier
     * 
     * @var int
     */
    public $slaveIdentifier;
    
    /**
     * MasterIdentifier
     * 
     * @var int
     */
    public $masterIdentifier;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get slaveIdentifier
     * 
     * @return int
     */
    public function getSlaveIdentifier()
    {
        return $this->slaveIdentifier;
    }
    
    /**
     * Set slaveIdentifier
     * 
     * @param int $value slaveIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO
     */
    public function setSlaveIdentifier($value)
    {
        $this->slaveIdentifier = $value;
        return $this;
    }
    
    /**
     * Get masterIdentifier
     * 
     * @return int
     */
    public function getMasterIdentifier()
    {
        return $this->masterIdentifier;
    }
    
    /**
     * Set masterIdentifier
     * 
     * @param int $value masterIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO
     */
    public function setMasterIdentifier($value)
    {
        $this->masterIdentifier = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
