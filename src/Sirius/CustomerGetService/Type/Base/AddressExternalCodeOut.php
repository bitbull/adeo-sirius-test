<?php
/**
 * Parent model for AddressExternalCodeOut
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class AddressExternalCodeOut
{
    
    /**
     * Code1
     * 
     * @var string
     */
    public $code1;
    
    /**
     * Code2
     * 
     * @var string
     */
    public $code2;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * Get code1
     * 
     * @return string
     */
    public function getCode1()
    {
        return $this->code1;
    }
    
    /**
     * Set code1
     * 
     * @param string $value code1
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut
     */
    public function setCode1($value)
    {
        $this->code1 = $value;
        return $this;
    }
    
    /**
     * Get code2
     * 
     * @return string
     */
    public function getCode2()
    {
        return $this->code2;
    }
    
    /**
     * Set code2
     * 
     * @param string $value code2
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut
     */
    public function setCode2($value)
    {
        $this->code2 = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
}
