<?php
/**
 * Parent model for SpecificOptinOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class SpecificOptinOutDTO
{
    
    /**
     * Value
     * 
     * @var boolean
     */
    public $value;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * UpdateDate
     * 
     * @var string
     */
    public $updateDate;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get value
     * 
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param boolean $value value
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get updateDate
     * 
     * @return string
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
    
    /**
     * Set updateDate
     * 
     * @param string $value updateDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO
     */
    public function setUpdateDate($value)
    {
        $this->updateDate = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
