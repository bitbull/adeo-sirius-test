<?php
/**
 * Parent model for HouseholdOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class HouseholdOutDTO
{
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * NaturalPersonCustomers collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO[]
     */
    public $naturalPersonCustomers = array();
    
    /**
     * Addresses collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[]
     */
    public $addresses = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get naturalPersonCustomers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO[]
     */
    public function getNaturalPersonCustomers()
    {
        return $this->naturalPersonCustomers;
    }
    
    /**
     * Add element on naturalPersonCustomers collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO[] $value naturalPersonCustomers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public function setNaturalPersonCustomers($value = null)
    {
        $this->naturalPersonCustomers[] = $value;
        return $this;
    }
    
    /**
     * Get addresses
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    
    /**
     * Add element on addresses collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[] $value addresses
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public function setAddresses($value = null)
    {
        $this->addresses[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
