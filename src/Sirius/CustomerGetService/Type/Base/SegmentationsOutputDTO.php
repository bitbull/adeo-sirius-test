<?php
/**
 * Parent model for SegmentationsOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class SegmentationsOutputDTO
{
    
    /**
     * Segmentations collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[]
     */
    public $segmentations = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get segmentations
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[]
     */
    public function getSegmentations()
    {
        return $this->segmentations;
    }
    
    /**
     * Add element on segmentations collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[] $value segmentations
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO
     */
    public function setSegmentations($value = null)
    {
        $this->segmentations[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
