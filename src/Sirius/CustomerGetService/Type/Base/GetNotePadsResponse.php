<?php
/**
 * Parent model for GetNotePadsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetNotePadsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetNotePadsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
