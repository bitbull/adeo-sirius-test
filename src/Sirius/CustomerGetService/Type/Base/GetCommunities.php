<?php
/**
 * Parent model for GetCommunities
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetCommunities
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerIdentificationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerIdentificationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerIdentificationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetCommunities
     */
    public function setArg0(\Adeo\Sirius\CustomerGetService\Type\Base\CustomerIdentificationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
