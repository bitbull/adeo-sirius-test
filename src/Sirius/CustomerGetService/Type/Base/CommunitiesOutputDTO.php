<?php
/**
 * Parent model for CommunitiesOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CommunitiesOutputDTO
{
    
    /**
     * Communities collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunityInformationOutDTO[]
     */
    public $communities = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunityInformationOutDTO[]
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Add element on communities collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunityInformationOutDTO[] $value communities
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO
     */
    public function setCommunities($value = null)
    {
        $this->communities[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
