<?php
/**
 * Parent model for GlobalOptInInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GlobalOptInInformationOutDTO
{
    
    /**
     * OptInValue
     * 
     * @var boolean
     */
    public $optInValue;
    
    /**
     * OptInTypeCode
     * 
     * @var int
     */
    public $optInTypeCode;
    
    /**
     * OptInTypeLabel
     * 
     * @var string
     */
    public $optInTypeLabel;
    
    /**
     * GlobalCommunicationTypeOptin
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO
     */
    public $globalCommunicationTypeOptin;
    
    /**
     * UpdateDate
     * 
     * @var string
     */
    public $updateDate;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get optInValue
     * 
     * @return boolean
     */
    public function getOptInValue()
    {
        return $this->optInValue;
    }
    
    /**
     * Set optInValue
     * 
     * @param boolean $value optInValue
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setOptInValue($value)
    {
        $this->optInValue = $value;
        return $this;
    }
    
    /**
     * Get optInTypeCode
     * 
     * @return int
     */
    public function getOptInTypeCode()
    {
        return $this->optInTypeCode;
    }
    
    /**
     * Set optInTypeCode
     * 
     * @param int $value optInTypeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setOptInTypeCode($value)
    {
        $this->optInTypeCode = $value;
        return $this;
    }
    
    /**
     * Get optInTypeLabel
     * 
     * @return string
     */
    public function getOptInTypeLabel()
    {
        return $this->optInTypeLabel;
    }
    
    /**
     * Set optInTypeLabel
     * 
     * @param string $value optInTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setOptInTypeLabel($value)
    {
        $this->optInTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get globalCommunicationTypeOptin
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO
     */
    public function getGlobalCommunicationTypeOptin()
    {
        return $this->globalCommunicationTypeOptin;
    }
    
    /**
     * Set globalCommunicationTypeOptin
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO $value globalCommunicationTypeOptin
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setGlobalCommunicationTypeOptin(\Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO $value)
    {
        $this->globalCommunicationTypeOptin = $value;
        return $this;
    }
    
    /**
     * Get updateDate
     * 
     * @return string
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
    
    /**
     * Set updateDate
     * 
     * @param string $value updateDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setUpdateDate($value)
    {
        $this->updateDate = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
