<?php
/**
 * Parent model for GetSlaveMasterResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetSlaveMasterResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SlaveMasterDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetSlaveMasterResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
