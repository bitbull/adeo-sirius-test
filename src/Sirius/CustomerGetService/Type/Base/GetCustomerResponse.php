<?php
/**
 * Parent model for GetCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetCustomerResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetCustomerResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
