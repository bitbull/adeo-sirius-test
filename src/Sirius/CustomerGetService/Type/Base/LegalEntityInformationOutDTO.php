<?php
/**
 * Parent model for LegalEntityInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class LegalEntityInformationOutDTO
{
    
    /**
     * CompanyName
     * 
     * @var string
     */
    public $companyName;
    
    /**
     * LegalEntityCustomerNumber
     * 
     * @var int
     */
    public $legalEntityCustomerNumber;
    
    /**
     * Active
     * 
     * @var boolean
     */
    public $active;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get companyName
     * 
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    
    /**
     * Set companyName
     * 
     * @param string $value companyName
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public function setCompanyName($value)
    {
        $this->companyName = $value;
        return $this;
    }
    
    /**
     * Get legalEntityCustomerNumber
     * 
     * @return int
     */
    public function getLegalEntityCustomerNumber()
    {
        return $this->legalEntityCustomerNumber;
    }
    
    /**
     * Set legalEntityCustomerNumber
     * 
     * @param int $value legalEntityCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public function setLegalEntityCustomerNumber($value)
    {
        $this->legalEntityCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set active
     * 
     * @param boolean $value active
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
