<?php
/**
 * Parent model for GetCommunitiesResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetCommunitiesResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetCommunitiesResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\CommunitiesOutputDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
