<?php
/**
 * Parent model for UsagePreferenceOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class UsagePreferenceOutDTO
{
    
    /**
     * Address
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\AddressDetailDTO
     */
    public $address;
    
    /**
     * Communication
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public $communication;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Description
     * 
     * @var string
     */
    public $description;
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Other
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOtherOutDTO
     */
    public $other;
    
    /**
     * PreferenceType
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceTypeDTO
     */
    public $preferenceType;
    
    /**
     * StateAlpha
     * 
     * @var string
     */
    public $stateAlpha;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Value
     * 
     * @var int
     */
    public $value;
    
    /**
     * Get address
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressDetailDTO
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set address
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\AddressDetailDTO $value address
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setAddress(\Adeo\Sirius\CustomerGetService\Type\Base\AddressDetailDTO $value)
    {
        $this->address = $value;
        return $this;
    }
    
    /**
     * Get communication
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function getCommunication()
    {
        return $this->communication;
    }
    
    /**
     * Set communication
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO $value communication
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setCommunication(\Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO $value)
    {
        $this->communication = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description
     * 
     * @param string $value description
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get other
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOtherOutDTO
     */
    public function getOther()
    {
        return $this->other;
    }
    
    /**
     * Set other
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOtherOutDTO $value other
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setOther(\Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOtherOutDTO $value)
    {
        $this->other = $value;
        return $this;
    }
    
    /**
     * Get preferenceType
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceTypeDTO
     */
    public function getPreferenceType()
    {
        return $this->preferenceType;
    }
    
    /**
     * Set preferenceType
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceTypeDTO $value preferenceType
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setPreferenceType(\Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceTypeDTO $value)
    {
        $this->preferenceType = $value;
        return $this;
    }
    
    /**
     * Get stateAlpha
     * 
     * @return string
     */
    public function getStateAlpha()
    {
        return $this->stateAlpha;
    }
    
    /**
     * Set stateAlpha
     * 
     * @param string $value stateAlpha
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setStateAlpha($value)
    {
        $this->stateAlpha = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param int $value value
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
