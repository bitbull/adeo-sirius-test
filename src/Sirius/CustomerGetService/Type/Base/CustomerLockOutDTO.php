<?php
/**
 * Parent model for CustomerLockOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CustomerLockOutDTO
{
    
    /**
     * LockCreationDate
     * 
     * @var string
     */
    public $lockCreationDate;
    
    /**
     * LockUpdateDate
     * 
     * @var string
     */
    public $lockUpdateDate;
    
    /**
     * LockEndDate
     * 
     * @var string
     */
    public $lockEndDate;
    
    /**
     * ActionDetailLockedCode
     * 
     * @var int
     */
    public $actionDetailLockedCode;
    
    /**
     * ActionDetailLockedLabel
     * 
     * @var string
     */
    public $actionDetailLockedLabel;
    
    /**
     * Application
     * 
     * @var string
     */
    public $application;
    
    /**
     * Deletable
     * 
     * @var boolean
     */
    public $deletable;
    
    /**
     * Modifiable
     * 
     * @var boolean
     */
    public $modifiable;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get lockCreationDate
     * 
     * @return string
     */
    public function getLockCreationDate()
    {
        return $this->lockCreationDate;
    }
    
    /**
     * Set lockCreationDate
     * 
     * @param string $value lockCreationDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setLockCreationDate($value)
    {
        $this->lockCreationDate = $value;
        return $this;
    }
    
    /**
     * Get lockUpdateDate
     * 
     * @return string
     */
    public function getLockUpdateDate()
    {
        return $this->lockUpdateDate;
    }
    
    /**
     * Set lockUpdateDate
     * 
     * @param string $value lockUpdateDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setLockUpdateDate($value)
    {
        $this->lockUpdateDate = $value;
        return $this;
    }
    
    /**
     * Get lockEndDate
     * 
     * @return string
     */
    public function getLockEndDate()
    {
        return $this->lockEndDate;
    }
    
    /**
     * Set lockEndDate
     * 
     * @param string $value lockEndDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setLockEndDate($value)
    {
        $this->lockEndDate = $value;
        return $this;
    }
    
    /**
     * Get actionDetailLockedCode
     * 
     * @return int
     */
    public function getActionDetailLockedCode()
    {
        return $this->actionDetailLockedCode;
    }
    
    /**
     * Set actionDetailLockedCode
     * 
     * @param int $value actionDetailLockedCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setActionDetailLockedCode($value)
    {
        $this->actionDetailLockedCode = $value;
        return $this;
    }
    
    /**
     * Get actionDetailLockedLabel
     * 
     * @return string
     */
    public function getActionDetailLockedLabel()
    {
        return $this->actionDetailLockedLabel;
    }
    
    /**
     * Set actionDetailLockedLabel
     * 
     * @param string $value actionDetailLockedLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setActionDetailLockedLabel($value)
    {
        $this->actionDetailLockedLabel = $value;
        return $this;
    }
    
    /**
     * Get application
     * 
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set application
     * 
     * @param string $value application
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setApplication($value)
    {
        $this->application = $value;
        return $this;
    }
    
    /**
     * Get deletable
     * 
     * @return boolean
     */
    public function getDeletable()
    {
        return $this->deletable;
    }
    
    /**
     * Set deletable
     * 
     * @param boolean $value deletable
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setDeletable($value)
    {
        $this->deletable = $value;
        return $this;
    }
    
    /**
     * Get modifiable
     * 
     * @return boolean
     */
    public function getModifiable()
    {
        return $this->modifiable;
    }
    
    /**
     * Set modifiable
     * 
     * @param boolean $value modifiable
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setModifiable($value)
    {
        $this->modifiable = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
