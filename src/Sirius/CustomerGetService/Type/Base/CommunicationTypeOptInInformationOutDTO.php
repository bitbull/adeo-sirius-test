<?php
/**
 * Parent model for CommunicationTypeOptInInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CommunicationTypeOptInInformationOutDTO
{
    
    /**
     * CommunicationTypeCode
     * 
     * @var int
     */
    public $communicationTypeCode;
    
    /**
     * CommunicationTypeLabel
     * 
     * @var string
     */
    public $communicationTypeLabel;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communicationTypeCode
     * 
     * @return int
     */
    public function getCommunicationTypeCode()
    {
        return $this->communicationTypeCode;
    }
    
    /**
     * Set communicationTypeCode
     * 
     * @param int $value communicationTypeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO
     */
    public function setCommunicationTypeCode($value)
    {
        $this->communicationTypeCode = $value;
        return $this;
    }
    
    /**
     * Get communicationTypeLabel
     * 
     * @return string
     */
    public function getCommunicationTypeLabel()
    {
        return $this->communicationTypeLabel;
    }
    
    /**
     * Set communicationTypeLabel
     * 
     * @param string $value communicationTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO
     */
    public function setCommunicationTypeLabel($value)
    {
        $this->communicationTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationTypeOptInInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
