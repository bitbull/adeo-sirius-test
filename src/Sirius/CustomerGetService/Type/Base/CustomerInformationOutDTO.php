<?php
/**
 * Parent model for CustomerInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CustomerInformationOutDTO
{
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO[]
     */
    public $contacts = array();
    
    /**
     * CustomerLocks collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO[]
     */
    public $customerLocks = array();
    
    /**
     * LanguageCode
     * 
     * @var string
     */
    public $languageCode;
    
    /**
     * LanguageLabel
     * 
     * @var string
     */
    public $languageLabel;
    
    /**
     * StatusCode
     * 
     * @var int
     */
    public $statusCode;
    
    /**
     * StatusLabel
     * 
     * @var string
     */
    public $statusLabel;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * ManagementEntityNumber
     * 
     * @var int
     */
    public $managementEntityNumber;
    
    /**
     * ManagementEntityLabel
     * 
     * @var string
     */
    public $managementEntityLabel;
    
    /**
     * CreationEntityNumber
     * 
     * @var int
     */
    public $creationEntityNumber;
    
    /**
     * CreationEntityLabel
     * 
     * @var string
     */
    public $creationEntityLabel;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * StateLabel
     * 
     * @var string
     */
    public $stateLabel;
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * GlobalOptins collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[]
     */
    public $globalOptins = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $classifications = array();
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get customerLocks
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO[]
     */
    public function getCustomerLocks()
    {
        return $this->customerLocks;
    }
    
    /**
     * Add element on customerLocks collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerLockOutDTO[] $value customerLocks
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setCustomerLocks($value = null)
    {
        $this->customerLocks[] = $value;
        return $this;
    }
    
    /**
     * Get languageCode
     * 
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }
    
    /**
     * Set languageCode
     * 
     * @param string $value languageCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setLanguageCode($value)
    {
        $this->languageCode = $value;
        return $this;
    }
    
    /**
     * Get languageLabel
     * 
     * @return string
     */
    public function getLanguageLabel()
    {
        return $this->languageLabel;
    }
    
    /**
     * Set languageLabel
     * 
     * @param string $value languageLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setLanguageLabel($value)
    {
        $this->languageLabel = $value;
        return $this;
    }
    
    /**
     * Get statusCode
     * 
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    
    /**
     * Set statusCode
     * 
     * @param int $value statusCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setStatusCode($value)
    {
        $this->statusCode = $value;
        return $this;
    }
    
    /**
     * Get statusLabel
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }
    
    /**
     * Set statusLabel
     * 
     * @param string $value statusLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setStatusLabel($value)
    {
        $this->statusLabel = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get managementEntityNumber
     * 
     * @return int
     */
    public function getManagementEntityNumber()
    {
        return $this->managementEntityNumber;
    }
    
    /**
     * Set managementEntityNumber
     * 
     * @param int $value managementEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setManagementEntityNumber($value)
    {
        $this->managementEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get managementEntityLabel
     * 
     * @return string
     */
    public function getManagementEntityLabel()
    {
        return $this->managementEntityLabel;
    }
    
    /**
     * Set managementEntityLabel
     * 
     * @param string $value managementEntityLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setManagementEntityLabel($value)
    {
        $this->managementEntityLabel = $value;
        return $this;
    }
    
    /**
     * Get creationEntityNumber
     * 
     * @return int
     */
    public function getCreationEntityNumber()
    {
        return $this->creationEntityNumber;
    }
    
    /**
     * Set creationEntityNumber
     * 
     * @param int $value creationEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setCreationEntityNumber($value)
    {
        $this->creationEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get creationEntityLabel
     * 
     * @return string
     */
    public function getCreationEntityLabel()
    {
        return $this->creationEntityLabel;
    }
    
    /**
     * Set creationEntityLabel
     * 
     * @param string $value creationEntityLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setCreationEntityLabel($value)
    {
        $this->creationEntityLabel = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get stateLabel
     * 
     * @return string
     */
    public function getStateLabel()
    {
        return $this->stateLabel;
    }
    
    /**
     * Set stateLabel
     * 
     * @param string $value stateLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setStateLabel($value)
    {
        $this->stateLabel = $value;
        return $this;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get globalOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[]
     */
    public function getGlobalOptins()
    {
        return $this->globalOptins;
    }
    
    /**
     * Add element on globalOptins collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[] $value globalOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setGlobalOptins($value = null)
    {
        $this->globalOptins[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
