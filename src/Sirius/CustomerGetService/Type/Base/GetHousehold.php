<?php
/**
 * Parent model for GetHousehold
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetHousehold
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetHousehold
     */
    public function setArg0(\Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
