<?php
/**
 * Parent model for LeisuresOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class LeisuresOutputDTO
{
    
    /**
     * Leisures collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO[]
     */
    public $leisures = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get leisures
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO[]
     */
    public function getLeisures()
    {
        return $this->leisures;
    }
    
    /**
     * Add element on leisures collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO[] $value leisures
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO
     */
    public function setLeisures($value = null)
    {
        $this->leisures[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
