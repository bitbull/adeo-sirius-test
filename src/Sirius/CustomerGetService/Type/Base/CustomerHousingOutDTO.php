<?php
/**
 * Parent model for CustomerHousingOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CustomerHousingOutDTO
{
    
    /**
     * MovingInDate
     * 
     * @var string
     */
    public $movingInDate;
    
    /**
     * PurchaseDate
     * 
     * @var string
     */
    public $purchaseDate;
    
    /**
     * StillIn
     * 
     * @var boolean
     */
    public $stillIn;
    
    /**
     * HousingCharacteristics collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingCharacteristicOutDTO[]
     */
    public $housingCharacteristics = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get movingInDate
     * 
     * @return string
     */
    public function getMovingInDate()
    {
        return $this->movingInDate;
    }
    
    /**
     * Set movingInDate
     * 
     * @param string $value movingInDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function setMovingInDate($value)
    {
        $this->movingInDate = $value;
        return $this;
    }
    
    /**
     * Get purchaseDate
     * 
     * @return string
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }
    
    /**
     * Set purchaseDate
     * 
     * @param string $value purchaseDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function setPurchaseDate($value)
    {
        $this->purchaseDate = $value;
        return $this;
    }
    
    /**
     * Get stillIn
     * 
     * @return boolean
     */
    public function getStillIn()
    {
        return $this->stillIn;
    }
    
    /**
     * Set stillIn
     * 
     * @param boolean $value stillIn
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function setStillIn($value)
    {
        $this->stillIn = $value;
        return $this;
    }
    
    /**
     * Get housingCharacteristics
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingCharacteristicOutDTO[]
     */
    public function getHousingCharacteristics()
    {
        return $this->housingCharacteristics;
    }
    
    /**
     * Add element on housingCharacteristics collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingCharacteristicOutDTO[] $value housingCharacteristics
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function setHousingCharacteristics($value = null)
    {
        $this->housingCharacteristics[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
