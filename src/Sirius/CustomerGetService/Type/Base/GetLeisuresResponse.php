<?php
/**
 * Parent model for GetLeisuresResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetLeisuresResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetLeisuresResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\LeisuresOutputDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
