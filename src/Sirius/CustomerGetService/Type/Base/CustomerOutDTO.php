<?php
/**
 * Parent model for CustomerOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CustomerOutDTO
{
    
    /**
     * AddressBook collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[]
     */
    public $addressBook = array();
    
    /**
     * LegalEntityInformation
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public $legalEntityInformation;
    
    /**
     * CustomerInformation
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public $customerInformation;
    
    /**
     * DegradedMode
     * 
     * @var boolean
     */
    public $degradedMode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get addressBook
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[]
     */
    public function getAddressBook()
    {
        return $this->addressBook;
    }
    
    /**
     * Add element on addressBook collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO[] $value addressBook
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function setAddressBook($value = null)
    {
        $this->addressBook[] = $value;
        return $this;
    }
    
    /**
     * Get legalEntityInformation
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO
     */
    public function getLegalEntityInformation()
    {
        return $this->legalEntityInformation;
    }
    
    /**
     * Set legalEntityInformation
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO $value legalEntityInformation
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function setLegalEntityInformation(\Adeo\Sirius\CustomerGetService\Type\Base\LegalEntityInformationOutDTO $value)
    {
        $this->legalEntityInformation = $value;
        return $this;
    }
    
    /**
     * Get customerInformation
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO
     */
    public function getCustomerInformation()
    {
        return $this->customerInformation;
    }
    
    /**
     * Set customerInformation
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO $value customerInformation
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function setCustomerInformation(\Adeo\Sirius\CustomerGetService\Type\Base\CustomerInformationOutDTO $value)
    {
        $this->customerInformation = $value;
        return $this;
    }
    
    /**
     * Get degradedMode
     * 
     * @return boolean
     */
    public function getDegradedMode()
    {
        return $this->degradedMode;
    }
    
    /**
     * Set degradedMode
     * 
     * @param boolean $value degradedMode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function setDegradedMode($value)
    {
        $this->degradedMode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
