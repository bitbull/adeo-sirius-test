<?php
/**
 * Parent model for LeisureInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class LeisureInformationOutDTO
{
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * LevelCode
     * 
     * @var int
     */
    public $levelCode;
    
    /**
     * LevelLabel
     * 
     * @var string
     */
    public $levelLabel;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get levelCode
     * 
     * @return int
     */
    public function getLevelCode()
    {
        return $this->levelCode;
    }
    
    /**
     * Set levelCode
     * 
     * @param int $value levelCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO
     */
    public function setLevelCode($value)
    {
        $this->levelCode = $value;
        return $this;
    }
    
    /**
     * Get levelLabel
     * 
     * @return string
     */
    public function getLevelLabel()
    {
        return $this->levelLabel;
    }
    
    /**
     * Set levelLabel
     * 
     * @param string $value levelLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO
     */
    public function setLevelLabel($value)
    {
        $this->levelLabel = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\LeisureInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
