<?php
/**
 * Parent model for NotePadsOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class NotePadsOutputDTO
{
    
    /**
     * NotePads collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO[]
     */
    public $notePads = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get notePads
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO[]
     */
    public function getNotePads()
    {
        return $this->notePads;
    }
    
    /**
     * Add element on notePads collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO[] $value notePads
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO
     */
    public function setNotePads($value = null)
    {
        $this->notePads[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadsOutputDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
