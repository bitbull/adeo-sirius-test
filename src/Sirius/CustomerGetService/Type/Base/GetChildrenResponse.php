<?php
/**
 * Parent model for GetChildrenResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetChildrenResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetChildrenResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
