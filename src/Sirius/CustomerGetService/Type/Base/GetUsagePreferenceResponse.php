<?php
/**
 * Parent model for GetUsagePreferenceResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetUsagePreferenceResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceOutDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetUsagePreferenceResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
