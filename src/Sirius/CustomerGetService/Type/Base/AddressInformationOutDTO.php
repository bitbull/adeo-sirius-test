<?php
/**
 * Parent model for AddressInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class AddressInformationOutDTO
{
    
    /**
     * Comment
     * 
     * @var string
     */
    public $comment;
    
    /**
     * AddressTypes collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\AddressTypeOutDTO[]
     */
    public $addressTypes = array();
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * Line1
     * 
     * @var string
     */
    public $line1;
    
    /**
     * Line2
     * 
     * @var string
     */
    public $line2;
    
    /**
     * Line3
     * 
     * @var string
     */
    public $line3;
    
    /**
     * Line4
     * 
     * @var string
     */
    public $line4;
    
    /**
     * PostalCode
     * 
     * @var string
     */
    public $postalCode;
    
    /**
     * City
     * 
     * @var string
     */
    public $city;
    
    /**
     * Province
     * 
     * @var string
     */
    public $province;
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * CountryLabel
     * 
     * @var string
     */
    public $countryLabel;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * ContactEntityNumber
     * 
     * @var int
     */
    public $contactEntityNumber;
    
    /**
     * ContactEntityLabel
     * 
     * @var string
     */
    public $contactEntityLabel;
    
    /**
     * MainAddress
     * 
     * @var boolean
     */
    public $mainAddress;
    
    /**
     * Goneaway
     * 
     * @var boolean
     */
    public $goneaway;
    
    /**
     * SpecificOptins collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[]
     */
    public $specificOptins = array();
    
    /**
     * AddressExternalCodes collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut[]
     */
    public $addressExternalCodes = array();
    
    /**
     * Housing
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public $housing;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get comment
     * 
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Set comment
     * 
     * @param string $value comment
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setComment($value)
    {
        $this->comment = $value;
        return $this;
    }
    
    /**
     * Get addressTypes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressTypeOutDTO[]
     */
    public function getAddressTypes()
    {
        return $this->addressTypes;
    }
    
    /**
     * Add element on addressTypes collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\AddressTypeOutDTO[] $value addressTypes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setAddressTypes($value = null)
    {
        $this->addressTypes[] = $value;
        return $this;
    }
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get line1
     * 
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }
    
    /**
     * Set line1
     * 
     * @param string $value line1
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setLine1($value)
    {
        $this->line1 = $value;
        return $this;
    }
    
    /**
     * Get line2
     * 
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }
    
    /**
     * Set line2
     * 
     * @param string $value line2
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setLine2($value)
    {
        $this->line2 = $value;
        return $this;
    }
    
    /**
     * Get line3
     * 
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }
    
    /**
     * Set line3
     * 
     * @param string $value line3
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setLine3($value)
    {
        $this->line3 = $value;
        return $this;
    }
    
    /**
     * Get line4
     * 
     * @return string
     */
    public function getLine4()
    {
        return $this->line4;
    }
    
    /**
     * Set line4
     * 
     * @param string $value line4
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setLine4($value)
    {
        $this->line4 = $value;
        return $this;
    }
    
    /**
     * Get postalCode
     * 
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * Set postalCode
     * 
     * @param string $value postalCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setPostalCode($value)
    {
        $this->postalCode = $value;
        return $this;
    }
    
    /**
     * Get city
     * 
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Set city
     * 
     * @param string $value city
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setCity($value)
    {
        $this->city = $value;
        return $this;
    }
    
    /**
     * Get province
     * 
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    /**
     * Set province
     * 
     * @param string $value province
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setProvince($value)
    {
        $this->province = $value;
        return $this;
    }
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get countryLabel
     * 
     * @return string
     */
    public function getCountryLabel()
    {
        return $this->countryLabel;
    }
    
    /**
     * Set countryLabel
     * 
     * @param string $value countryLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setCountryLabel($value)
    {
        $this->countryLabel = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get contactEntityNumber
     * 
     * @return int
     */
    public function getContactEntityNumber()
    {
        return $this->contactEntityNumber;
    }
    
    /**
     * Set contactEntityNumber
     * 
     * @param int $value contactEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setContactEntityNumber($value)
    {
        $this->contactEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get contactEntityLabel
     * 
     * @return string
     */
    public function getContactEntityLabel()
    {
        return $this->contactEntityLabel;
    }
    
    /**
     * Set contactEntityLabel
     * 
     * @param string $value contactEntityLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setContactEntityLabel($value)
    {
        $this->contactEntityLabel = $value;
        return $this;
    }
    
    /**
     * Get mainAddress
     * 
     * @return boolean
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }
    
    /**
     * Set mainAddress
     * 
     * @param boolean $value mainAddress
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setMainAddress($value)
    {
        $this->mainAddress = $value;
        return $this;
    }
    
    /**
     * Get goneaway
     * 
     * @return boolean
     */
    public function getGoneaway()
    {
        return $this->goneaway;
    }
    
    /**
     * Set goneaway
     * 
     * @param boolean $value goneaway
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setGoneaway($value)
    {
        $this->goneaway = $value;
        return $this;
    }
    
    /**
     * Get specificOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[]
     */
    public function getSpecificOptins()
    {
        return $this->specificOptins;
    }
    
    /**
     * Add element on specificOptins collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[] $value specificOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setSpecificOptins($value = null)
    {
        $this->specificOptins[] = $value;
        return $this;
    }
    
    /**
     * Get addressExternalCodes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut[]
     */
    public function getAddressExternalCodes()
    {
        return $this->addressExternalCodes;
    }
    
    /**
     * Add element on addressExternalCodes collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\AddressExternalCodeOut[] $value addressExternalCodes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setAddressExternalCodes($value = null)
    {
        $this->addressExternalCodes[] = $value;
        return $this;
    }
    
    /**
     * Get housing
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO
     */
    public function getHousing()
    {
        return $this->housing;
    }
    
    /**
     * Set housing
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO $value housing
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setHousing(\Adeo\Sirius\CustomerGetService\Type\Base\CustomerHousingOutDTO $value)
    {
        $this->housing = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\AddressInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
