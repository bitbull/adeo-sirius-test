<?php
/**
 * Parent model for NotePadInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class NotePadInformationDTO
{
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * Subject
     * 
     * @var string
     */
    public $subject;
    
    /**
     * Message
     * 
     * @var string
     */
    public $message;
    
    /**
     * CreationDate
     * 
     * @var string
     */
    public $creationDate;
    
    /**
     * FullName
     * 
     * @var string
     */
    public $fullName;
    
    /**
     * Ldap
     * 
     * @var string
     */
    public $ldap;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get subject
     * 
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * Set subject
     * 
     * @param string $value subject
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setSubject($value)
    {
        $this->subject = $value;
        return $this;
    }
    
    /**
     * Get message
     * 
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     * 
     * @param string $value message
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }
    
    /**
     * Get creationDate
     * 
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    /**
     * Set creationDate
     * 
     * @param string $value creationDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setCreationDate($value)
    {
        $this->creationDate = $value;
        return $this;
    }
    
    /**
     * Get fullName
     * 
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }
    
    /**
     * Set fullName
     * 
     * @param string $value fullName
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setFullName($value)
    {
        $this->fullName = $value;
        return $this;
    }
    
    /**
     * Get ldap
     * 
     * @return string
     */
    public function getLdap()
    {
        return $this->ldap;
    }
    
    /**
     * Set ldap
     * 
     * @param string $value ldap
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setLdap($value)
    {
        $this->ldap = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\NotePadInformationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
