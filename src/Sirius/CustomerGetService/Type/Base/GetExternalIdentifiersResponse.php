<?php
/**
 * Parent model for GetExternalIdentifiersResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetExternalIdentifiersResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetExternalIdentifiersResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
