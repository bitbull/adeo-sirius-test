<?php
/**
 * Parent model for CommunicationInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class CommunicationInformationOutDTO
{
    
    /**
     * CommunicationScopes collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationScopeOutDTO[]
     */
    public $communicationScopes = array();
    
    /**
     * Goneaway
     * 
     * @var boolean
     */
    public $goneaway;
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * PhoneTypeCode
     * 
     * @var int
     */
    public $phoneTypeCode;
    
    /**
     * PhoneTypeLabel
     * 
     * @var string
     */
    public $phoneTypeLabel;
    
    /**
     * MainCommunication
     * 
     * @var boolean
     */
    public $mainCommunication;
    
    /**
     * Order
     * 
     * @var int
     */
    public $order;
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * SpecificOptins collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[]
     */
    public $specificOptins = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communicationScopes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationScopeOutDTO[]
     */
    public function getCommunicationScopes()
    {
        return $this->communicationScopes;
    }
    
    /**
     * Add element on communicationScopes collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationScopeOutDTO[] $value communicationScopes
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setCommunicationScopes($value = null)
    {
        $this->communicationScopes[] = $value;
        return $this;
    }
    
    /**
     * Get goneaway
     * 
     * @return boolean
     */
    public function getGoneaway()
    {
        return $this->goneaway;
    }
    
    /**
     * Set goneaway
     * 
     * @param boolean $value goneaway
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setGoneaway($value)
    {
        $this->goneaway = $value;
        return $this;
    }
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get phoneTypeCode
     * 
     * @return int
     */
    public function getPhoneTypeCode()
    {
        return $this->phoneTypeCode;
    }
    
    /**
     * Set phoneTypeCode
     * 
     * @param int $value phoneTypeCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setPhoneTypeCode($value)
    {
        $this->phoneTypeCode = $value;
        return $this;
    }
    
    /**
     * Get phoneTypeLabel
     * 
     * @return string
     */
    public function getPhoneTypeLabel()
    {
        return $this->phoneTypeLabel;
    }
    
    /**
     * Set phoneTypeLabel
     * 
     * @param string $value phoneTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setPhoneTypeLabel($value)
    {
        $this->phoneTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get mainCommunication
     * 
     * @return boolean
     */
    public function getMainCommunication()
    {
        return $this->mainCommunication;
    }
    
    /**
     * Set mainCommunication
     * 
     * @param boolean $value mainCommunication
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setMainCommunication($value)
    {
        $this->mainCommunication = $value;
        return $this;
    }
    
    /**
     * Get order
     * 
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set order
     * 
     * @param int $value order
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get specificOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[]
     */
    public function getSpecificOptins()
    {
        return $this->specificOptins;
    }
    
    /**
     * Add element on specificOptins collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SpecificOptinOutDTO[] $value specificOptins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setSpecificOptins($value = null)
    {
        $this->specificOptins[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
