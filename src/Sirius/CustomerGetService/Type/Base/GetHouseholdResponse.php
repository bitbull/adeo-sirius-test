<?php
/**
 * Parent model for GetHouseholdResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetHouseholdResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetHouseholdResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\HouseholdOutDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
