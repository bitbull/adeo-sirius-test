<?php
/**
 * Parent model for UsagePreferenceIdentificationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class UsagePreferenceIdentificationDTO
{
    
    /**
     * Context
     * 
     * @var int
     */
    public $context;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Get context
     * 
     * @return int
     */
    public function getContext()
    {
        return $this->context;
    }
    
    /**
     * Set context
     * 
     * @param int $value context
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public function setContext($value)
    {
        $this->context = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
}
