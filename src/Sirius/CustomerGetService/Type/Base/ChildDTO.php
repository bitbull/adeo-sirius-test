<?php
/**
 * Parent model for ChildDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class ChildDTO
{
    
    /**
     * BirthDate
     * 
     * @var string
     */
    public $birthDate;
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * Get birthDate
     * 
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /**
     * Set birthDate
     * 
     * @param string $value birthDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $value;
        return $this;
    }
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ChildDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
