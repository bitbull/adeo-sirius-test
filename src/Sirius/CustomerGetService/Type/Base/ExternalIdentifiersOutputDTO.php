<?php
/**
 * Parent model for ExternalIdentifiersOutputDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class ExternalIdentifiersOutputDTO
{
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifiersOutputDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
