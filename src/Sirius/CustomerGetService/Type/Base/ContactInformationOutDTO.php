<?php
/**
 * Parent model for ContactInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class ContactInformationOutDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * Communications collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO[]
     */
    public $communications = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $classifications = array();
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Segmentations collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[]
     */
    public $segmentations = array();
    
    /**
     * Active
     * 
     * @var boolean
     */
    public $active;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[]
     */
    public $optins = array();
    
    /**
     * ContactCustomerNumber
     * 
     * @var int
     */
    public $contactCustomerNumber;
    
    /**
     * TitleCode
     * 
     * @var int
     */
    public $titleCode;
    
    /**
     * TitleLabel
     * 
     * @var string
     */
    public $titleLabel;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * BirthName
     * 
     * @var string
     */
    public $birthName;
    
    /**
     * BirthDate
     * 
     * @var string
     */
    public $birthDate;
    
    /**
     * CompanyDepartment
     * 
     * @var string
     */
    public $companyDepartment;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * HouseholdIdentifier
     * 
     * @var int
     */
    public $householdIdentifier;
    
    /**
     * MaritalStatusCode
     * 
     * @var int
     */
    public $maritalStatusCode;
    
    /**
     * MaritalStatusLabel
     * 
     * @var string
     */
    public $maritalStatusLabel;
    
    /**
     * Order
     * 
     * @var int
     */
    public $order;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO[]
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Add element on communications collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\CommunicationInformationOutDTO[] $value communications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setCommunications($value = null)
    {
        $this->communications[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ClassificationInformationOutDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\ExternalIdentifierInformationOutDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get segmentations
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[]
     */
    public function getSegmentations()
    {
        return $this->segmentations;
    }
    
    /**
     * Add element on segmentations collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationInformationOutDTO[] $value segmentations
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setSegmentations($value = null)
    {
        $this->segmentations[] = $value;
        return $this;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set active
     * 
     * @param boolean $value active
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\GlobalOptInInformationOutDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get contactCustomerNumber
     * 
     * @return int
     */
    public function getContactCustomerNumber()
    {
        return $this->contactCustomerNumber;
    }
    
    /**
     * Set contactCustomerNumber
     * 
     * @param int $value contactCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setContactCustomerNumber($value)
    {
        $this->contactCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get titleCode
     * 
     * @return int
     */
    public function getTitleCode()
    {
        return $this->titleCode;
    }
    
    /**
     * Set titleCode
     * 
     * @param int $value titleCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setTitleCode($value)
    {
        $this->titleCode = $value;
        return $this;
    }
    
    /**
     * Get titleLabel
     * 
     * @return string
     */
    public function getTitleLabel()
    {
        return $this->titleLabel;
    }
    
    /**
     * Set titleLabel
     * 
     * @param string $value titleLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setTitleLabel($value)
    {
        $this->titleLabel = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get birthName
     * 
     * @return string
     */
    public function getBirthName()
    {
        return $this->birthName;
    }
    
    /**
     * Set birthName
     * 
     * @param string $value birthName
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setBirthName($value)
    {
        $this->birthName = $value;
        return $this;
    }
    
    /**
     * Get birthDate
     * 
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /**
     * Set birthDate
     * 
     * @param string $value birthDate
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $value;
        return $this;
    }
    
    /**
     * Get companyDepartment
     * 
     * @return string
     */
    public function getCompanyDepartment()
    {
        return $this->companyDepartment;
    }
    
    /**
     * Set companyDepartment
     * 
     * @param string $value companyDepartment
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setCompanyDepartment($value)
    {
        $this->companyDepartment = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get householdIdentifier
     * 
     * @return int
     */
    public function getHouseholdIdentifier()
    {
        return $this->householdIdentifier;
    }
    
    /**
     * Set householdIdentifier
     * 
     * @param int $value householdIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setHouseholdIdentifier($value)
    {
        $this->householdIdentifier = $value;
        return $this;
    }
    
    /**
     * Get maritalStatusCode
     * 
     * @return int
     */
    public function getMaritalStatusCode()
    {
        return $this->maritalStatusCode;
    }
    
    /**
     * Set maritalStatusCode
     * 
     * @param int $value maritalStatusCode
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setMaritalStatusCode($value)
    {
        $this->maritalStatusCode = $value;
        return $this;
    }
    
    /**
     * Get maritalStatusLabel
     * 
     * @return string
     */
    public function getMaritalStatusLabel()
    {
        return $this->maritalStatusLabel;
    }
    
    /**
     * Set maritalStatusLabel
     * 
     * @param string $value maritalStatusLabel
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setMaritalStatusLabel($value)
    {
        $this->maritalStatusLabel = $value;
        return $this;
    }
    
    /**
     * Get order
     * 
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set order
     * 
     * @param int $value order
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\ContactInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
