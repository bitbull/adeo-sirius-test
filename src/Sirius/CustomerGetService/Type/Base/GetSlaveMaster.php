<?php
/**
 * Parent model for GetSlaveMaster
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetSlaveMaster
{
    
    /**
     * Arg0
     * 
     * @var int
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return int
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param int $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetSlaveMaster
     */
    public function setArg0($value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
