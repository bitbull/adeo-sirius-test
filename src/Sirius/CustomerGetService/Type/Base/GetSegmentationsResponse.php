<?php
/**
 * Parent model for GetSegmentationsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetSegmentationsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetSegmentationsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGetService\Type\Base\SegmentationsOutputDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
