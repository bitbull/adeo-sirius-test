<?php
/**
 * Parent model for HouseholdIdentificationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class HouseholdIdentificationDTO
{
    
    /**
     * HouseholdIdentifier
     * 
     * @var int
     */
    public $householdIdentifier;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get householdIdentifier
     * 
     * @return int
     */
    public function getHouseholdIdentifier()
    {
        return $this->householdIdentifier;
    }
    
    /**
     * Set householdIdentifier
     * 
     * @param int $value householdIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public function setHouseholdIdentifier($value)
    {
        $this->householdIdentifier = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerGetService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\HouseholdIdentificationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
