<?php
/**
 * Parent model for GetUsagePreference
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetService\Type\Base;

abstract class GetUsagePreference
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\Base\GetUsagePreference
     */
    public function setArg0(\Adeo\Sirius\CustomerGetService\Type\Base\UsagePreferenceIdentificationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
