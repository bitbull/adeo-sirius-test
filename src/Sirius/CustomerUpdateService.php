<?php
/**
 * Proxy for CustomerUpdateService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerUpdateService as CustomerUpdateServiceBase;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentity;
use \Adeo\Sirius\CustomerUpdateService\Type\SetChildren;
use \Adeo\Sirius\CustomerUpdateService\Type\SetLeisures;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContext;
use \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreference;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCommunications;
use \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLock;
use \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContacts;
use \Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCommunities;
use \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiers;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\SetClassifications;
use \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomer;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLock;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdAddress;
use \Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\DelNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentity;
use \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentations;
use \Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO;
use \Adeo\Sirius\CustomerUpdateService\Type\NaturalPersonCustomerIdentityDTO;
use \Adeo\Sirius\CustomerUpdateService\Type\LegalEntityCustomerIdentityDTO;
use \Adeo\Sirius\CustomerUpdateService\Type\DeleteAddressDTO;

class CustomerUpdateService
    extends CustomerUpdateServiceBase
{
    
    /**
     * Method updLegalEntityCustomerIdentity
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\LegalEntityCustomerIdentityDTO $legalEntity Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\LegalEntityCustomerIdentityDTO
     */
    public function updLegalEntityCustomerIdentity(LegalEntityCustomerIdentityDTO $legalEntity)
    {
        $updLegalEntityCustomerIdentity = new UpdLegalEntityCustomerIdentity();
        $updLegalEntityCustomerIdentity->setArg0($legalEntity);
        $response = parent::_updLegalEntityCustomerIdentity($updLegalEntityCustomerIdentity);
        return $response->getReturn();
    }
    
    /**
     * Method setChildren
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetChildren $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetChildrenResponse
     */
    public function setChildren(SetChildren $parameters)
    {
        $response = parent::_setChildren($parameters);
        return $response;
    }
    
    /**
     * Method setLeisures
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetLeisures $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetLeisuresResponse
     */
    public function setLeisures(SetLeisures $parameters)
    {
        $response = parent::_setLeisures($parameters);
        return $response;
    }
    
    /**
     * Method setCustomerUseContext
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContext $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContextResponse
     */
    public function setCustomerUseContext(SetCustomerUseContext $parameters)
    {
        $response = parent::_setCustomerUseContext($parameters);
        return $response;
    }
    
    /**
     * Method setUsagePreference
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreference $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreferenceResponse
     */
    public function setUsagePreference(SetUsagePreference $parameters)
    {
        $response = parent::_setUsagePreference($parameters);
        return $response;
    }
    
    /**
     * Method setCommunications
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCommunications $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCommunicationsResponse
     */
    public function setCommunications(SetCommunications $parameters)
    {
        $response = parent::_setCommunications($parameters);
        return $response;
    }
    
    /**
     * Method delCustomerLock
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLock $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLockResponse
     */
    public function delCustomerLock(DelCustomerLock $parameters)
    {
        $response = parent::_delCustomerLock($parameters);
        return $response;
    }
    
    /**
     * Method setLegalEntityContacts
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContacts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContactsResponse
     */
    public function setLegalEntityContacts(SetLegalEntityContacts $parameters)
    {
        $response = parent::_setLegalEntityContacts($parameters);
        return $response;
    }
    
    /**
     * Method delAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DeleteAddressDTO $deleteAddress Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousingResponse
     */
    public function delAddressAndHousing(DeleteAddressDTO $deleteAddress)
    {
        $delAddressAndHousing = new DelAddressAndHousing();
        $delAddressAndHousing->setArg0($deleteAddress);
        $response = parent::_delAddressAndHousing($delAddressAndHousing);
        return $response;
    }
    
    /**
     * Method setCommunities
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCommunities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCommunitiesResponse
     */
    public function setCommunities(SetCommunities $parameters)
    {
        $response = parent::_setCommunities($parameters);
        return $response;
    }
    
    /**
     * Method setExternalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiersResponse
     */
    public function setExternalIdentifiers(SetExternalIdentifiers $parameters)
    {
        $response = parent::_setExternalIdentifiers($parameters);
        return $response;
    }
    
    /**
     * Method updateNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePadResponse
     */
    public function updateNotePad(UpdateNotePad $parameters)
    {
        $response = parent::_updateNotePad($parameters);
        return $response;
    }
    
    /**
     * Method createNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePadResponse
     */
    public function createNotePad(CreateNotePad $parameters)
    {
        $response = parent::_createNotePad($parameters);
        return $response;
    }
    
    /**
     * Method setClassifications
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetClassifications $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetClassificationsResponse
     */
    public function setClassifications(SetClassifications $parameters)
    {
        $response = parent::_setClassifications($parameters);
        return $response;
    }
    
    /**
     * Method moveNaturalPersonCustomer
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomerResponse
     */
    public function moveNaturalPersonCustomer(MoveNaturalPersonCustomer $parameters)
    {
        $response = parent::_moveNaturalPersonCustomer($parameters);
        return $response;
    }
    
    /**
     * Method setCustomerLock
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLock $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLockResponse
     */
    public function setCustomerLock(SetCustomerLock $parameters)
    {
        $response = parent::_setCustomerLock($parameters);
        return $response;
    }
    
    /**
     * Method updAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO $address Address
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO
     */
    public function updAddressAndHousing(CustomerAddressDTO $address)
    {
        $updAddressAndHousing = new UpdAddressAndHousing();
        $updAddressAndHousing->setArg0($address);
        $response = parent::_updAddressAndHousing($updAddressAndHousing);
        return $response->getReturn();
    }
    
    /**
     * Method updAddress
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdAddress $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressResponse
     */
    public function updAddress(UpdAddress $parameters)
    {
        $response = parent::_updAddress($parameters);
        return $response;
    }
    
    /**
     * Method createAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO $address Address
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO
     */
    public function createAddressAndHousing(CustomerAddressDTO $address)
    {
        $createAddressAndHousing = new CreateAddressAndHousing();
        $createAddressAndHousing->setArg0($address);
        $response = parent::_createAddressAndHousing($createAddressAndHousing);
        return $response->getReturn();
    }
    
    /**
     * Method delNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DelNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelNotePadResponse
     */
    public function delNotePad(DelNotePad $parameters)
    {
        $response = parent::_delNotePad($parameters);
        return $response;
    }
    
    /**
     * Method updNaturalPersonCustomerIdentity
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\NaturalPersonCustomerIdentityDTO $customer Customer
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\NaturalPersonCustomerIdentityDTO
     */
    public function updNaturalPersonCustomerIdentity(NaturalPersonCustomerIdentityDTO $customer)
    {
        $updNaturalPersonCustomerIdentity = new updNaturalPersonCustomerIdentity();
        $updNaturalPersonCustomerIdentity->setArg0($customer);
        $response = parent::_updNaturalPersonCustomerIdentity($updNaturalPersonCustomerIdentity);
        return $response->getReturn();
    }
    
    /**
     * Method setSegmentations
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentations $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentationsResponse
     */
    public function setSegmentations(SetSegmentations $parameters)
    {
        $response = parent::_setSegmentations($parameters);
        return $response;
    }
}
