<?php
/**
 * Proxy for CustomerParameterService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerParameterService as CustomerParameterServiceBase;
use \Adeo\Sirius\CustomerParameterService\Type\GetAllProperties;

class CustomerParameterService
    extends CustomerParameterServiceBase
{
    
    /**
     * Method getAllProperties
     * 
     * @param \Adeo\Sirius\CustomerParameterService\Type\GetAllProperties $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\GetAllPropertiesResponse
     */
    public function getAllProperties(GetAllProperties $parameters)
    {
        $response = parent::_getAllProperties($parameters);
        return $response;
    }
}
