<?php
/**
 * Model for SendSMSRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type;

use \Adeo\Sirius\SmsService\Type\Base\SendSMSRequest as SendSMSRequestBase;

class SendSMSRequest
    extends SendSMSRequestBase
{
}
