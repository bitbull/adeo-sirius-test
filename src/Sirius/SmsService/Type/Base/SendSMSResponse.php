<?php
/**
 * Parent model for SendSMSResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class SendSMSResponse
{
    
    /**
     * ResponseCode
     * 
     * @var string
     */
    public $responseCode;
    
    /**
     * ResponseLabel
     * 
     * @var string
     */
    public $responseLabel;
    
    /**
     * Messages collection
     * 
     * @var \Adeo\Sirius\SmsService\Type\Base\Message[]
     */
    public $messages = array();
    
    /**
     * Construct SendSMSResponse
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\Message[] $messages Value of messages
     * 
     * @return void
     */
    public function __construct($messages = null)
    {
        $this->messages[] = $messages;
    }
    
    /**
     * Get responseCode
     * 
     * @return string
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }
    
    /**
     * Set responseCode
     * 
     * @param string $value responseCode
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSResponse
     */
    public function setResponseCode($value)
    {
        $this->responseCode = $value;
        return $this;
    }
    
    /**
     * Get responseLabel
     * 
     * @return string
     */
    public function getResponseLabel()
    {
        return $this->responseLabel;
    }
    
    /**
     * Set responseLabel
     * 
     * @param string $value responseLabel
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSResponse
     */
    public function setResponseLabel($value)
    {
        $this->responseLabel = $value;
        return $this;
    }
    
    /**
     * Get messages
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * Add element on messages collection
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\Message[] $value messages
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSResponse
     */
    public function setMessages($value)
    {
        $this->messages[] = $value;
        return $this;
    }
}
