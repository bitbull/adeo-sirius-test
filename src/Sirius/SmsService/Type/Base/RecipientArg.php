<?php
/**
 * Parent model for RecipientArg
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class RecipientArg
{
    
    /**
     * Position
     * 
     * @var int
     */
    public $position;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Get position
     * 
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Set position
     * 
     * @param int $value position
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\RecipientArg
     */
    public function setPosition($value)
    {
        $this->position = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\RecipientArg
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
