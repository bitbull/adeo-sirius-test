<?php
/**
 * Parent model for SendSMSRequestHeader
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class SendSMSRequestHeader
{
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * EntityType
     * 
     * @var int
     */
    public $entityType;
    
    /**
     * EntityNumber
     * 
     * @var int
     */
    public $entityNumber;
    
    /**
     * SenderEmail
     * 
     * @var string
     */
    public $senderEmail;
    
    /**
     * ApplicationCode
     * 
     * @var string
     */
    public $applicationCode;
    
    /**
     * Construct SendSMSRequestHeader
     * 
     * @param string $applicationCode Value of applicationCode
     * 
     * @return void
     */
    public function __construct($applicationCode = null)
    {
        $this->applicationCode = $applicationCode;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get entityType
     * 
     * @return int
     */
    public function getEntityType()
    {
        return $this->entityType;
    }
    
    /**
     * Set entityType
     * 
     * @param int $value entityType
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function setEntityType($value)
    {
        $this->entityType = $value;
        return $this;
    }
    
    /**
     * Get entityNumber
     * 
     * @return int
     */
    public function getEntityNumber()
    {
        return $this->entityNumber;
    }
    
    /**
     * Set entityNumber
     * 
     * @param int $value entityNumber
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function setEntityNumber($value)
    {
        $this->entityNumber = $value;
        return $this;
    }
    
    /**
     * Get senderEmail
     * 
     * @return string
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }
    
    /**
     * Set senderEmail
     * 
     * @param string $value senderEmail
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function setSenderEmail($value)
    {
        $this->senderEmail = $value;
        return $this;
    }
    
    /**
     * Get applicationCode
     * 
     * @return string
     */
    public function getApplicationCode()
    {
        return $this->applicationCode;
    }
    
    /**
     * Set applicationCode
     * 
     * @param string $value applicationCode
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function setApplicationCode($value)
    {
        $this->applicationCode = $value;
        return $this;
    }
}
