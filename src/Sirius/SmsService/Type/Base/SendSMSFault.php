<?php
/**
 * Parent model for SendSMSFault
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class SendSMSFault
{
    
    /**
     * SendSMSFault
     * 
     * @var string
     */
    public $SendSMSFault;
    
    /**
     * Get SendSMSFault
     * 
     * @return string
     */
    public function getSendSMSFault()
    {
        return $this->SendSMSFault;
    }
    
    /**
     * Set SendSMSFault
     * 
     * @param string $value SendSMSFault
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSFault
     */
    public function setSendSMSFault(string $value)
    {
        $this->SendSMSFault = $value;
        return $this;
    }
}
