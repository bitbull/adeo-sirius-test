<?php
/**
 * Parent model for Message
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class Message
{
    
    /**
     * Recipients collection
     * 
     * @var \Adeo\Sirius\SmsService\Type\Base\Recipient[]
     */
    public $recipients = array();
    
    /**
     * TextTemplate
     * 
     * @var string
     */
    public $textTemplate;
    
    /**
     * Construct Message
     * 
     * @param string $textTemplate
     * 
     * @return void
     */
    public function __construct($textTemplate = null)
    {
        $this->textTemplate = $textTemplate;
    }
    
    /**
     * Get recipients
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Recipient[]
     */
    public function getRecipients()
    {
        return $this->recipients;
    }
    
    /**
     * Add element on recipients collection
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\Recipient[] $value recipients
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Message
     */
    public function setRecipients($value)
    {
        $this->recipients[] = $value;
        return $this;
    }
    
    /**
     * Get textTemplate
     * 
     * @return string
     */
    public function getTextTemplate()
    {
        return $this->textTemplate;
    }
    
    /**
     * Set textTemplate
     * 
     * @param string $value textTemplate
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Message
     */
    public function setTextTemplate($value)
    {
        $this->textTemplate = $value;
        return $this;
    }
}
