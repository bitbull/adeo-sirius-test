<?php
/**
 * Parent model for Recipient
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class Recipient
{
    
    /**
     * RequestId
     * 
     * @var string
     */
    public $requestId;
    
    /**
     * PhoneNumber
     * 
     * @var string
     */
    public $phoneNumber;
    
    /**
     * RecipientArgs collection
     * 
     * @var \Adeo\Sirius\SmsService\Type\Base\RecipientArg[]
     */
    public $recipientArgs = array();
    
    /**
     * SmsId
     * 
     * @var string
     */
    public $smsId;
    
    /**
     * Get requestId
     * 
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }
    
    /**
     * Set requestId
     * 
     * @param string $value requestId
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Recipient
     */
    public function setRequestId($value)
    {
        $this->requestId = $value;
        return $this;
    }
    
    /**
     * Get phoneNumber
     * 
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
    
    /**
     * Set phoneNumber
     * 
     * @param string $value phoneNumber
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Recipient
     */
    public function setPhoneNumber($value)
    {
        $this->phoneNumber = $value;
        return $this;
    }
    
    /**
     * Get recipientArgs
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\RecipientArg[]
     */
    public function getRecipientArgs()
    {
        return $this->recipientArgs;
    }
    
    /**
     * Add element on recipientArgs collection
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\RecipientArg[] $value recipientArgs
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Recipient
     */
    public function setRecipientArgs($value)
    {
        $this->recipientArgs[] = $value;
        return $this;
    }
    
    /**
     * Get smsId
     * 
     * @return string
     */
    public function getSmsId()
    {
        return $this->smsId;
    }
    
    /**
     * Set smsId
     * 
     * @param string $value smsId
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Recipient
     */
    public function setSmsId($value)
    {
        $this->smsId = $value;
        return $this;
    }
}
