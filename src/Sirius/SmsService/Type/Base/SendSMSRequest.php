<?php
/**
 * Parent model for SendSMSRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type\Base;

abstract class SendSMSRequest
{
    
    /**
     * Header
     * 
     * @var \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public $header;
    
    /**
     * Messages collection
     * 
     * @var \Adeo\Sirius\SmsService\Type\Base\Message[]
     */
    public $messages = array();
    
    /**
     * Construct SendSMSRequest
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\Message[] $messages Value of messages
     * 
     * @return void
     */
    public function __construct($messages = null)
    {
        $this->messages[] = $messages;
    }
    
    /**
     * Get header
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader
     */
    public function getHeader()
    {
        return $this->header;
    }
    
    /**
     * Set header
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader $value header
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequest
     */
    public function setHeader(\Adeo\Sirius\SmsService\Type\Base\SendSMSRequestHeader $value)
    {
        $this->header = $value;
        return $this;
    }
    
    /**
     * Get messages
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * Add element on messages collection
     * 
     * @param \Adeo\Sirius\SmsService\Type\Base\Message[] $value messages
     * 
     * @return \Adeo\Sirius\SmsService\Type\Base\SendSMSRequest
     */
    public function setMessages($value)
    {
        $this->messages[] = $value;
        return $this;
    }
}
