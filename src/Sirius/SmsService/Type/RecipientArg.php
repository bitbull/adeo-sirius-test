<?php
/**
 * Model for RecipientArg
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type;

use \Adeo\Sirius\SmsService\Type\Base\RecipientArg as RecipientArgBase;

class RecipientArg
    extends RecipientArgBase
{
}
