<?php
/**
 * Model for SendSMSResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\SmsService\Type;

use \Adeo\Sirius\SmsService\Type\Base\SendSMSResponse as SendSMSResponseBase;

class SendSMSResponse
    extends SendSMSResponseBase
{
}
