<?php
/**
 * Parent model for proxy for CustomerCreationService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntity;

abstract class CustomerCreationService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'createLegalEntityCustomer'              => 'Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomer',
        'creationLegalEntityCustomerDTO'         => 'Adeo\Sirius\CustomerCreationService\Type\CreationLegalEntityCustomerDTO',
        'abstractDTO'                            => 'Adeo\Sirius\CustomerCreationService\Type\AbstractDTO',
        'historyInputDTO'                        => 'Adeo\Sirius\CustomerCreationService\Type\HistoryInputDTO',
        'globalOptInInformationInDTO'            => 'Adeo\Sirius\CustomerCreationService\Type\GlobalOptInInformationInDTO',
        'createLegalEntityCustomerResponse'      => 'Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomerResponse',
        'createNaturalPersonCustomer'            => 'Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomer',
        'creationNaturalPersonCustomerDTO'       => 'Adeo\Sirius\CustomerCreationService\Type\CreationNaturalPersonCustomerDTO',
        'createNaturalPersonCustomerResponse'    => 'Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomerResponse',
        'createGlobalNaturalCustomer'            => 'Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomer',
        'fullNaturalCustomersCreationDTO'        => 'Adeo\Sirius\CustomerCreationService\Type\FullNaturalCustomersCreationDTO',
        'customerAddressDTO'                     => 'Adeo\Sirius\CustomerCreationService\Type\CustomerAddressDTO',
        'addressInformationInDTO'                => 'Adeo\Sirius\CustomerCreationService\Type\AddressInformationInDTO',
        'addressTypeInDTO'                       => 'Adeo\Sirius\CustomerCreationService\Type\AddressTypeInDTO',
        'customerHousingInDTO'                   => 'Adeo\Sirius\CustomerCreationService\Type\CustomerHousingInDTO',
        'customerHousingCharacteristicInDTO'     => 'Adeo\Sirius\CustomerCreationService\Type\CustomerHousingCharacteristicInDTO',
        'specificOptinInDTO'                     => 'Adeo\Sirius\CustomerCreationService\Type\SpecificOptinInDTO',
        'addressExternalCodeInDTO'               => 'Adeo\Sirius\CustomerCreationService\Type\AddressExternalCodeInDTO',
        'abstractModelObject'                    => 'Adeo\Sirius\CustomerCreationService\Type\AbstractModelObject',
        'fullNaturalCustomerDTO'                 => 'Adeo\Sirius\CustomerCreationService\Type\FullNaturalCustomerDTO',
        'commonInformationsDTO'                  => 'Adeo\Sirius\CustomerCreationService\Type\CommonInformationsDTO',
        'naturalPersonCommunicationDTO'          => 'Adeo\Sirius\CustomerCreationService\Type\NaturalPersonCommunicationDTO',
        'setCommunicationDTO'                    => 'Adeo\Sirius\CustomerCreationService\Type\SetCommunicationDTO',
        'communicationScopeOutDTO'               => 'Adeo\Sirius\CustomerCreationService\Type\CommunicationScopeOutDTO',
        'communicationTypeOptInInformationInDTO' => 'Adeo\Sirius\CustomerCreationService\Type\CommunicationTypeOptInInformationInDTO',
        'customerClassificationDTO'              => 'Adeo\Sirius\CustomerCreationService\Type\CustomerClassificationDTO',
        'classificationInformationInDTO'         => 'Adeo\Sirius\CustomerCreationService\Type\ClassificationInformationInDTO',
        'customerExternalIdentifiersDTO'         => 'Adeo\Sirius\CustomerCreationService\Type\CustomerExternalIdentifiersDTO',
        'externalIdentifierInformationInDTO'     => 'Adeo\Sirius\CustomerCreationService\Type\ExternalIdentifierInformationInDTO',
        'customerLeisuresDTO'                    => 'Adeo\Sirius\CustomerCreationService\Type\CustomerLeisuresDTO',
        'leisureInformationInDTO'                => 'Adeo\Sirius\CustomerCreationService\Type\LeisureInformationInDTO',
        'customerCommunitiesDTO'                 => 'Adeo\Sirius\CustomerCreationService\Type\CustomerCommunitiesDTO',
        'communityInformationInDTO'              => 'Adeo\Sirius\CustomerCreationService\Type\CommunityInformationInDTO',
        'customerMessageDTO'                     => 'Adeo\Sirius\CustomerCreationService\Type\CustomerMessageDTO',
        'createGlobalNaturalCustomerResponse'    => 'Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomerResponse',
        'createGlobalLegalEntity'                => 'Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntity',
        'fullLegalCustomerDTO'                   => 'Adeo\Sirius\CustomerCreationService\Type\FullLegalCustomerDTO',
        'fullLegalContactDTO'                    => 'Adeo\Sirius\CustomerCreationService\Type\FullLegalContactDTO',
        'legalEntityContactDTO'                  => 'Adeo\Sirius\CustomerCreationService\Type\LegalEntityContactDTO',
        'createGlobalLegalEntityResponse'        => 'Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntityResponse',
        'SiriusBusinessFault'                    => 'Adeo\Sirius\CustomerCreationService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method createLegalEntityCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomerResponse
     */
    public function _createLegalEntityCustomer(CreateLegalEntityCustomer $parameters)
    {
        return $this->__soapCall("createLegalEntityCustomer", array($parameters));
    }
    
    /**
     * Method createNaturalPersonCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomerResponse
     */
    public function _createNaturalPersonCustomer(CreateNaturalPersonCustomer $parameters)
    {
        return $this->__soapCall("createNaturalPersonCustomer", array($parameters));
    }
    
    /**
     * Method createGlobalNaturalCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomerResponse
     */
    public function _createGlobalNaturalCustomer(CreateGlobalNaturalCustomer $parameters)
    {
        return $this->__soapCall("createGlobalNaturalCustomer", array($parameters));
    }
    
    /**
     * Method createGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntityResponse
     */
    public function _createGlobalLegalEntity(CreateGlobalLegalEntity $parameters)
    {
        return $this->__soapCall("createGlobalLegalEntity", array($parameters));
    }
}
