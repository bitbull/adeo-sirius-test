<?php
/**
 * Parent model for proxy for CustomerGetLoyaltyService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerRequest;

abstract class CustomerGetLoyaltyService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'getLoyaltyCustomerRequest'  => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerRequest',
        'getLoyaltyCustomerResponse' => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerResponse',
        'WLLResponseHeader'          => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\WLLResponseHeader',
        'Account'                    => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\Account',
        'Customer'                   => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\Customer',
        'Card'                       => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\Card',
        'Voucher'                    => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\Voucher',
        'UnitBalance'                => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\UnitBalance',
        'ExtendedInformation'        => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\ExtendedInformation',
        'CustomerIdentification'     => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\CustomerIdentification',
        'Context'                    => 'Adeo\Sirius\CustomerGetLoyaltyService\Type\Context',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method getLoyaltyCustomer
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerRequest $request Value of request
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerResponse
     */
    public function _getLoyaltyCustomer(GetLoyaltyCustomerRequest $request)
    {
        return $this->__soapCall("getLoyaltyCustomer", array($request));
    }
}
