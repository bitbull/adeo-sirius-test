<?php
/**
 * Parent model for proxy for CustomerListOfValuesApplicationService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunities;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristics;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguages;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitles;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfo;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatus;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevels;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntities;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContext;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountries;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentations;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassifications;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopes;

abstract class CustomerListOfValuesApplicationService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'getAllSegmentationsByType'                  => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByType',
        'getAllSegmentationsByTypeResponse'          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByTypeResponse',
        'segmentationInformationOutDTO'              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\SegmentationInformationOutDTO',
        'abstractDTO'                                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\AbstractDTO',
        'getAllUsagePeferenceTypes'                  => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypes',
        'getAllUsagePeferenceTypesResponse'          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypesResponse',
        'usagePreferenceTypeDTO'                     => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\UsagePreferenceTypeDTO',
        'getAllCommunities'                          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunities',
        'getAllCommunitiesResponse'                  => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunitiesResponse',
        'communityInformationOutDTO'                 => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\CommunityInformationOutDTO',
        'getAllHousingCharacteristics'               => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristics',
        'getAllHousingCharacteristicsResponse'       => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsResponse',
        'housingCharacteristicOutDTO'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\HousingCharacteristicOutDTO',
        'getAllLanguages'                            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguages',
        'getAllLanguagesResponse'                    => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguagesResponse',
        'languageOutDTO'                             => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\LanguageOutDTO',
        'getAllTitles'                               => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitles',
        'getAllTitlesResponse'                       => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitlesResponse',
        'titleOutDTO'                                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\TitleOutDTO',
        'getAllCountriesWithFormatInfo'              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfo',
        'getAllCountriesWithFormatInfoResponse'      => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfoResponse',
        'countryWithFormatInfoDTO'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\CountryWithFormatInfoDTO',
        'getAllMaritalStatus'                        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatus',
        'getAllMaritalStatusResponse'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatusResponse',
        'maritalStatusOutDTO'                        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\MaritalStatusOutDTO',
        'getAllPhoneTypes'                           => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypes',
        'getAllPhoneTypesResponse'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypesResponse',
        'phoneTypeOutDTO'                            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\PhoneTypeOutDTO',
        'getAllLeisureLevels'                        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevels',
        'getAllLeisureLevelsResponse'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevelsResponse',
        'leisureLevelOutDTO'                         => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\LeisureLevelOutDTO',
        'getAllHousingCharacteristicTypes'           => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypes',
        'getAllHousingCharacteristicTypesResponse'   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypesResponse',
        'housingCharacteristicTypeOutDTO'            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\HousingCharacteristicTypeOutDTO',
        'getAllBUEntities'                           => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntities',
        'getAllBUEntitiesResponse'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntitiesResponse',
        'buEntityOutDTO'                             => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\BuEntityOutDTO',
        'getAllAddressTypes'                         => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypes',
        'getAllAddressTypesResponse'                 => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypesResponse',
        'addressTypeOutDTO'                          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\AddressTypeOutDTO',
        'getAllUsagePeferenceContext'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContext',
        'getAllUsagePeferenceContextResponse'        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContextResponse',
        'usagePreferenceContextDTO'                  => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\UsagePreferenceContextDTO',
        'getAllLeisureTypes'                         => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypes',
        'getAllLeisureTypesResponse'                 => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypesResponse',
        'leisureTypeOutDTO'                          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\LeisureTypeOutDTO',
        'getAllCountries'                            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountries',
        'getAllCountriesResponse'                    => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesResponse',
        'countryOutDTO'                              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\CountryOutDTO',
        'getAllCommunicationTypes'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypes',
        'getAllCommunicationTypesResponse'           => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypesResponse',
        'communicationTypeOutDTO'                    => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\CommunicationTypeOutDTO',
        'getAllClassificationsByType'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByType',
        'getAllClassificationsByTypeResponse'        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByTypeResponse',
        'classificationInformationOutDTO'            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\ClassificationInformationOutDTO',
        'getAllSegmentations'                        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentations',
        'getAllSegmentationsResponse'                => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsResponse',
        'getAllClassifications'                      => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassifications',
        'getAllClassificationsResponse'              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsResponse',
        'getAllOptinTypes'                           => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypes',
        'getAllOptinTypesResponse'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypesResponse',
        'optinTypeOutDTO'                            => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\OptinTypeOutDTO',
        'getAllAddressExternalCodeTypes'             => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypes',
        'getAllAddressExternalCodeTypesResponse'     => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypesResponse',
        'addressExternalCodeTypeOutDTO'              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\AddressExternalCodeTypeOutDTO',
        'getAllHousingCharacteristicsByType'         => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByType',
        'getAllHousingCharacteristicsByTypeResponse' => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByTypeResponse',
        'getAllExternalIdentifierTypes'              => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypes',
        'getAllExternalIdentifierTypesResponse'      => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypesResponse',
        'externalIdentifierTypeOutDTO'               => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\ExternalIdentifierTypeOutDTO',
        'getAllCommunicationScopes'                  => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopes',
        'getAllCommunicationScopesResponse'          => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopesResponse',
        'communicationScopeOutDTO'                   => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\CommunicationScopeOutDTO',
        'SiriusBusinessFault'                        => 'Adeo\Sirius\CustomerListOfValuesApplicationService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method getAllSegmentationsByType
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByType $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByTypeResponse
     */
    public function _getAllSegmentationsByType(GetAllSegmentationsByType $parameters)
    {
        return $this->__soapCall("getAllSegmentationsByType", array($parameters));
    }
    
    /**
     * Method getAllUsagePeferenceTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypesResponse
     */
    public function _getAllUsagePeferenceTypes(GetAllUsagePeferenceTypes $parameters)
    {
        return $this->__soapCall("getAllUsagePeferenceTypes", array($parameters));
    }
    
    /**
     * Method getAllCommunities
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunitiesResponse
     */
    public function _getAllCommunities(GetAllCommunities $parameters)
    {
        return $this->__soapCall("getAllCommunities", array($parameters));
    }
    
    /**
     * Method getAllHousingCharacteristics
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristics $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsResponse
     */
    public function _getAllHousingCharacteristics(GetAllHousingCharacteristics $parameters)
    {
        return $this->__soapCall("getAllHousingCharacteristics", array($parameters));
    }
    
    /**
     * Method getAllLanguages
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguages $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguagesResponse
     */
    public function _getAllLanguages(GetAllLanguages $parameters)
    {
        return $this->__soapCall("getAllLanguages", array($parameters));
    }
    
    /**
     * Method getAllTitles
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitles $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitlesResponse
     */
    public function _getAllTitles(GetAllTitles $parameters)
    {
        return $this->__soapCall("getAllTitles", array($parameters));
    }
    
    /**
     * Method getAllCountriesWithFormatInfo
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfo $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfoResponse
     */
    public function _getAllCountriesWithFormatInfo(GetAllCountriesWithFormatInfo $parameters)
    {
        return $this->__soapCall("getAllCountriesWithFormatInfo", array($parameters));
    }
    
    /**
     * Method getAllMaritalStatus
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatus $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatusResponse
     */
    public function _getAllMaritalStatus(GetAllMaritalStatus $parameters)
    {
        return $this->__soapCall("getAllMaritalStatus", array($parameters));
    }
    
    /**
     * Method getAllPhoneTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypesResponse
     */
    public function _getAllPhoneTypes(GetAllPhoneTypes $parameters)
    {
        return $this->__soapCall("getAllPhoneTypes", array($parameters));
    }
    
    /**
     * Method getAllLeisureLevels
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevels $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevelsResponse
     */
    public function _getAllLeisureLevels(GetAllLeisureLevels $parameters)
    {
        return $this->__soapCall("getAllLeisureLevels", array($parameters));
    }
    
    /**
     * Method getAllHousingCharacteristicTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypesResponse
     */
    public function _getAllHousingCharacteristicTypes(GetAllHousingCharacteristicTypes $parameters)
    {
        return $this->__soapCall("getAllHousingCharacteristicTypes", array($parameters));
    }
    
    /**
     * Method getAllBUEntities
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntitiesResponse
     */
    public function _getAllBUEntities(GetAllBUEntities $parameters)
    {
        return $this->__soapCall("getAllBUEntities", array($parameters));
    }
    
    /**
     * Method getAllAddressTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypesResponse
     */
    public function _getAllAddressTypes(GetAllAddressTypes $parameters)
    {
        return $this->__soapCall("getAllAddressTypes", array($parameters));
    }
    
    /**
     * Method getAllUsagePeferenceContext
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContext $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContextResponse
     */
    public function _getAllUsagePeferenceContext(GetAllUsagePeferenceContext $parameters)
    {
        return $this->__soapCall("getAllUsagePeferenceContext", array($parameters));
    }
    
    /**
     * Method getAllLeisureTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypesResponse
     */
    public function _getAllLeisureTypes(GetAllLeisureTypes $parameters)
    {
        return $this->__soapCall("getAllLeisureTypes", array($parameters));
    }
    
    /**
     * Method getAllCountries
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountries $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesResponse
     */
    public function _getAllCountries(GetAllCountries $parameters)
    {
        return $this->__soapCall("getAllCountries", array($parameters));
    }
    
    /**
     * Method getAllCommunicationTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypesResponse
     */
    public function _getAllCommunicationTypes(GetAllCommunicationTypes $parameters)
    {
        return $this->__soapCall("getAllCommunicationTypes", array($parameters));
    }
    
    /**
     * Method getAllClassificationsByType
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByType $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByTypeResponse
     */
    public function _getAllClassificationsByType(GetAllClassificationsByType $parameters)
    {
        return $this->__soapCall("getAllClassificationsByType", array($parameters));
    }
    
    /**
     * Method getAllSegmentations
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentations $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsResponse
     */
    public function _getAllSegmentations(GetAllSegmentations $parameters)
    {
        return $this->__soapCall("getAllSegmentations", array($parameters));
    }
    
    /**
     * Method getAllClassifications
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassifications $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsResponse
     */
    public function _getAllClassifications(GetAllClassifications $parameters)
    {
        return $this->__soapCall("getAllClassifications", array($parameters));
    }
    
    /**
     * Method getAllOptinTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypesResponse
     */
    public function _getAllOptinTypes(GetAllOptinTypes $parameters)
    {
        return $this->__soapCall("getAllOptinTypes", array($parameters));
    }
    
    /**
     * Method getAllAddressExternalCodeTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypesResponse
     */
    public function _getAllAddressExternalCodeTypes(GetAllAddressExternalCodeTypes $parameters)
    {
        return $this->__soapCall("getAllAddressExternalCodeTypes", array($parameters));
    }
    
    /**
     * Method getAllHousingCharacteristicsByType
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByType $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByTypeResponse
     */
    public function _getAllHousingCharacteristicsByType(GetAllHousingCharacteristicsByType $parameters)
    {
        return $this->__soapCall("getAllHousingCharacteristicsByType", array($parameters));
    }
    
    /**
     * Method getAllExternalIdentifierTypes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypesResponse
     */
    public function _getAllExternalIdentifierTypes(GetAllExternalIdentifierTypes $parameters)
    {
        return $this->__soapCall("getAllExternalIdentifierTypes", array($parameters));
    }
    
    /**
     * Method getAllCommunicationScopes
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopes $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopesResponse
     */
    public function _getAllCommunicationScopes(GetAllCommunicationScopes $parameters)
    {
        return $this->__soapCall("getAllCommunicationScopes", array($parameters));
    }
}
