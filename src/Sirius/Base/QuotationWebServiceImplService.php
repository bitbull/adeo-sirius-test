<?php
/**
 * Parent model for proxy for QuotationWebServiceImplService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationRequest;

abstract class QuotationWebServiceImplService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'QuotationRequest'  => 'Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationRequest',
        'TechnicalContext'  => 'Adeo\Sirius\QuotationWebServiceImplService\Type\TechnicalContext',
        'BusinessContext'   => 'Adeo\Sirius\QuotationWebServiceImplService\Type\BusinessContext',
        'OrderHeader'       => 'Adeo\Sirius\QuotationWebServiceImplService\Type\OrderHeader',
        'Order'             => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Order',
        'Product'           => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Product',
        'Discount'          => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Discount',
        'Payment'           => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Payment',
        'QuotationResponse' => 'Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationResponse',
        'Error'             => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Error',
        'Voucher'           => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Voucher',
        'DiscountsSummary'  => 'Adeo\Sirius\QuotationWebServiceImplService\Type\DiscountsSummary',
        'Message'           => 'Adeo\Sirius\QuotationWebServiceImplService\Type\Message',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method performQuotation
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationRequest $quotationRequest Value of quotationRequest
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationResponse
     */
    public function _performQuotation(QuotationRequest $quotationRequest)
    {
        return $this->__soapCall("performQuotation", array($quotationRequest));
    }
}
