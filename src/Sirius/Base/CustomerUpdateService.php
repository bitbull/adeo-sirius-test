<?php
/**
 * Parent model for proxy for CustomerUpdateService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentity;
use \Adeo\Sirius\CustomerUpdateService\Type\SetChildren;
use \Adeo\Sirius\CustomerUpdateService\Type\SetLeisures;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContext;
use \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreference;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCommunications;
use \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLock;
use \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContacts;
use \Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCommunities;
use \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiers;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\SetClassifications;
use \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomer;
use \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLock;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdAddress;
use \Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousing;
use \Adeo\Sirius\CustomerUpdateService\Type\DelNotePad;
use \Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentity;
use \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentations;

abstract class CustomerUpdateService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'updLegalEntityCustomerIdentity'           => 'Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentity',
        'legalEntityCustomerIdentityDTO'           => 'Adeo\Sirius\CustomerUpdateService\Type\LegalEntityCustomerIdentityDTO',
        'abstractDTO'                              => 'Adeo\Sirius\CustomerUpdateService\Type\AbstractDTO',
        'globalOptInInformationInDTO'              => 'Adeo\Sirius\CustomerUpdateService\Type\GlobalOptInInformationInDTO',
        'historyInputDTO'                          => 'Adeo\Sirius\CustomerUpdateService\Type\HistoryInputDTO',
        'updLegalEntityCustomerIdentityResponse'   => 'Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentityResponse',
        'setChildren'                              => 'Adeo\Sirius\CustomerUpdateService\Type\SetChildren',
        'setChildrenDTO'                           => 'Adeo\Sirius\CustomerUpdateService\Type\SetChildrenDTO',
        'childDTO'                                 => 'Adeo\Sirius\CustomerUpdateService\Type\ChildDTO',
        'HistoryInput'                             => 'Adeo\Sirius\CustomerUpdateService\Type\HistoryInput',
        'abstractModelObject'                      => 'Adeo\Sirius\CustomerUpdateService\Type\AbstractModelObject',
        'setChildrenResponse'                      => 'Adeo\Sirius\CustomerUpdateService\Type\SetChildrenResponse',
        'setLeisures'                              => 'Adeo\Sirius\CustomerUpdateService\Type\SetLeisures',
        'customerLeisuresDTO'                      => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerLeisuresDTO',
        'leisureInformationInDTO'                  => 'Adeo\Sirius\CustomerUpdateService\Type\LeisureInformationInDTO',
        'setLeisuresResponse'                      => 'Adeo\Sirius\CustomerUpdateService\Type\SetLeisuresResponse',
        'setCustomerUseContext'                    => 'Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContext',
        'creationCustomerUseContextDTO'            => 'Adeo\Sirius\CustomerUpdateService\Type\CreationCustomerUseContextDTO',
        'setCustomerUseContextResponse'            => 'Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContextResponse',
        'setUsagePreference'                       => 'Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreference',
        'setUsagePreferenceDTO'                    => 'Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreferenceDTO',
        'usagePreferenceInDTO'                     => 'Adeo\Sirius\CustomerUpdateService\Type\UsagePreferenceInDTO',
        'usagePreferenceOtherInDTO'                => 'Adeo\Sirius\CustomerUpdateService\Type\UsagePreferenceOtherInDTO',
        'setUsagePreferenceResponse'               => 'Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreferenceResponse',
        'setCommunications'                        => 'Adeo\Sirius\CustomerUpdateService\Type\SetCommunications',
        'naturalPersonCommunicationDTO'            => 'Adeo\Sirius\CustomerUpdateService\Type\NaturalPersonCommunicationDTO',
        'setCommunicationDTO'                      => 'Adeo\Sirius\CustomerUpdateService\Type\SetCommunicationDTO',
        'communicationScopeOutDTO'                 => 'Adeo\Sirius\CustomerUpdateService\Type\CommunicationScopeOutDTO',
        'specificOptinInDTO'                       => 'Adeo\Sirius\CustomerUpdateService\Type\SpecificOptinInDTO',
        'communicationTypeOptInInformationInDTO'   => 'Adeo\Sirius\CustomerUpdateService\Type\CommunicationTypeOptInInformationInDTO',
        'setCommunicationsResponse'                => 'Adeo\Sirius\CustomerUpdateService\Type\SetCommunicationsResponse',
        'delCustomerLock'                          => 'Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLock',
        'deleteCustomerLockDTO'                    => 'Adeo\Sirius\CustomerUpdateService\Type\DeleteCustomerLockDTO',
        'delCustomerLockResponse'                  => 'Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLockResponse',
        'setLegalEntityContacts'                   => 'Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContacts',
        'setLegalEntityContactsDTO'                => 'Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContactsDTO',
        'legalEntityContactDTO'                    => 'Adeo\Sirius\CustomerUpdateService\Type\LegalEntityContactDTO',
        'setLegalEntityContactsResponse'           => 'Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContactsResponse',
        'delAddressAndHousing'                     => 'Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousing',
        'deleteAddressDTO'                         => 'Adeo\Sirius\CustomerUpdateService\Type\DeleteAddressDTO',
        'delAddressAndHousingResponse'             => 'Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousingResponse',
        'setCommunities'                           => 'Adeo\Sirius\CustomerUpdateService\Type\SetCommunities',
        'customerCommunitiesDTO'                   => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerCommunitiesDTO',
        'communityInformationInDTO'                => 'Adeo\Sirius\CustomerUpdateService\Type\CommunityInformationInDTO',
        'setCommunitiesResponse'                   => 'Adeo\Sirius\CustomerUpdateService\Type\SetCommunitiesResponse',
        'setExternalIdentifiers'                   => 'Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiers',
        'customerExternalIdentifiersDTO'           => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerExternalIdentifiersDTO',
        'externalIdentifierInformationInDTO'       => 'Adeo\Sirius\CustomerUpdateService\Type\ExternalIdentifierInformationInDTO',
        'setExternalIdentifiersResponse'           => 'Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiersResponse',
        'updateNotePad'                            => 'Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePad',
        'customerMessageDTO'                       => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerMessageDTO',
        'updateNotePadResponse'                    => 'Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePadResponse',
        'createNotePad'                            => 'Adeo\Sirius\CustomerUpdateService\Type\CreateNotePad',
        'createNotePadResponse'                    => 'Adeo\Sirius\CustomerUpdateService\Type\CreateNotePadResponse',
        'setClassifications'                       => 'Adeo\Sirius\CustomerUpdateService\Type\SetClassifications',
        'customerClassificationDTO'                => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerClassificationDTO',
        'classificationInformationInDTO'           => 'Adeo\Sirius\CustomerUpdateService\Type\ClassificationInformationInDTO',
        'setClassificationsResponse'               => 'Adeo\Sirius\CustomerUpdateService\Type\SetClassificationsResponse',
        'moveNaturalPersonCustomer'                => 'Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomer',
        'moveNaturalPersonCustomerResponse'        => 'Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomerResponse',
        'setCustomerLock'                          => 'Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLock',
        'creationCustomerLockDTO'                  => 'Adeo\Sirius\CustomerUpdateService\Type\CreationCustomerLockDTO',
        'setCustomerLockResponse'                  => 'Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLockResponse',
        'updAddressAndHousing'                     => 'Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousing',
        'customerAddressDTO'                       => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerAddressDTO',
        'addressInformationInDTO'                  => 'Adeo\Sirius\CustomerUpdateService\Type\AddressInformationInDTO',
        'addressTypeInDTO'                         => 'Adeo\Sirius\CustomerUpdateService\Type\AddressTypeInDTO',
        'customerHousingInDTO'                     => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerHousingInDTO',
        'customerHousingCharacteristicInDTO'       => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerHousingCharacteristicInDTO',
        'addressExternalCodeInDTO'                 => 'Adeo\Sirius\CustomerUpdateService\Type\AddressExternalCodeInDTO',
        'updAddressAndHousingResponse'             => 'Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousingResponse',
        'updAddress'                               => 'Adeo\Sirius\CustomerUpdateService\Type\UpdAddress',
        'updAddressResponse'                       => 'Adeo\Sirius\CustomerUpdateService\Type\UpdAddressResponse',
        'createAddressAndHousing'                  => 'Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousing',
        'createAddressAndHousingResponse'          => 'Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousingResponse',
        'delNotePad'                               => 'Adeo\Sirius\CustomerUpdateService\Type\DelNotePad',
        'deleteNotePadDTO'                         => 'Adeo\Sirius\CustomerUpdateService\Type\DeleteNotePadDTO',
        'delNotePadResponse'                       => 'Adeo\Sirius\CustomerUpdateService\Type\DelNotePadResponse',
        'updNaturalPersonCustomerIdentity'         => 'Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentity',
        'naturalPersonCustomerIdentityDTO'         => 'Adeo\Sirius\CustomerUpdateService\Type\NaturalPersonCustomerIdentityDTO',
        'updNaturalPersonCustomerIdentityResponse' => 'Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentityResponse',
        'setSegmentations'                         => 'Adeo\Sirius\CustomerUpdateService\Type\SetSegmentations',
        'customerSegmentationsDTO'                 => 'Adeo\Sirius\CustomerUpdateService\Type\CustomerSegmentationsDTO',
        'segmentationInformationInDTO'             => 'Adeo\Sirius\CustomerUpdateService\Type\SegmentationInformationInDTO',
        'setSegmentationsResponse'                 => 'Adeo\Sirius\CustomerUpdateService\Type\SetSegmentationsResponse',
        'SiriusBusinessFault'                      => 'Adeo\Sirius\CustomerUpdateService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method updLegalEntityCustomerIdentity
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdLegalEntityCustomerIdentityResponse
     */
    public function _updLegalEntityCustomerIdentity(UpdLegalEntityCustomerIdentity $parameters)
    {
        return $this->__soapCall("updLegalEntityCustomerIdentity", array($parameters));
    }
    
    /**
     * Method setChildren
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetChildren $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetChildrenResponse
     */
    public function _setChildren(SetChildren $parameters)
    {
        return $this->__soapCall("setChildren", array($parameters));
    }
    
    /**
     * Method setLeisures
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetLeisures $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetLeisuresResponse
     */
    public function _setLeisures(SetLeisures $parameters)
    {
        return $this->__soapCall("setLeisures", array($parameters));
    }
    
    /**
     * Method setCustomerUseContext
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContext $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerUseContextResponse
     */
    public function _setCustomerUseContext(SetCustomerUseContext $parameters)
    {
        return $this->__soapCall("setCustomerUseContext", array($parameters));
    }
    
    /**
     * Method setUsagePreference
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreference $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetUsagePreferenceResponse
     */
    public function _setUsagePreference(SetUsagePreference $parameters)
    {
        return $this->__soapCall("setUsagePreference", array($parameters));
    }
    
    /**
     * Method setCommunications
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCommunications $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCommunicationsResponse
     */
    public function _setCommunications(SetCommunications $parameters)
    {
        return $this->__soapCall("setCommunications", array($parameters));
    }
    
    /**
     * Method delCustomerLock
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLock $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelCustomerLockResponse
     */
    public function _delCustomerLock(DelCustomerLock $parameters)
    {
        return $this->__soapCall("delCustomerLock", array($parameters));
    }
    
    /**
     * Method setLegalEntityContacts
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContacts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetLegalEntityContactsResponse
     */
    public function _setLegalEntityContacts(SetLegalEntityContacts $parameters)
    {
        return $this->__soapCall("setLegalEntityContacts", array($parameters));
    }
    
    /**
     * Method delAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousing $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelAddressAndHousingResponse
     */
    public function _delAddressAndHousing(DelAddressAndHousing $parameters)
    {
        return $this->__soapCall("delAddressAndHousing", array($parameters));
    }
    
    /**
     * Method setCommunities
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCommunities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCommunitiesResponse
     */
    public function _setCommunities(SetCommunities $parameters)
    {
        return $this->__soapCall("setCommunities", array($parameters));
    }
    
    /**
     * Method setExternalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetExternalIdentifiersResponse
     */
    public function _setExternalIdentifiers(SetExternalIdentifiers $parameters)
    {
        return $this->__soapCall("setExternalIdentifiers", array($parameters));
    }
    
    /**
     * Method updateNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdateNotePadResponse
     */
    public function _updateNotePad(UpdateNotePad $parameters)
    {
        return $this->__soapCall("updateNotePad", array($parameters));
    }
    
    /**
     * Method createNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\CreateNotePadResponse
     */
    public function _createNotePad(CreateNotePad $parameters)
    {
        return $this->__soapCall("createNotePad", array($parameters));
    }
    
    /**
     * Method setClassifications
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetClassifications $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetClassificationsResponse
     */
    public function _setClassifications(SetClassifications $parameters)
    {
        return $this->__soapCall("setClassifications", array($parameters));
    }
    
    /**
     * Method moveNaturalPersonCustomer
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\MoveNaturalPersonCustomerResponse
     */
    public function _moveNaturalPersonCustomer(MoveNaturalPersonCustomer $parameters)
    {
        return $this->__soapCall("moveNaturalPersonCustomer", array($parameters));
    }
    
    /**
     * Method setCustomerLock
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLock $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetCustomerLockResponse
     */
    public function _setCustomerLock(SetCustomerLock $parameters)
    {
        return $this->__soapCall("setCustomerLock", array($parameters));
    }
    
    /**
     * Method updAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousing $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressAndHousingResponse
     */
    public function _updAddressAndHousing(UpdAddressAndHousing $parameters)
    {
        return $this->__soapCall("updAddressAndHousing", array($parameters));
    }
    
    /**
     * Method updAddress
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdAddress $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdAddressResponse
     */
    public function _updAddress(UpdAddress $parameters)
    {
        return $this->__soapCall("updAddress", array($parameters));
    }
    
    /**
     * Method createAddressAndHousing
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousing $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\CreateAddressAndHousingResponse
     */
    public function _createAddressAndHousing(CreateAddressAndHousing $parameters)
    {
        return $this->__soapCall("createAddressAndHousing", array($parameters));
    }
    
    /**
     * Method delNotePad
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\DelNotePad $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\DelNotePadResponse
     */
    public function _delNotePad(DelNotePad $parameters)
    {
        return $this->__soapCall("delNotePad", array($parameters));
    }
    
    /**
     * Method updNaturalPersonCustomerIdentity
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\UpdNaturalPersonCustomerIdentityResponse
     */
    public function _updNaturalPersonCustomerIdentity(UpdNaturalPersonCustomerIdentity $parameters)
    {
        return $this->__soapCall("updNaturalPersonCustomerIdentity", array($parameters));
    }
    
    /**
     * Method setSegmentations
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentations $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\SetSegmentationsResponse
     */
    public function _setSegmentations(SetSegmentations $parameters)
    {
        return $this->__soapCall("setSegmentations", array($parameters));
    }
}
