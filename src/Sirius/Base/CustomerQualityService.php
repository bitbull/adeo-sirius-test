<?php
/**
 * Parent model for proxy for CustomerQualityService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneaway;
use \Adeo\Sirius\CustomerQualityService\Type\GetHistory;
use \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomer;
use \Adeo\Sirius\CustomerQualityService\Type\DelCustomer;

abstract class CustomerQualityService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'updAddressGoneaway'              => 'Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneaway',
        'goneawayCustomerAddressDTO'      => 'Adeo\Sirius\CustomerQualityService\Type\GoneawayCustomerAddressDTO',
        'abstractDTO'                     => 'Adeo\Sirius\CustomerQualityService\Type\AbstractDTO',
        'historyInputDTO'                 => 'Adeo\Sirius\CustomerQualityService\Type\HistoryInputDTO',
        'updAddressGoneawayResponse'      => 'Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneawayResponse',
        'getHistory'                      => 'Adeo\Sirius\CustomerQualityService\Type\GetHistory',
        'customerIdentificationDTO'       => 'Adeo\Sirius\CustomerQualityService\Type\CustomerIdentificationDTO',
        'getHistoryResponse'              => 'Adeo\Sirius\CustomerQualityService\Type\GetHistoryResponse',
        'historyOutDTO'                   => 'Adeo\Sirius\CustomerQualityService\Type\HistoryOutDTO',
        'historyInformationOutDTO'        => 'Adeo\Sirius\CustomerQualityService\Type\HistoryInformationOutDTO',
        'reactivateCustomer'              => 'Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomer',
        'updateCustomerIdentificationDTO' => 'Adeo\Sirius\CustomerQualityService\Type\UpdateCustomerIdentificationDTO',
        'reactivateCustomerResponse'      => 'Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomerResponse',
        'delCustomer'                     => 'Adeo\Sirius\CustomerQualityService\Type\DelCustomer',
        'delCustomerResponse'             => 'Adeo\Sirius\CustomerQualityService\Type\DelCustomerResponse',
        'SiriusBusinessFault'             => 'Adeo\Sirius\CustomerQualityService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method updAddressGoneaway
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneaway $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneawayResponse
     */
    public function _updAddressGoneaway(UpdAddressGoneaway $parameters)
    {
        return $this->__soapCall("updAddressGoneaway", array($parameters));
    }
    
    /**
     * Method getHistory
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\GetHistory $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\GetHistoryResponse
     */
    public function _getHistory(GetHistory $parameters)
    {
        return $this->__soapCall("getHistory", array($parameters));
    }
    
    /**
     * Method reactivateCustomer
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomerResponse
     */
    public function _reactivateCustomer(ReactivateCustomer $parameters)
    {
        return $this->__soapCall("reactivateCustomer", array($parameters));
    }
    
    /**
     * Method delCustomer
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\DelCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\DelCustomerResponse
     */
    public function _delCustomer(DelCustomer $parameters)
    {
        return $this->__soapCall("delCustomer", array($parameters));
    }
}
