<?php
/**
 * Parent model for proxy for CustomerPreExistingSearchService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomer;

abstract class CustomerPreExistingSearchService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'searchPreExistingCustomer'                          => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomer',
        'customerPreExistingSearchCriteriaDTO'               => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingSearchCriteriaDTO',
        'abstractDTO'                                        => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\AbstractDTO',
        'customerPreExistingAddressCriteriaDTO'              => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingAddressCriteriaDTO',
        'customerPreExistingAddressLineCriterionDTO'         => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingAddressLineCriterionDTO',
        'customerPreExistingAddressExternalCodeCriterionDTO' => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingAddressExternalCodeCriterionDTO',
        'customerPreExistingDiscriminatingCriteriaDTO'       => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingDiscriminatingCriteriaDTO',
        'customerPreExistingClassificationCriterionDTO'      => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingClassificationCriterionDTO',
        'customerPreExistingExternalIdentifierCriterionDTO'  => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingExternalIdentifierCriterionDTO',
        'searchPreExistingCustomerResponse'                  => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomerResponse',
        'customerPreExistingSearchResponseDTO'               => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingSearchResponseDTO',
        'customerPreExistingCustomerListDTO'                 => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingCustomerListDTO',
        'customerPreExistingMainInformationDTO'              => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingMainInformationDTO',
        'customerMainInformationDTO'                         => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerMainInformationDTO',
        'commonCustomerMainInformationDTO'                   => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CommonCustomerMainInformationDTO',
        'addressMainInformationDTO'                          => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\AddressMainInformationDTO',
        'commonAddressMainInformationDTO'                    => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CommonAddressMainInformationDTO',
        'externalIdentifierInformationOutDTO'                => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\ExternalIdentifierInformationOutDTO',
        'classificationInformationOutDTO'                    => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\ClassificationInformationOutDTO',
        'communicationMainInformationDTO'                    => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\CommunicationMainInformationDTO',
        'segmentationInformationOutDTO'                      => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\SegmentationInformationOutDTO',
        'SiriusBusinessFault'                                => 'Adeo\Sirius\CustomerPreExistingSearchService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method searchPreExistingCustomer
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomerResponse
     */
    public function _searchPreExistingCustomer(SearchPreExistingCustomer $parameters)
    {
        return $this->__soapCall("searchPreExistingCustomer", array($parameters));
    }
}
