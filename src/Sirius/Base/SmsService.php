<?php
/**
 * Parent model for proxy for SmsService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\SmsService\Type\SendSMSRequest;

abstract class SmsService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'SendSMSRequest'       => 'Adeo\Sirius\SmsService\Type\SendSMSRequest',
        'SendSMSResponse'      => 'Adeo\Sirius\SmsService\Type\SendSMSResponse',
        'SendSMSRequestHeader' => 'Adeo\Sirius\SmsService\Type\SendSMSRequestHeader',
        'Message'              => 'Adeo\Sirius\SmsService\Type\Message',
        'Recipient'            => 'Adeo\Sirius\SmsService\Type\Recipient',
        'RecipientArg'         => 'Adeo\Sirius\SmsService\Type\RecipientArg',
        'SendSMSFault'         => 'Adeo\Sirius\SmsService\Type\SendSMSFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method sendSMS
     *
     * @param \Adeo\Sirius\SmsService\Type\SendSMSRequest $parameters Value of parameters
     * 
     * @return mixed
     */
    public function _sendSMS(SendSMSRequest $parameters)
    {
        return $this->__soapCall("sendSMS", array($parameters));
    }
}
