<?php
/**
 * Parent model for proxy for AuthentService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByte;
use \Adeo\Sirius\AuthentService\Type\DoRequestLDAP;
use \Adeo\Sirius\AuthentService\Type\DoAuthentification;
use \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByte;
use \Adeo\Sirius\AuthentService\Type\Ping;

abstract class AuthentService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'doAuthentificationToByte'          => 'Adeo\Sirius\AuthentService\Type\DoAuthentificationToByte',
        'doAuthentificationToByteResponse'  => 'Adeo\Sirius\AuthentService\Type\DoAuthentificationToByteResponse',
        'doRequestLDAP'                     => 'Adeo\Sirius\AuthentService\Type\DoRequestLDAP',
        'string2string2ArrayOfStringMapMap' => 'Adeo\Sirius\AuthentService\Type\String2string2ArrayOfStringMapMap',
        'entry'                             => 'Adeo\Sirius\AuthentService\Type\Entry',
        'string2ArrayOfStringMap'           => 'Adeo\Sirius\AuthentService\Type\String2ArrayOfStringMap',
        'ArrayOfString'                     => 'Adeo\Sirius\AuthentService\Type\ArrayOfString',
        'doRequestLDAPResponse'             => 'Adeo\Sirius\AuthentService\Type\DoRequestLDAPResponse',
        'doAuthentification'                => 'Adeo\Sirius\AuthentService\Type\DoAuthentification',
        'doAuthentificationResponse'        => 'Adeo\Sirius\AuthentService\Type\DoAuthentificationResponse',
        'doRequestLDAPToByte'               => 'Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByte',
        'doRequestLDAPToByteResponse'       => 'Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByteResponse',
        'ping'                              => 'Adeo\Sirius\AuthentService\Type\Ping',
        'anyType2anyTypeMap'                => 'Adeo\Sirius\AuthentService\Type\AnyType2anyTypeMap',
        'pingResponse'                      => 'Adeo\Sirius\AuthentService\Type\PingResponse',
        'AuthServExceptionDetail'           => 'Adeo\Sirius\AuthentService\Type\AuthServExceptionDetail',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method doAuthentificationToByte
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByte $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByteResponse
     */
    public function _doAuthentificationToByte(DoAuthentificationToByte $parameters)
    {
        return $this->__soapCall("doAuthentificationToByte", array($parameters));
    }
    
    /**
     * Method doRequestLDAP
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoRequestLDAP $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoRequestLDAPResponse
     */
    public function _doRequestLDAP(DoRequestLDAP $parameters)
    {
        return $this->__soapCall("doRequestLDAP", array($parameters));
    }
    
    /**
     * Method doAuthentification
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoAuthentification $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoAuthentificationResponse
     */
    public function _doAuthentification(DoAuthentification $parameters)
    {
        return $this->__soapCall("doAuthentification", array($parameters));
    }
    
    /**
     * Method doRequestLDAPToByte
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByte $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByteResponse
     */
    public function _doRequestLDAPToByte(DoRequestLDAPToByte $parameters)
    {
        return $this->__soapCall("doRequestLDAPToByte", array($parameters));
    }
    
    /**
     * Method ping
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Ping $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\PingResponse
     */
    public function _ping(Ping $parameters)
    {
        return $this->__soapCall("ping", array($parameters));
    }
}
