<?php
/**
 * Parent model for proxy for CustomerReceiptService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaders;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceipts;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceipt;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMag;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNum;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDate;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaders;

abstract class CustomerReceiptService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'SearchDetailedReceiptHeaderRequest'   => 'Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaderRequest',
        'ArrayOfString'                        => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfString',
        'ArrayOfLong'                          => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfLong',
        'searchDetailedReceiptHeaders'         => 'Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaders',
        'SearchReceiptHeaderResponse'          => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaderResponse',
        'searchDetailedReceiptHeadersResponse' => 'Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeadersResponse',
        'GetReceiptsRequest'                   => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptsRequest',
        'getReceipts'                          => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceipts',
        'GetReceiptsResponse'                  => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptsResponse',
        'getReceiptsResponse'                  => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptsResponse',
        'GetReceiptRequest'                    => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptRequest',
        'getReceipt'                           => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceipt',
        'GetReceiptResponse'                   => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptResponse',
        'getReceiptResponse'                   => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptResponse',
        'SearchReceiptByMagRequest'            => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMagRequest',
        'searchReceiptByMag'                   => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMag',
        'searchReceiptByMagResponse'           => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMagResponse',
        'GetReceiptByNumRequest'               => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNumRequest',
        'getReceiptByNum'                      => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNum',
        'getReceiptByNumResponse'              => 'Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNumResponse',
        'SearchReceiptHeaderByDateRequest'     => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaderByDateRequest',
        'searchReceiptHeadersByDate'           => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDate',
        'searchReceiptHeadersByDateResponse'   => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDateResponse',
        'SearchReceiptHeaderRequest'           => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaderRequest',
        'searchReceiptHeaders'                 => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaders',
        'searchReceiptHeadersResponse'         => 'Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersResponse',
        'ArrayOfReceiptHeaderWS'               => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfReceiptHeaderWS',
        'ReceiptHeaderWS'                      => 'Adeo\Sirius\CustomerReceiptService\Type\ReceiptHeaderWS',
        'ArrayOfPaymentTypeWS'                 => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfPaymentTypeWS',
        'PaymentTypeWS'                        => 'Adeo\Sirius\CustomerReceiptService\Type\PaymentTypeWS',
        'ArrayOfReceiptIdWS'                   => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfReceiptIdWS',
        'ReceiptIdWS'                          => 'Adeo\Sirius\CustomerReceiptService\Type\ReceiptIdWS',
        'ArrayOfReceiptLineWS'                 => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfReceiptLineWS',
        'ReceiptLineWS'                        => 'Adeo\Sirius\CustomerReceiptService\Type\ReceiptLineWS',
        'TransactionWS'                        => 'Adeo\Sirius\CustomerReceiptService\Type\TransactionWS',
        'EntityWS'                             => 'Adeo\Sirius\CustomerReceiptService\Type\EntityWS',
        'VATRateWS'                            => 'Adeo\Sirius\CustomerReceiptService\Type\VATRateWS',
        'ArrayOfDiscountWS'                    => 'Adeo\Sirius\CustomerReceiptService\Type\ArrayOfDiscountWS',
        'DiscountWS'                           => 'Adeo\Sirius\CustomerReceiptService\Type\DiscountWS',
        'AmountWS'                             => 'Adeo\Sirius\CustomerReceiptService\Type\AmountWS',
        'ItemWS'                               => 'Adeo\Sirius\CustomerReceiptService\Type\ItemWS',
        'SimpleDepartmentWS'                   => 'Adeo\Sirius\CustomerReceiptService\Type\SimpleDepartmentWS',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method searchDetailedReceiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaders $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeadersResponse
     */
    public function _searchDetailedReceiptHeaders(SearchDetailedReceiptHeaders $parameters)
    {
        return $this->__soapCall("searchDetailedReceiptHeaders", array($parameters));
    }
    
    /**
     * Method getReceipts
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceipts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptsResponse
     */
    public function _getReceipts(GetReceipts $parameters)
    {
        return $this->__soapCall("getReceipts", array($parameters));
    }
    
    /**
     * Method getReceipt
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceipt $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptResponse
     */
    public function _getReceipt(GetReceipt $parameters)
    {
        return $this->__soapCall("getReceipt", array($parameters));
    }
    
    /**
     * Method searchReceiptByMag
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMag $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMagResponse
     */
    public function _searchReceiptByMag(SearchReceiptByMag $parameters)
    {
        return $this->__soapCall("searchReceiptByMag", array($parameters));
    }
    
    /**
     * Method getReceiptByNum
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNum $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNumResponse
     */
    public function _getReceiptByNum(GetReceiptByNum $parameters)
    {
        return $this->__soapCall("getReceiptByNum", array($parameters));
    }
    
    /**
     * Method searchReceiptHeadersByDate
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDate $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDateResponse
     */
    public function _searchReceiptHeadersByDate(SearchReceiptHeadersByDate $parameters)
    {
        return $this->__soapCall("searchReceiptHeadersByDate", array($parameters));
    }
    
    /**
     * Method searchReceiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaders $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersResponse
     */
    public function _searchReceiptHeaders(SearchReceiptHeaders $parameters)
    {
        return $this->__soapCall("searchReceiptHeaders", array($parameters));
    }
}
