<?php
/**
 * Parent model for proxy for CustomerSearchService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerSearchService\Type\SearchCustomers;
use \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroups;
use \Adeo\Sirius\CustomerSearchService\Type\SearchContacts;

abstract class CustomerSearchService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'searchCustomers'                     => 'Adeo\Sirius\CustomerSearchService\Type\SearchCustomers',
        'customerSearchCriteriaDTO'           => 'Adeo\Sirius\CustomerSearchService\Type\CustomerSearchCriteriaDTO',
        'abstractDTO'                         => 'Adeo\Sirius\CustomerSearchService\Type\AbstractDTO',
        'searchCustomersResponse'             => 'Adeo\Sirius\CustomerSearchService\Type\SearchCustomersResponse',
        'customerListDTO'                     => 'Adeo\Sirius\CustomerSearchService\Type\CustomerListDTO',
        'customerMainInformationDTO'          => 'Adeo\Sirius\CustomerSearchService\Type\CustomerMainInformationDTO',
        'addressMainInformationDTO'           => 'Adeo\Sirius\CustomerSearchService\Type\AddressMainInformationDTO',
        'externalIdentifierInformationOutDTO' => 'Adeo\Sirius\CustomerSearchService\Type\ExternalIdentifierInformationOutDTO',
        'segmentationInformationOutDTO'       => 'Adeo\Sirius\CustomerSearchService\Type\SegmentationInformationOutDTO',
        'communicationMainInformationDTO'     => 'Adeo\Sirius\CustomerSearchService\Type\CommunicationMainInformationDTO',
        'classificationInformationOutDTO'     => 'Adeo\Sirius\CustomerSearchService\Type\ClassificationInformationOutDTO',
        'searchCustomerGroups'                => 'Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroups',
        'customerGroupSearchCriteriaDTO'      => 'Adeo\Sirius\CustomerSearchService\Type\CustomerGroupSearchCriteriaDTO',
        'searchCustomerGroupsResponse'        => 'Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroupsResponse',
        'customerGrouplistDTO'                => 'Adeo\Sirius\CustomerSearchService\Type\CustomerGrouplistDTO',
        'groupDTO'                            => 'Adeo\Sirius\CustomerSearchService\Type\GroupDTO',
        'naturalPersonMainInformationDTO'     => 'Adeo\Sirius\CustomerSearchService\Type\NaturalPersonMainInformationDTO',
        'legalEntityMainInformationDTO'       => 'Adeo\Sirius\CustomerSearchService\Type\LegalEntityMainInformationDTO',
        'searchContacts'                      => 'Adeo\Sirius\CustomerSearchService\Type\SearchContacts',
        'searchContactsResponse'              => 'Adeo\Sirius\CustomerSearchService\Type\SearchContactsResponse',
        'SiriusBusinessFault'                 => 'Adeo\Sirius\CustomerSearchService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method searchCustomers
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchCustomers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchCustomersResponse
     */
    public function _searchCustomers(SearchCustomers $parameters)
    {
        return $this->__soapCall("searchCustomers", array($parameters));
    }
    
    /**
     * Method searchCustomerGroups
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroups $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroupsResponse
     */
    public function _searchCustomerGroups(SearchCustomerGroups $parameters)
    {
        return $this->__soapCall("searchCustomerGroups", array($parameters));
    }
    
    /**
     * Method searchContacts
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchContacts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchContactsResponse
     */
    public function _searchContacts(SearchContacts $parameters)
    {
        return $this->__soapCall("searchContacts", array($parameters));
    }
}
