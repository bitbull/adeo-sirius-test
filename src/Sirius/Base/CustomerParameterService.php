<?php
/**
 * Parent model for proxy for CustomerParameterService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerParameterService\Type\GetAllProperties;

abstract class CustomerParameterService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'getAllProperties'         => 'Adeo\Sirius\CustomerParameterService\Type\GetAllProperties',
        'getAllPropertiesResponse' => 'Adeo\Sirius\CustomerParameterService\Type\GetAllPropertiesResponse',
        'scrParameterDTO'          => 'Adeo\Sirius\CustomerParameterService\Type\ScrParameterDTO',
        'abstractDTO'              => 'Adeo\Sirius\CustomerParameterService\Type\AbstractDTO',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method getAllProperties
     * 
     * @param \Adeo\Sirius\CustomerParameterService\Type\GetAllProperties $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\GetAllPropertiesResponse
     */
    public function _getAllProperties(GetAllProperties $parameters)
    {
        return $this->__soapCall("getAllProperties", array($parameters));
    }
}
