<?php
/**
 * Parent model for proxy for CustomerGetService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerGetService\Type\GetCustomer;
use \Adeo\Sirius\CustomerGetService\Type\GetSegmentations;
use \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiers;
use \Adeo\Sirius\CustomerGetService\Type\GetChildren;
use \Adeo\Sirius\CustomerGetService\Type\GetCommunities;
use \Adeo\Sirius\CustomerGetService\Type\GetNotePads;
use \Adeo\Sirius\CustomerGetService\Type\GetLeisures;
use \Adeo\Sirius\CustomerGetService\Type\GetUsagePreference;
use \Adeo\Sirius\CustomerGetService\Type\GetSlaveMaster;
use \Adeo\Sirius\CustomerGetService\Type\GetHousehold;

abstract class CustomerGetService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'getCustomer'                             => 'Adeo\Sirius\CustomerGetService\Type\GetCustomer',
        'customerIdentificationDTO'               => 'Adeo\Sirius\CustomerGetService\Type\CustomerIdentificationDTO',
        'abstractDTO'                             => 'Adeo\Sirius\CustomerGetService\Type\AbstractDTO',
        'historyInputDTO'                         => 'Adeo\Sirius\CustomerGetService\Type\HistoryInputDTO',
        'getCustomerResponse'                     => 'Adeo\Sirius\CustomerGetService\Type\GetCustomerResponse',
        'customerOutDTO'                          => 'Adeo\Sirius\CustomerGetService\Type\CustomerOutDTO',
        'addressInformationOutDTO'                => 'Adeo\Sirius\CustomerGetService\Type\AddressInformationOutDTO',
        'addressTypeOutDTO'                       => 'Adeo\Sirius\CustomerGetService\Type\AddressTypeOutDTO',
        'abstractModelObject'                     => 'Adeo\Sirius\CustomerGetService\Type\AbstractModelObject',
        'specificOptinOutDTO'                     => 'Adeo\Sirius\CustomerGetService\Type\SpecificOptinOutDTO',
        'AddressExternalCodeOut'                  => 'Adeo\Sirius\CustomerGetService\Type\AddressExternalCodeOut',
        'customerHousingOutDTO'                   => 'Adeo\Sirius\CustomerGetService\Type\CustomerHousingOutDTO',
        'customerHousingCharacteristicOutDTO'     => 'Adeo\Sirius\CustomerGetService\Type\CustomerHousingCharacteristicOutDTO',
        'legalEntityInformationOutDTO'            => 'Adeo\Sirius\CustomerGetService\Type\LegalEntityInformationOutDTO',
        'customerInformationOutDTO'               => 'Adeo\Sirius\CustomerGetService\Type\CustomerInformationOutDTO',
        'contactInformationOutDTO'                => 'Adeo\Sirius\CustomerGetService\Type\ContactInformationOutDTO',
        'communicationInformationOutDTO'          => 'Adeo\Sirius\CustomerGetService\Type\CommunicationInformationOutDTO',
        'communicationScopeOutDTO'                => 'Adeo\Sirius\CustomerGetService\Type\CommunicationScopeOutDTO',
        'classificationInformationOutDTO'         => 'Adeo\Sirius\CustomerGetService\Type\ClassificationInformationOutDTO',
        'externalIdentifierInformationOutDTO'     => 'Adeo\Sirius\CustomerGetService\Type\ExternalIdentifierInformationOutDTO',
        'segmentationInformationOutDTO'           => 'Adeo\Sirius\CustomerGetService\Type\SegmentationInformationOutDTO',
        'globalOptInInformationOutDTO'            => 'Adeo\Sirius\CustomerGetService\Type\GlobalOptInInformationOutDTO',
        'communicationTypeOptInInformationOutDTO' => 'Adeo\Sirius\CustomerGetService\Type\CommunicationTypeOptInInformationOutDTO',
        'customerLockOutDTO'                      => 'Adeo\Sirius\CustomerGetService\Type\CustomerLockOutDTO',
        'getSegmentations'                        => 'Adeo\Sirius\CustomerGetService\Type\GetSegmentations',
        'getSegmentationsResponse'                => 'Adeo\Sirius\CustomerGetService\Type\GetSegmentationsResponse',
        'segmentationsOutputDTO'                  => 'Adeo\Sirius\CustomerGetService\Type\SegmentationsOutputDTO',
        'getExternalIdentifiers'                  => 'Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiers',
        'getExternalIdentifiersResponse'          => 'Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiersResponse',
        'externalIdentifiersOutputDTO'            => 'Adeo\Sirius\CustomerGetService\Type\ExternalIdentifiersOutputDTO',
        'getChildren'                             => 'Adeo\Sirius\CustomerGetService\Type\GetChildren',
        'getChildrenResponse'                     => 'Adeo\Sirius\CustomerGetService\Type\GetChildrenResponse',
        'childDTO'                                => 'Adeo\Sirius\CustomerGetService\Type\ChildDTO',
        'getCommunities'                          => 'Adeo\Sirius\CustomerGetService\Type\GetCommunities',
        'getCommunitiesResponse'                  => 'Adeo\Sirius\CustomerGetService\Type\GetCommunitiesResponse',
        'communitiesOutputDTO'                    => 'Adeo\Sirius\CustomerGetService\Type\CommunitiesOutputDTO',
        'communityInformationOutDTO'              => 'Adeo\Sirius\CustomerGetService\Type\CommunityInformationOutDTO',
        'getNotePads'                             => 'Adeo\Sirius\CustomerGetService\Type\GetNotePads',
        'getNotePadsResponse'                     => 'Adeo\Sirius\CustomerGetService\Type\GetNotePadsResponse',
        'notePadsOutputDTO'                       => 'Adeo\Sirius\CustomerGetService\Type\NotePadsOutputDTO',
        'notePadInformationDTO'                   => 'Adeo\Sirius\CustomerGetService\Type\NotePadInformationDTO',
        'getLeisures'                             => 'Adeo\Sirius\CustomerGetService\Type\GetLeisures',
        'getLeisuresResponse'                     => 'Adeo\Sirius\CustomerGetService\Type\GetLeisuresResponse',
        'leisuresOutputDTO'                       => 'Adeo\Sirius\CustomerGetService\Type\LeisuresOutputDTO',
        'leisureInformationOutDTO'                => 'Adeo\Sirius\CustomerGetService\Type\LeisureInformationOutDTO',
        'getUsagePreference'                      => 'Adeo\Sirius\CustomerGetService\Type\GetUsagePreference',
        'usagePreferenceIdentificationDTO'        => 'Adeo\Sirius\CustomerGetService\Type\UsagePreferenceIdentificationDTO',
        'getUsagePreferenceResponse'              => 'Adeo\Sirius\CustomerGetService\Type\GetUsagePreferenceResponse',
        'usagePreferenceOutDTO'                   => 'Adeo\Sirius\CustomerGetService\Type\UsagePreferenceOutDTO',
        'addressDetailDTO'                        => 'Adeo\Sirius\CustomerGetService\Type\AddressDetailDTO',
        'usagePreferenceOtherOutDTO'              => 'Adeo\Sirius\CustomerGetService\Type\UsagePreferenceOtherOutDTO',
        'usagePreferenceTypeDTO'                  => 'Adeo\Sirius\CustomerGetService\Type\UsagePreferenceTypeDTO',
        'getSlaveMaster'                          => 'Adeo\Sirius\CustomerGetService\Type\GetSlaveMaster',
        'getSlaveMasterResponse'                  => 'Adeo\Sirius\CustomerGetService\Type\GetSlaveMasterResponse',
        'slaveMasterDTO'                          => 'Adeo\Sirius\CustomerGetService\Type\SlaveMasterDTO',
        'getHousehold'                            => 'Adeo\Sirius\CustomerGetService\Type\GetHousehold',
        'householdIdentificationDTO'              => 'Adeo\Sirius\CustomerGetService\Type\HouseholdIdentificationDTO',
        'getHouseholdResponse'                    => 'Adeo\Sirius\CustomerGetService\Type\GetHouseholdResponse',
        'householdOutDTO'                         => 'Adeo\Sirius\CustomerGetService\Type\HouseholdOutDTO',
        'SiriusBusinessFault'                     => 'Adeo\Sirius\CustomerGetService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method getCustomer
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetCustomerResponse
     */
    public function _getCustomer(GetCustomer $parameters)
    {
        return $this->__soapCall("getCustomer", array($parameters));
    }
    
    /**
     * Method getSegmentations
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetSegmentations $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetSegmentationsResponse
     */
    public function _getSegmentations(GetSegmentations $parameters)
    {
        return $this->__soapCall("getSegmentations", array($parameters));
    }
    
    /**
     * Method getExternalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetExternalIdentifiersResponse
     */
    public function _getExternalIdentifiers(GetExternalIdentifiers $parameters)
    {
        return $this->__soapCall("getExternalIdentifiers", array($parameters));
    }
    
    /**
     * Method getChildren
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetChildren $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetChildrenResponse
     */
    public function _getChildren(GetChildren $parameters)
    {
        return $this->__soapCall("getChildren", array($parameters));
    }
    
    /**
     * Method getCommunities
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetCommunities $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetCommunitiesResponse
     */
    public function _getCommunities(GetCommunities $parameters)
    {
        return $this->__soapCall("getCommunities", array($parameters));
    }
    
    /**
     * Method getNotePads
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetNotePads $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetNotePadsResponse
     */
    public function _getNotePads(GetNotePads $parameters)
    {
        return $this->__soapCall("getNotePads", array($parameters));
    }
    
    /**
     * Method getLeisures
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetLeisures $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetLeisuresResponse
     */
    public function _getLeisures(GetLeisures $parameters)
    {
        return $this->__soapCall("getLeisures", array($parameters));
    }
    
    /**
     * Method getUsagePreference
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetUsagePreference $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetUsagePreferenceResponse
     */
    public function _getUsagePreference(GetUsagePreference $parameters)
    {
        return $this->__soapCall("getUsagePreference", array($parameters));
    }
    
    /**
     * Method getSlaveMaster
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetSlaveMaster $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetSlaveMasterResponse
     */
    public function _getSlaveMaster(GetSlaveMaster $parameters)
    {
        return $this->__soapCall("getSlaveMaster", array($parameters));
    }
    
    /**
     * Method getHousehold
     * 
     * @param \Adeo\Sirius\CustomerGetService\Type\GetHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGetService\Type\GetHouseholdResponse
     */
    public function _getHousehold(GetHousehold $parameters)
    {
        return $this->__soapCall("getHousehold", array($parameters));
    }
}
