<?php
/**
 * Parent model for proxy for CustomerGlobalService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\Base;

use \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\GlobalSearch;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\SendJMS;
use \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\GetHousehold;

abstract class CustomerGlobalService
    extends \SoapClient
{
    
    /**
     * Class mapping for SOAP client
     * 
     * @var array
     */
    protected $classmap = array(
        'updateGlobalHousehold'                   => 'Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHousehold',
        'fullHouseholdDTO'                        => 'Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO',
        'abstractDTO'                             => 'Adeo\Sirius\CustomerGlobalService\Type\AbstractDTO',
        'historyDTO'                              => 'Adeo\Sirius\CustomerGlobalService\Type\HistoryDTO',
        'addressDTO'                              => 'Adeo\Sirius\CustomerGlobalService\Type\AddressDTO',
        'addressDetailDTO'                        => 'Adeo\Sirius\CustomerGlobalService\Type\AddressDetailDTO',
        'optinDTO'                                => 'Adeo\Sirius\CustomerGlobalService\Type\OptinDTO',
        'addressTypeDTO'                          => 'Adeo\Sirius\CustomerGlobalService\Type\AddressTypeDTO',
        'addressExternalCodeDTO'                  => 'Adeo\Sirius\CustomerGlobalService\Type\AddressExternalCodeDTO',
        'housingDTO'                              => 'Adeo\Sirius\CustomerGlobalService\Type\HousingDTO',
        'housingDetailDTO'                        => 'Adeo\Sirius\CustomerGlobalService\Type\HousingDetailDTO',
        'housingCharacteristicDTO'                => 'Adeo\Sirius\CustomerGlobalService\Type\HousingCharacteristicDTO',
        'fullNaturalCustomerDTO'                  => 'Adeo\Sirius\CustomerGlobalService\Type\FullNaturalCustomerDTO',
        'naturalCustomerIdentityDTO'              => 'Adeo\Sirius\CustomerGlobalService\Type\NaturalCustomerIdentityDTO',
        'communicationDTO'                        => 'Adeo\Sirius\CustomerGlobalService\Type\CommunicationDTO',
        'communicationDetailDTO'                  => 'Adeo\Sirius\CustomerGlobalService\Type\CommunicationDetailDTO',
        'scopeDTO'                                => 'Adeo\Sirius\CustomerGlobalService\Type\ScopeDTO',
        'classificationDTO'                       => 'Adeo\Sirius\CustomerGlobalService\Type\ClassificationDTO',
        'externalIdentifierDTO'                   => 'Adeo\Sirius\CustomerGlobalService\Type\ExternalIdentifierDTO',
        'leisureDTO'                              => 'Adeo\Sirius\CustomerGlobalService\Type\LeisureDTO',
        'communityDTO'                            => 'Adeo\Sirius\CustomerGlobalService\Type\CommunityDTO',
        'notePadDTO'                              => 'Adeo\Sirius\CustomerGlobalService\Type\NotePadDTO',
        'updateGlobalHouseholdResponse'           => 'Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHouseholdResponse',
        'updateGlobalLegalEntity'                 => 'Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntity',
        'fullLegalEntityDTO'                      => 'Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO',
        'legalIdentityDTO'                        => 'Adeo\Sirius\CustomerGlobalService\Type\LegalIdentityDTO',
        'fullLegalContactDTO'                     => 'Adeo\Sirius\CustomerGlobalService\Type\FullLegalContactDTO',
        'legalContactIdentityDTO'                 => 'Adeo\Sirius\CustomerGlobalService\Type\LegalContactIdentityDTO',
        'updateGlobalLegalEntityResponse'         => 'Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntityResponse',
        'globalSearch'                            => 'Adeo\Sirius\CustomerGlobalService\Type\GlobalSearch',
        'searchCriteriaDTO'                       => 'Adeo\Sirius\CustomerGlobalService\Type\SearchCriteriaDTO',
        'globalSearchResponse'                    => 'Adeo\Sirius\CustomerGlobalService\Type\GlobalSearchResponse',
        'searchResultDTO'                         => 'Adeo\Sirius\CustomerGlobalService\Type\SearchResultDTO',
        'validateUpdateGlobalLegalEntity'         => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntity',
        'validateUpdateGlobalLegalEntityResponse' => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntityResponse',
        'errorCodeAndDetail'                      => 'Adeo\Sirius\CustomerGlobalService\Type\ErrorCodeAndDetail',
        'createGlobalHousehold'                   => 'Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHousehold',
        'createGlobalHouseholdResponse'           => 'Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHouseholdResponse',
        'validateCreateGlobalLegalEntity'         => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntity',
        'validateCreateGlobalLegalEntityResponse' => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntityResponse',
        'validateCreateGlobalHousehold'           => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHousehold',
        'validateCreateGlobalHouseholdResponse'   => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHouseholdResponse',
        'sendJMS'                                 => 'Adeo\Sirius\CustomerGlobalService\Type\SendJMS',
        'sendJMSResponse'                         => 'Adeo\Sirius\CustomerGlobalService\Type\SendJMSResponse',
        'createGlobalLegalEntity'                 => 'Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntity',
        'createGlobalLegalEntityResponse'         => 'Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntityResponse',
        'validateUpdateGlobalHousehold'           => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHousehold',
        'validateUpdateGlobalHouseholdResponse'   => 'Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHouseholdResponse',
        'getLegalEntity'                          => 'Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntity',
        'getLegalEntityResponse'                  => 'Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntityResponse',
        'getHousehold'                            => 'Adeo\Sirius\CustomerGlobalService\Type\GetHousehold',
        'getHouseholdResponse'                    => 'Adeo\Sirius\CustomerGlobalService\Type\GetHouseholdResponse',
        'SiriusApplicationFault'                  => 'Adeo\Sirius\CustomerGlobalService\Type\SiriusApplicationFault',
        'SiriusBusinessFault'                     => 'Adeo\Sirius\CustomerGlobalService\Type\SiriusBusinessFault',
        );
    
    /**
     * Service contructor
     * 
     * @param string $wsdl    URI of the WSDL file
     * @param array  $options An array of options
     * 
     * @return void
     */
    public function __construct($wsdl, array $options = array())
    {
        foreach ($this->classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        parent::__construct($wsdl, $options);
    }
    
    /**
     * Method updateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHouseholdResponse
     */
    public function _updateGlobalHousehold(UpdateGlobalHousehold $parameters)
    {
        return $this->__soapCall("updateGlobalHousehold", array($parameters));
    }
    
    /**
     * Method updateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntityResponse
     */
    public function _updateGlobalLegalEntity(UpdateGlobalLegalEntity $parameters)
    {
        return $this->__soapCall("updateGlobalLegalEntity", array($parameters));
    }
    
    /**
     * Method globalSearch
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\GlobalSearch $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\GlobalSearchResponse
     */
    public function _globalSearch(GlobalSearch $parameters)
    {
        return $this->__soapCall("globalSearch", array($parameters));
    }
    
    /**
     * Method validateUpdateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntityResponse
     */
    public function _validateUpdateGlobalLegalEntity(ValidateUpdateGlobalLegalEntity $parameters)
    {
        return $this->__soapCall("validateUpdateGlobalLegalEntity", array($parameters));
    }
    
    /**
     * Method createGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHouseholdResponse
     */
    public function _createGlobalHousehold(CreateGlobalHousehold $parameters)
    {
        return $this->__soapCall("createGlobalHousehold", array($parameters));
    }
    
    /**
     * Method validateCreateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntityResponse
     */
    public function _validateCreateGlobalLegalEntity(ValidateCreateGlobalLegalEntity $parameters)
    {
        return $this->__soapCall("validateCreateGlobalLegalEntity", array($parameters));
    }
    
    /**
     * Method validateCreateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHouseholdResponse
     */
    public function _validateCreateGlobalHousehold(ValidateCreateGlobalHousehold $parameters)
    {
        return $this->__soapCall("validateCreateGlobalHousehold", array($parameters));
    }
    
    /**
     * Method sendJMS
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\SendJMS $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\SendJMSResponse
     */
    public function _sendJMS(SendJMS $parameters)
    {
        return $this->__soapCall("sendJMS", array($parameters));
    }
    
    /**
     * Method createGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntityResponse
     */
    public function _createGlobalLegalEntity(CreateGlobalLegalEntity $parameters)
    {
        return $this->__soapCall("createGlobalLegalEntity", array($parameters));
    }
    
    /**
     * Method validateUpdateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHouseholdResponse
     */
    public function _validateUpdateGlobalHousehold(ValidateUpdateGlobalHousehold $parameters)
    {
        return $this->__soapCall("validateUpdateGlobalHousehold", array($parameters));
    }
    
    /**
     * Method getLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntityResponse
     */
    public function _getLegalEntity(GetLegalEntity $parameters)
    {
        return $this->__soapCall("getLegalEntity", array($parameters));
    }
    
    /**
     * Method getHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\GetHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\GetHouseholdResponse
     */
    public function _getHousehold(GetHousehold $parameters)
    {
        return $this->__soapCall("getHousehold", array($parameters));
    }
}
