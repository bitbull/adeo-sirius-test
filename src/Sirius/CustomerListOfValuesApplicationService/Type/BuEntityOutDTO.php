<?php
/**
 * Model for BuEntityOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type;

use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO as BuEntityOutDTOBase;

class BuEntityOutDTO
    extends buEntityOutDTOBase
{
}
