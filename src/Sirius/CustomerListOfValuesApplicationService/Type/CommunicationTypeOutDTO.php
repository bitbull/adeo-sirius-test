<?php
/**
 * Model for CommunicationTypeOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type;

use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CommunicationTypeOutDTO as CommunicationTypeOutDTOBase;

class CommunicationTypeOutDTO
    extends communicationTypeOutDTOBase
{
}
