<?php
/**
 * Model for GetAllHousingCharacteristicTypes
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type;

use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllHousingCharacteristicTypes as GetAllHousingCharacteristicTypesBase;

class GetAllHousingCharacteristicTypes
    extends getAllHousingCharacteristicTypesBase
{
}
