<?php
/**
 * Parent model for ClassificationInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class ClassificationInformationOutDTO
{
    
    /**
     * ValueCode
     * 
     * @var string
     */
    public $valueCode;
    
    /**
     * ValueLabel
     * 
     * @var string
     */
    public $valueLabel;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * SubClassifications collection
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $subClassifications = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get valueCode
     * 
     * @return string
     */
    public function getValueCode()
    {
        return $this->valueCode;
    }
    
    /**
     * Set valueCode
     * 
     * @param string $value valueCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setValueCode($value)
    {
        $this->valueCode = $value;
        return $this;
    }
    
    /**
     * Get valueLabel
     * 
     * @return string
     */
    public function getValueLabel()
    {
        return $this->valueLabel;
    }
    
    /**
     * Set valueLabel
     * 
     * @param string $value valueLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setValueLabel($value)
    {
        $this->valueLabel = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get subClassifications
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getSubClassifications()
    {
        return $this->subClassifications;
    }
    
    /**
     * Add element on subClassifications collection
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[] $value subClassifications
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setSubClassifications($value = null)
    {
        $this->subClassifications[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
