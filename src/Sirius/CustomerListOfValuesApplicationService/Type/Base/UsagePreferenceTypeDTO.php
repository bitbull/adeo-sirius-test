<?php
/**
 * Parent model for UsagePreferenceTypeDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class UsagePreferenceTypeDTO
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code;
    
    /**
     * ContextCode
     * 
     * @var int
     */
    public $contextCode;
    
    /**
     * ContextLabel
     * 
     * @var string
     */
    public $contextLabel;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * SubPreferenceType
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public $subPreferenceType;
    
    /**
     * TargetTypeCode
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTargetTypeDTO
     */
    public $targetTypeCode;
    
    /**
     * TargetTypeLabel
     * 
     * @var string
     */
    public $targetTypeLabel;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get contextCode
     * 
     * @return int
     */
    public function getContextCode()
    {
        return $this->contextCode;
    }
    
    /**
     * Set contextCode
     * 
     * @param int $value contextCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setContextCode($value)
    {
        $this->contextCode = $value;
        return $this;
    }
    
    /**
     * Get contextLabel
     * 
     * @return string
     */
    public function getContextLabel()
    {
        return $this->contextLabel;
    }
    
    /**
     * Set contextLabel
     * 
     * @param string $value contextLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setContextLabel($value)
    {
        $this->contextLabel = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get subPreferenceType
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function getSubPreferenceType()
    {
        return $this->subPreferenceType;
    }
    
    /**
     * Set subPreferenceType
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO $value subPreferenceType
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setSubPreferenceType(\Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO $value)
    {
        $this->subPreferenceType = $value;
        return $this;
    }
    
    /**
     * Get targetTypeCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTargetTypeDTO
     */
    public function getTargetTypeCode()
    {
        return $this->targetTypeCode;
    }
    
    /**
     * Set targetTypeCode
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTargetTypeDTO $value targetTypeCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setTargetTypeCode(\Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTargetTypeDTO $value)
    {
        $this->targetTypeCode = $value;
        return $this;
    }
    
    /**
     * Get targetTypeLabel
     * 
     * @return string
     */
    public function getTargetTypeLabel()
    {
        return $this->targetTypeLabel;
    }
    
    /**
     * Set targetTypeLabel
     * 
     * @param string $value targetTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceTypeDTO
     */
    public function setTargetTypeLabel($value)
    {
        $this->targetTypeLabel = $value;
        return $this;
    }
}
