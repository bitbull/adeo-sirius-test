<?php
/**
 * Parent model for UsagePreferenceContextDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class UsagePreferenceContextDTO
{
    
    /**
     * ContextCode
     * 
     * @var int
     */
    public $contextCode;
    
    /**
     * ContextLabel
     * 
     * @var string
     */
    public $contextLabel;
    
    /**
     * Get contextCode
     * 
     * @return int
     */
    public function getContextCode()
    {
        return $this->contextCode;
    }
    
    /**
     * Set contextCode
     * 
     * @param int $value contextCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceContextDTO
     */
    public function setContextCode($value)
    {
        $this->contextCode = $value;
        return $this;
    }
    
    /**
     * Get contextLabel
     * 
     * @return string
     */
    public function getContextLabel()
    {
        return $this->contextLabel;
    }
    
    /**
     * Set contextLabel
     * 
     * @param string $value contextLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\UsagePreferenceContextDTO
     */
    public function setContextLabel($value)
    {
        $this->contextLabel = $value;
        return $this;
    }
}
