<?php
/**
 * Parent model for GetAllSegmentationsByTypeResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class GetAllSegmentationsByTypeResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\SegmentationInformationOutDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\SegmentationInformationOutDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\SegmentationInformationOutDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllSegmentationsByTypeResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
