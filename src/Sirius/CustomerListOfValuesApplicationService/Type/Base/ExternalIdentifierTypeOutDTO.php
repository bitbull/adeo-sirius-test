<?php
/**
 * Parent model for ExternalIdentifierTypeOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class ExternalIdentifierTypeOutDTO
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ExternalIdentifierTypeOutDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ExternalIdentifierTypeOutDTO
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ExternalIdentifierTypeOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
