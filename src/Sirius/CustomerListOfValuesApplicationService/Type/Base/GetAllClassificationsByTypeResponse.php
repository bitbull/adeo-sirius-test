<?php
/**
 * Parent model for GetAllClassificationsByTypeResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class GetAllClassificationsByTypeResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\ClassificationInformationOutDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllClassificationsByTypeResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
