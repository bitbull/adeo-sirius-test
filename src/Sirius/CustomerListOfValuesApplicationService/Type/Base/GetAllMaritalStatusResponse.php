<?php
/**
 * Parent model for GetAllMaritalStatusResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class GetAllMaritalStatusResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\MaritalStatusOutDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\MaritalStatusOutDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\MaritalStatusOutDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllMaritalStatusResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
