<?php
/**
 * Parent model for CountryWithFormatInfoDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class CountryWithFormatInfoDTO
{
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * CountryLabel
     * 
     * @var string
     */
    public $countryLabel;
    
    /**
     * PhoneAreaCode
     * 
     * @var string
     */
    public $phoneAreaCode;
    
    /**
     * PhoneRegEx
     * 
     * @var string
     */
    public $phoneRegEx;
    
    /**
     * PhoneFormat
     * 
     * @var string
     */
    public $phoneFormat;
    
    /**
     * PostalCodeRegEx
     * 
     * @var string
     */
    public $postalCodeRegEx;
    
    /**
     * PostalCodeFormat
     * 
     * @var string
     */
    public $postalCodeFormat;
    
    /**
     * SearchPostalCodeRegEx
     * 
     * @var string
     */
    public $searchPostalCodeRegEx;
    
    /**
     * SearchPostalCodeFormat
     * 
     * @var string
     */
    public $searchPostalCodeFormat;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get countryLabel
     * 
     * @return string
     */
    public function getCountryLabel()
    {
        return $this->countryLabel;
    }
    
    /**
     * Set countryLabel
     * 
     * @param string $value countryLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setCountryLabel($value)
    {
        $this->countryLabel = $value;
        return $this;
    }
    
    /**
     * Get phoneAreaCode
     * 
     * @return string
     */
    public function getPhoneAreaCode()
    {
        return $this->phoneAreaCode;
    }
    
    /**
     * Set phoneAreaCode
     * 
     * @param string $value phoneAreaCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setPhoneAreaCode($value)
    {
        $this->phoneAreaCode = $value;
        return $this;
    }
    
    /**
     * Get phoneRegEx
     * 
     * @return string
     */
    public function getPhoneRegEx()
    {
        return $this->phoneRegEx;
    }
    
    /**
     * Set phoneRegEx
     * 
     * @param string $value phoneRegEx
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setPhoneRegEx($value)
    {
        $this->phoneRegEx = $value;
        return $this;
    }
    
    /**
     * Get phoneFormat
     * 
     * @return string
     */
    public function getPhoneFormat()
    {
        return $this->phoneFormat;
    }
    
    /**
     * Set phoneFormat
     * 
     * @param string $value phoneFormat
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setPhoneFormat($value)
    {
        $this->phoneFormat = $value;
        return $this;
    }
    
    /**
     * Get postalCodeRegEx
     * 
     * @return string
     */
    public function getPostalCodeRegEx()
    {
        return $this->postalCodeRegEx;
    }
    
    /**
     * Set postalCodeRegEx
     * 
     * @param string $value postalCodeRegEx
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setPostalCodeRegEx($value)
    {
        $this->postalCodeRegEx = $value;
        return $this;
    }
    
    /**
     * Get postalCodeFormat
     * 
     * @return string
     */
    public function getPostalCodeFormat()
    {
        return $this->postalCodeFormat;
    }
    
    /**
     * Set postalCodeFormat
     * 
     * @param string $value postalCodeFormat
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setPostalCodeFormat($value)
    {
        $this->postalCodeFormat = $value;
        return $this;
    }
    
    /**
     * Get searchPostalCodeRegEx
     * 
     * @return string
     */
    public function getSearchPostalCodeRegEx()
    {
        return $this->searchPostalCodeRegEx;
    }
    
    /**
     * Set searchPostalCodeRegEx
     * 
     * @param string $value searchPostalCodeRegEx
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setSearchPostalCodeRegEx($value)
    {
        $this->searchPostalCodeRegEx = $value;
        return $this;
    }
    
    /**
     * Get searchPostalCodeFormat
     * 
     * @return string
     */
    public function getSearchPostalCodeFormat()
    {
        return $this->searchPostalCodeFormat;
    }
    
    /**
     * Set searchPostalCodeFormat
     * 
     * @param string $value searchPostalCodeFormat
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setSearchPostalCodeFormat($value)
    {
        $this->searchPostalCodeFormat = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CountryWithFormatInfoDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
