<?php
/**
 * Parent model for GetAllUsagePeferenceTypes
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class GetAllUsagePeferenceTypes
{
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllUsagePeferenceTypes
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
}
