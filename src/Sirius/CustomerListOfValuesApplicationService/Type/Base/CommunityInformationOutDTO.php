<?php
/**
 * Parent model for CommunityInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class CommunityInformationOutDTO
{
    
    /**
     * ValueCode
     * 
     * @var int
     */
    public $valueCode;
    
    /**
     * ValueLabel
     * 
     * @var string
     */
    public $valueLabel;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get valueCode
     * 
     * @return int
     */
    public function getValueCode()
    {
        return $this->valueCode;
    }
    
    /**
     * Set valueCode
     * 
     * @param int $value valueCode
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CommunityInformationOutDTO
     */
    public function setValueCode($value)
    {
        $this->valueCode = $value;
        return $this;
    }
    
    /**
     * Get valueLabel
     * 
     * @return string
     */
    public function getValueLabel()
    {
        return $this->valueLabel;
    }
    
    /**
     * Set valueLabel
     * 
     * @param string $value valueLabel
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CommunityInformationOutDTO
     */
    public function setValueLabel($value)
    {
        $this->valueLabel = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\CommunityInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
