<?php
/**
 * Parent model for GetAllHousingCharacteristicsByTypeResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class GetAllHousingCharacteristicsByTypeResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\HousingCharacteristicOutDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\HousingCharacteristicOutDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\HousingCharacteristicOutDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllHousingCharacteristicsByTypeResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
