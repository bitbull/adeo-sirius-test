<?php
/**
 * Parent model for BuEntityOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base;

abstract class BuEntityOutDTO
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * EntityType
     * 
     * @var int
     */
    public $entityType;
    
    /**
     * OpeningDate
     * 
     * @var string
     */
    public $openingDate;
    
    /**
     * ClosingDate
     * 
     * @var string
     */
    public $closingDate;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get entityType
     * 
     * @return int
     */
    public function getEntityType()
    {
        return $this->entityType;
    }
    
    /**
     * Set entityType
     * 
     * @param int $value entityType
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setEntityType($value)
    {
        $this->entityType = $value;
        return $this;
    }
    
    /**
     * Get openingDate
     * 
     * @return string
     */
    public function getOpeningDate()
    {
        return $this->openingDate;
    }
    
    /**
     * Set openingDate
     * 
     * @param string $value openingDate
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setOpeningDate($value)
    {
        $this->openingDate = $value;
        return $this;
    }
    
    /**
     * Get closingDate
     * 
     * @return string
     */
    public function getClosingDate()
    {
        return $this->closingDate;
    }
    
    /**
     * Set closingDate
     * 
     * @param string $value closingDate
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setClosingDate($value)
    {
        $this->closingDate = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\BuEntityOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
