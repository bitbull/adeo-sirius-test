<?php
/**
 * Model for GetAllLeisureTypesResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerListOfValuesApplicationService\Type;

use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\Base\GetAllLeisureTypesResponse as GetAllLeisureTypesResponseBase;

class GetAllLeisureTypesResponse
    extends getAllLeisureTypesResponseBase
{
}
