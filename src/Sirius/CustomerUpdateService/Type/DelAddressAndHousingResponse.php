<?php
/**
 * Model for DelAddressAndHousingResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\DelAddressAndHousingResponse as DelAddressAndHousingResponseBase;

class DelAddressAndHousingResponse
    extends delAddressAndHousingResponseBase
{
}
