<?php
/**
 * Model for SetCommunicationsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationsResponse as SetCommunicationsResponseBase;

class SetCommunicationsResponse
    extends setCommunicationsResponseBase
{
}
