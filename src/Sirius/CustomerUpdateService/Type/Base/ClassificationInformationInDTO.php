<?php
/**
 * Parent model for ClassificationInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class ClassificationInformationInDTO
{
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\ClassificationInformationInDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\ClassificationInformationInDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\ClassificationInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
