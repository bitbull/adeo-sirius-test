<?php
/**
 * Parent model for UsagePreferenceInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class UsagePreferenceInDTO
{
    
    /**
     * CodeType
     * 
     * @var int
     */
    public $codeType;
    
    /**
     * Description
     * 
     * @var string
     */
    public $description;
    
    /**
     * Other
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO
     */
    public $other;
    
    /**
     * Value
     * 
     * @var int
     */
    public $value;
    
    /**
     * Get codeType
     * 
     * @return int
     */
    public function getCodeType()
    {
        return $this->codeType;
    }
    
    /**
     * Set codeType
     * 
     * @param int $value codeType
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO
     */
    public function setCodeType($value)
    {
        $this->codeType = $value;
        return $this;
    }
    
    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description
     * 
     * @param string $value description
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }
    
    /**
     * Get other
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO
     */
    public function getOther()
    {
        return $this->other;
    }
    
    /**
     * Set other
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO $value other
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO
     */
    public function setOther(\Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO $value)
    {
        $this->other = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param int $value value
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
