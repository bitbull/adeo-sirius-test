<?php
/**
 * Parent model for SetLegalEntityContactsDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetLegalEntityContactsDTO
{
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO[]
     */
    public $contacts = array();
    
    /**
     * LegalEntityCustomerNumber
     * 
     * @var int
     */
    public $legalEntityCustomerNumber;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get legalEntityCustomerNumber
     * 
     * @return int
     */
    public function getLegalEntityCustomerNumber()
    {
        return $this->legalEntityCustomerNumber;
    }
    
    /**
     * Set legalEntityCustomerNumber
     * 
     * @param int $value legalEntityCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public function setLegalEntityCustomerNumber($value)
    {
        $this->legalEntityCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
