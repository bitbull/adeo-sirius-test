<?php
/**
 * Parent model for MoveNaturalPersonCustomer
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class MoveNaturalPersonCustomer
{
    
    /**
     * Arg0
     * 
     * @var int
     */
    public $arg0;
    
    /**
     * Arg1
     * 
     * @var int
     */
    public $arg1;
    
    /**
     * Arg2
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $arg2;
    
    /**
     * Get arg0
     * 
     * @return int
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param int $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\MoveNaturalPersonCustomer
     */
    public function setArg0($value)
    {
        $this->arg0 = $value;
        return $this;
    }
    
    /**
     * Get arg1
     * 
     * @return int
     */
    public function getArg1()
    {
        return $this->arg1;
    }
    
    /**
     * Set arg1
     * 
     * @param int $value arg1
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\MoveNaturalPersonCustomer
     */
    public function setArg1($value)
    {
        $this->arg1 = $value;
        return $this;
    }
    
    /**
     * Get arg2
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getArg2()
    {
        return $this->arg2;
    }
    
    /**
     * Set arg2
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value arg2
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\MoveNaturalPersonCustomer
     */
    public function setArg2(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->arg2 = $value;
        return $this;
    }
}
