<?php
/**
 * Parent model for CustomerAddressDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CustomerAddressDTO
{
    
    /**
     * AddressInformationIn
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\AddressInformationInDTO
     */
    public $addressInformationIn;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * HistoryInput
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $historyInput;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get addressInformationIn
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\AddressInformationInDTO
     */
    public function getAddressInformationIn()
    {
        return $this->addressInformationIn;
    }
    
    /**
     * Set addressInformationIn
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\AddressInformationInDTO $value addressInformationIn
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public function setAddressInformationIn(\Adeo\Sirius\CustomerUpdateService\Type\Base\AddressInformationInDTO $value)
    {
        $this->addressInformationIn = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get historyInput
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInput()
    {
        return $this->historyInput;
    }
    
    /**
     * Set historyInput
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value historyInput
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public function setHistoryInput(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInput = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
