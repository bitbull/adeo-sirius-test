<?php
/**
 * Parent model for UsagePreferenceOtherInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class UsagePreferenceOtherInDTO
{
    
    /**
     * Type
     * 
     * @var string
     */
    public $type;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Get type
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param string $value type
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceOtherInDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
