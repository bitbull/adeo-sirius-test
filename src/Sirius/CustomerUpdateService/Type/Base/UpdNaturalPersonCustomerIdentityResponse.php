<?php
/**
 * Parent model for UpdNaturalPersonCustomerIdentityResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class UpdNaturalPersonCustomerIdentityResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UpdNaturalPersonCustomerIdentityResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
