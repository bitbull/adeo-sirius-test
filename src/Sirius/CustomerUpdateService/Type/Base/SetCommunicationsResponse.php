<?php
/**
 * Parent model for SetCommunicationsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetCommunicationsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
