<?php
/**
 * Parent model for SetChildrenResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetChildrenResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
