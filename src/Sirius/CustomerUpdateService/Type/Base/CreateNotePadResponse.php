<?php
/**
 * Parent model for CreateNotePadResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CreateNotePadResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreateNotePadResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
