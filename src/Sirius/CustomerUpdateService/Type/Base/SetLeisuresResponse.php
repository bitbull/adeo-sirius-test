<?php
/**
 * Parent model for SetLeisuresResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetLeisuresResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerLeisuresDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerLeisuresDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerLeisuresDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLeisuresResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerLeisuresDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
