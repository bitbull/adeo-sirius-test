<?php
/**
 * Parent model for DelNotePad
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class DelNotePad
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DelNotePad
     */
    public function setArg0(\Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
