<?php
/**
 * Parent model for SetClassificationsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetClassificationsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerClassificationDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerClassificationDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerClassificationDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetClassificationsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerClassificationDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
