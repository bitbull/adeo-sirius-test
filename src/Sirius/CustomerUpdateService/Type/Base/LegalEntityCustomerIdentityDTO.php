<?php
/**
 * Parent model for LegalEntityCustomerIdentityDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class LegalEntityCustomerIdentityDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * CorporateName
     * 
     * @var string
     */
    public $corporateName;
    
    /**
     * ManagementEntityNumber
     * 
     * @var int
     */
    public $managementEntityNumber;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $optins = array();
    
    /**
     * HistoryInputDTO
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $historyInputDTO;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get corporateName
     * 
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }
    
    /**
     * Set corporateName
     * 
     * @param string $value corporateName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setCorporateName($value)
    {
        $this->corporateName = $value;
        return $this;
    }
    
    /**
     * Get managementEntityNumber
     * 
     * @return int
     */
    public function getManagementEntityNumber()
    {
        return $this->managementEntityNumber;
    }
    
    /**
     * Set managementEntityNumber
     * 
     * @param int $value managementEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setManagementEntityNumber($value)
    {
        $this->managementEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInputDTO()
    {
        return $this->historyInputDTO;
    }
    
    /**
     * Set historyInputDTO
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setHistoryInputDTO(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInputDTO = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
