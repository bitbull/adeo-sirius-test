<?php
/**
 * Parent model for SetCommunicationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetCommunicationDTO
{
    
    /**
     * CommunicationScopes collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CommunicationScopeOutDTO[]
     */
    public $communicationScopes = array();
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * PhoneType
     * 
     * @var int
     */
    public $phoneType;
    
    /**
     * Order
     * 
     * @var int
     */
    public $order;
    
    /**
     * Main
     * 
     * @var boolean
     */
    public $main;
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * SpecificOptinIns collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\SpecificOptinInDTO[]
     */
    public $specificOptinIns = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communicationScopes
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CommunicationScopeOutDTO[]
     */
    public function getCommunicationScopes()
    {
        return $this->communicationScopes;
    }
    
    /**
     * Add element on communicationScopes collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CommunicationScopeOutDTO[] $value communicationScopes
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setCommunicationScopes($value = null)
    {
        $this->communicationScopes[] = $value;
        return $this;
    }
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get phoneType
     * 
     * @return int
     */
    public function getPhoneType()
    {
        return $this->phoneType;
    }
    
    /**
     * Set phoneType
     * 
     * @param int $value phoneType
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setPhoneType($value)
    {
        $this->phoneType = $value;
        return $this;
    }
    
    /**
     * Get order
     * 
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set order
     * 
     * @param int $value order
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }
    
    /**
     * Get main
     * 
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }
    
    /**
     * Set main
     * 
     * @param boolean $value main
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setMain($value)
    {
        $this->main = $value;
        return $this;
    }
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get specificOptinIns
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SpecificOptinInDTO[]
     */
    public function getSpecificOptinIns()
    {
        return $this->specificOptinIns;
    }
    
    /**
     * Add element on specificOptinIns collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\SpecificOptinInDTO[] $value specificOptinIns
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setSpecificOptinIns($value = null)
    {
        $this->specificOptinIns[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunicationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
