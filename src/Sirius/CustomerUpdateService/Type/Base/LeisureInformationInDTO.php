<?php
/**
 * Parent model for LeisureInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class LeisureInformationInDTO
{
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * LevelCode
     * 
     * @var int
     */
    public $levelCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LeisureInformationInDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get levelCode
     * 
     * @return int
     */
    public function getLevelCode()
    {
        return $this->levelCode;
    }
    
    /**
     * Set levelCode
     * 
     * @param int $value levelCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LeisureInformationInDTO
     */
    public function setLevelCode($value)
    {
        $this->levelCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LeisureInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
