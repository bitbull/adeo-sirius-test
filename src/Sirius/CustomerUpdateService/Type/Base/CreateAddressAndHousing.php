<?php
/**
 * Parent model for CreateAddressAndHousing
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CreateAddressAndHousing
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreateAddressAndHousing
     */
    public function setArg0(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerAddressDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
