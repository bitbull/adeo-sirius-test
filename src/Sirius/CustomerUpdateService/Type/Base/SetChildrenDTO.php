<?php
/**
 * Parent model for SetChildrenDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetChildrenDTO
{
    
    /**
     * Children collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\ChildDTO[]
     */
    public $children = array();
    
    /**
     * HistoryInput
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInput
     */
    public $historyInput;
    
    /**
     * HouseholdId
     * 
     * @var int
     */
    public $householdId;
    
    /**
     * Get children
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\ChildDTO[]
     */
    public function getChildren()
    {
        return $this->children;
    }
    
    /**
     * Add element on children collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\ChildDTO[] $value children
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO
     */
    public function setChildren($value = null)
    {
        $this->children[] = $value;
        return $this;
    }
    
    /**
     * Get historyInput
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInput
     */
    public function getHistoryInput()
    {
        return $this->historyInput;
    }
    
    /**
     * Set historyInput
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInput $value historyInput
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO
     */
    public function setHistoryInput(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInput $value)
    {
        $this->historyInput = $value;
        return $this;
    }
    
    /**
     * Get householdId
     * 
     * @return int
     */
    public function getHouseholdId()
    {
        return $this->householdId;
    }
    
    /**
     * Set householdId
     * 
     * @param int $value householdId
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetChildrenDTO
     */
    public function setHouseholdId($value)
    {
        $this->householdId = $value;
        return $this;
    }
}
