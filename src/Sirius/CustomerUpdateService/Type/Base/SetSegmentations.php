<?php
/**
 * Parent model for SetSegmentations
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetSegmentations
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetSegmentations
     */
    public function setArg0(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
