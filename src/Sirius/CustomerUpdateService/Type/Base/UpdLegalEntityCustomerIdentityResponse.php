<?php
/**
 * Parent model for UpdLegalEntityCustomerIdentityResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class UpdLegalEntityCustomerIdentityResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UpdLegalEntityCustomerIdentityResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityCustomerIdentityDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
