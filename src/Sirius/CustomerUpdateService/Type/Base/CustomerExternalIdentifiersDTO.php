<?php
/**
 * Parent model for CustomerExternalIdentifiersDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CustomerExternalIdentifiersDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\ExternalIdentifierInformationInDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\ExternalIdentifierInformationInDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\ExternalIdentifierInformationInDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
