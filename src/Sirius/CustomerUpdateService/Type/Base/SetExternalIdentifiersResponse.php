<?php
/**
 * Parent model for SetExternalIdentifiersResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetExternalIdentifiersResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetExternalIdentifiersResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerExternalIdentifiersDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
