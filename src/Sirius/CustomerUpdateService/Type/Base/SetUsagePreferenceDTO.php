<?php
/**
 * Parent model for SetUsagePreferenceDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetUsagePreferenceDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * UsagePreferences collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO[]
     */
    public $usagePreferences = array();
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get usagePreferences
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO[]
     */
    public function getUsagePreferences()
    {
        return $this->usagePreferences;
    }
    
    /**
     * Add element on usagePreferences collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO[] $value usagePreferences
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO
     */
    public function setUsagePreferences($value = null)
    {
        $this->usagePreferences[] = $value;
        return $this;
    }
}
