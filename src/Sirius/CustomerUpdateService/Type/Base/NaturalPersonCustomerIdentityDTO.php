<?php
/**
 * Parent model for NaturalPersonCustomerIdentityDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class NaturalPersonCustomerIdentityDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * BirthName
     * 
     * @var string
     */
    public $birthName;
    
    /**
     * BirthDate
     * 
     * @var string
     */
    public $birthDate;
    
    /**
     * MaritalStatus
     * 
     * @var int
     */
    public $maritalStatus;
    
    /**
     * ManagementEntityNumber
     * 
     * @var int
     */
    public $managementEntityNumber;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * HouseholdIdentifier
     * 
     * @var int
     */
    public $householdIdentifier;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * Title
     * 
     * @var int
     */
    public $title;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $optins = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get birthName
     * 
     * @return string
     */
    public function getBirthName()
    {
        return $this->birthName;
    }
    
    /**
     * Set birthName
     * 
     * @param string $value birthName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setBirthName($value)
    {
        $this->birthName = $value;
        return $this;
    }
    
    /**
     * Get birthDate
     * 
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /**
     * Set birthDate
     * 
     * @param string $value birthDate
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $value;
        return $this;
    }
    
    /**
     * Get maritalStatus
     * 
     * @return int
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }
    
    /**
     * Set maritalStatus
     * 
     * @param int $value maritalStatus
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setMaritalStatus($value)
    {
        $this->maritalStatus = $value;
        return $this;
    }
    
    /**
     * Get managementEntityNumber
     * 
     * @return int
     */
    public function getManagementEntityNumber()
    {
        return $this->managementEntityNumber;
    }
    
    /**
     * Set managementEntityNumber
     * 
     * @param int $value managementEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setManagementEntityNumber($value)
    {
        $this->managementEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get householdIdentifier
     * 
     * @return int
     */
    public function getHouseholdIdentifier()
    {
        return $this->householdIdentifier;
    }
    
    /**
     * Set householdIdentifier
     * 
     * @param int $value householdIdentifier
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setHouseholdIdentifier($value)
    {
        $this->householdIdentifier = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get title
     * 
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set title
     * 
     * @param int $value title
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCustomerIdentityDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
