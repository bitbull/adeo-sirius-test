<?php
/**
 * Parent model for SetCommunications
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetCommunications
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetCommunications
     */
    public function setArg0(\Adeo\Sirius\CustomerUpdateService\Type\Base\NaturalPersonCommunicationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
