<?php
/**
 * Parent model for DeleteNotePadDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class DeleteNotePadDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * NotePadIdentifier
     * 
     * @var int
     */
    public $notePadIdentifier;
    
    /**
     * HistoryInputDTO
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $historyInputDTO;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get notePadIdentifier
     * 
     * @return int
     */
    public function getNotePadIdentifier()
    {
        return $this->notePadIdentifier;
    }
    
    /**
     * Set notePadIdentifier
     * 
     * @param int $value notePadIdentifier
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public function setNotePadIdentifier($value)
    {
        $this->notePadIdentifier = $value;
        return $this;
    }
    
    /**
     * Get historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInputDTO()
    {
        return $this->historyInputDTO;
    }
    
    /**
     * Set historyInputDTO
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public function setHistoryInputDTO(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInputDTO = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\DeleteNotePadDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
