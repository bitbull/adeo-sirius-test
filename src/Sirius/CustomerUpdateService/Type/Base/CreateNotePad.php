<?php
/**
 * Parent model for CreateNotePad
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CreateNotePad
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreateNotePad
     */
    public function setArg0(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerMessageDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
