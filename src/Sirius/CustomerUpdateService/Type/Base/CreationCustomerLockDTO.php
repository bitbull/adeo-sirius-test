<?php
/**
 * Parent model for CreationCustomerLockDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CreationCustomerLockDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Deletable
     * 
     * @var boolean
     */
    public $deletable;
    
    /**
     * Modifiable
     * 
     * @var boolean
     */
    public $modifiable;
    
    /**
     * ActionDetailCodeList collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\Int[]
     */
    public $actionDetailCodeList = array();
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get deletable
     * 
     * @return boolean
     */
    public function getDeletable()
    {
        return $this->deletable;
    }
    
    /**
     * Set deletable
     * 
     * @param boolean $value deletable
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setDeletable($value)
    {
        $this->deletable = $value;
        return $this;
    }
    
    /**
     * Get modifiable
     * 
     * @return boolean
     */
    public function getModifiable()
    {
        return $this->modifiable;
    }
    
    /**
     * Set modifiable
     * 
     * @param boolean $value modifiable
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setModifiable($value)
    {
        $this->modifiable = $value;
        return $this;
    }
    
    /**
     * Get actionDetailCodeList
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\Int[]
     */
    public function getActionDetailCodeList()
    {
        return $this->actionDetailCodeList;
    }
    
    /**
     * Add element on actionDetailCodeList collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\Int[] $value actionDetailCodeList
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setActionDetailCodeList($value = null)
    {
        $this->actionDetailCodeList[] = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerUpdateService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CreationCustomerLockDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
