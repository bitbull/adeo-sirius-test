<?php
/**
 * Parent model for SetSegmentationsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetSegmentationsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetSegmentationsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerSegmentationsDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
