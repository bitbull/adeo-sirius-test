<?php
/**
 * Parent model for SetUsagePreferenceResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetUsagePreferenceResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreferenceDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
