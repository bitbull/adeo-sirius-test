<?php
/**
 * Parent model for LegalEntityContactDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class LegalEntityContactDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * ContactCustomerNumber
     * 
     * @var int
     */
    public $contactCustomerNumber;
    
    /**
     * Title
     * 
     * @var int
     */
    public $title;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * CompanyDepartment
     * 
     * @var string
     */
    public $companyDepartment;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $optins = array();
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * Order
     * 
     * @var int
     */
    public $order;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get contactCustomerNumber
     * 
     * @return int
     */
    public function getContactCustomerNumber()
    {
        return $this->contactCustomerNumber;
    }
    
    /**
     * Set contactCustomerNumber
     * 
     * @param int $value contactCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setContactCustomerNumber($value)
    {
        $this->contactCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get title
     * 
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set title
     * 
     * @param int $value title
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get companyDepartment
     * 
     * @return string
     */
    public function getCompanyDepartment()
    {
        return $this->companyDepartment;
    }
    
    /**
     * Set companyDepartment
     * 
     * @param string $value companyDepartment
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setCompanyDepartment($value)
    {
        $this->companyDepartment = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\GlobalOptInInformationInDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get order
     * 
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set order
     * 
     * @param int $value order
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\LegalEntityContactDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
