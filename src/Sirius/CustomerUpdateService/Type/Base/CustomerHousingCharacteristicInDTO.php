<?php
/**
 * Parent model for CustomerHousingCharacteristicInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class CustomerHousingCharacteristicInDTO
{
    
    /**
     * ValueCode
     * 
     * @var int
     */
    public $valueCode;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get valueCode
     * 
     * @return int
     */
    public function getValueCode()
    {
        return $this->valueCode;
    }
    
    /**
     * Set valueCode
     * 
     * @param int $value valueCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerHousingCharacteristicInDTO
     */
    public function setValueCode($value)
    {
        $this->valueCode = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerHousingCharacteristicInDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerHousingCharacteristicInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
