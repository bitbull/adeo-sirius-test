<?php
/**
 * Parent model for SetLegalEntityContactsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type\Base;

abstract class SetLegalEntityContactsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerUpdateService\Type\Base\SetLegalEntityContactsDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
