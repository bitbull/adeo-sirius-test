<?php
/**
 * Model for UsagePreferenceInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\UsagePreferenceInDTO as UsagePreferenceInDTOBase;

class UsagePreferenceInDTO
    extends usagePreferenceInDTOBase
{
}
