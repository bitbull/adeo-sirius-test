<?php
/**
 * Model for SetUsagePreference
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\SetUsagePreference as SetUsagePreferenceBase;

class SetUsagePreference
    extends setUsagePreferenceBase
{
}
