<?php
/**
 * Model for CustomerLeisuresDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\CustomerLeisuresDTO as CustomerLeisuresDTOBase;

class CustomerLeisuresDTO
    extends customerLeisuresDTOBase
{
}
