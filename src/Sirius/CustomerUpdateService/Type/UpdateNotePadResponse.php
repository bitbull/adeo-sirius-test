<?php
/**
 * Model for UpdateNotePadResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\UpdateNotePadResponse as UpdateNotePadResponseBase;

class UpdateNotePadResponse
    extends updateNotePadResponseBase
{
}
