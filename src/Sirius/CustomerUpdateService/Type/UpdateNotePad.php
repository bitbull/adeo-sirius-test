<?php
/**
 * Model for UpdateNotePad
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerUpdateService\Type;

use \Adeo\Sirius\CustomerUpdateService\Type\Base\UpdateNotePad as UpdateNotePadBase;

class UpdateNotePad
    extends updateNotePadBase
{
}
