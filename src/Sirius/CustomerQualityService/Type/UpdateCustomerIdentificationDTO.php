<?php
/**
 * Model for UpdateCustomerIdentificationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type;

use \Adeo\Sirius\CustomerQualityService\Type\Base\UpdateCustomerIdentificationDTO as UpdateCustomerIdentificationDTOBase;

class UpdateCustomerIdentificationDTO
    extends updateCustomerIdentificationDTOBase
{
}
