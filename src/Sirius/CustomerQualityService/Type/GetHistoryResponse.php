<?php
/**
 * Model for GetHistoryResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type;

use \Adeo\Sirius\CustomerQualityService\Type\Base\GetHistoryResponse as GetHistoryResponseBase;

class GetHistoryResponse
    extends getHistoryResponseBase
{
}
