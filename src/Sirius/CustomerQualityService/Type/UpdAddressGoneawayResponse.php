<?php
/**
 * Model for UpdAddressGoneawayResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type;

use \Adeo\Sirius\CustomerQualityService\Type\Base\UpdAddressGoneawayResponse as UpdAddressGoneawayResponseBase;

class UpdAddressGoneawayResponse
    extends updAddressGoneawayResponseBase
{
}
