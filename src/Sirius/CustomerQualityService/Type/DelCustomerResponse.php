<?php
/**
 * Model for DelCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type;

use \Adeo\Sirius\CustomerQualityService\Type\Base\DelCustomerResponse as DelCustomerResponseBase;

class DelCustomerResponse
    extends delCustomerResponseBase
{
}
