<?php
/**
 * Parent model for UpdAddressGoneaway
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class UpdAddressGoneaway
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\UpdAddressGoneaway
     */
    public function setArg0(\Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
