<?php
/**
 * Parent model for HistoryInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class HistoryInformationOutDTO
{
    
    /**
     * OperatorIdentifier
     * 
     * @var string
     */
    public $operatorIdentifier;
    
    /**
     * OperatorFullName
     * 
     * @var string
     */
    public $operatorFullName;
    
    /**
     * ApplicationCode
     * 
     * @var string
     */
    public $applicationCode;
    
    /**
     * ApplicationLabel
     * 
     * @var string
     */
    public $applicationLabel;
    
    /**
     * EntityNumber
     * 
     * @var int
     */
    public $entityNumber;
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * ActionDate
     * 
     * @var string
     */
    public $actionDate;
    
    /**
     * ActionDetailCode
     * 
     * @var int
     */
    public $actionDetailCode;
    
    /**
     * ActionDetailLabel
     * 
     * @var string
     */
    public $actionDetailLabel;
    
    /**
     * ActionDetailType
     * 
     * @var int
     */
    public $actionDetailType;
    
    /**
     * ActionDetailTypeLabel
     * 
     * @var string
     */
    public $actionDetailTypeLabel;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get operatorIdentifier
     * 
     * @return string
     */
    public function getOperatorIdentifier()
    {
        return $this->operatorIdentifier;
    }
    
    /**
     * Set operatorIdentifier
     * 
     * @param string $value operatorIdentifier
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setOperatorIdentifier($value)
    {
        $this->operatorIdentifier = $value;
        return $this;
    }
    
    /**
     * Get operatorFullName
     * 
     * @return string
     */
    public function getOperatorFullName()
    {
        return $this->operatorFullName;
    }
    
    /**
     * Set operatorFullName
     * 
     * @param string $value operatorFullName
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setOperatorFullName($value)
    {
        $this->operatorFullName = $value;
        return $this;
    }
    
    /**
     * Get applicationCode
     * 
     * @return string
     */
    public function getApplicationCode()
    {
        return $this->applicationCode;
    }
    
    /**
     * Set applicationCode
     * 
     * @param string $value applicationCode
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setApplicationCode($value)
    {
        $this->applicationCode = $value;
        return $this;
    }
    
    /**
     * Get applicationLabel
     * 
     * @return string
     */
    public function getApplicationLabel()
    {
        return $this->applicationLabel;
    }
    
    /**
     * Set applicationLabel
     * 
     * @param string $value applicationLabel
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setApplicationLabel($value)
    {
        $this->applicationLabel = $value;
        return $this;
    }
    
    /**
     * Get entityNumber
     * 
     * @return int
     */
    public function getEntityNumber()
    {
        return $this->entityNumber;
    }
    
    /**
     * Set entityNumber
     * 
     * @param int $value entityNumber
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setEntityNumber($value)
    {
        $this->entityNumber = $value;
        return $this;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get actionDate
     * 
     * @return string
     */
    public function getActionDate()
    {
        return $this->actionDate;
    }
    
    /**
     * Set actionDate
     * 
     * @param string $value actionDate
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setActionDate($value)
    {
        $this->actionDate = $value;
        return $this;
    }
    
    /**
     * Get actionDetailCode
     * 
     * @return int
     */
    public function getActionDetailCode()
    {
        return $this->actionDetailCode;
    }
    
    /**
     * Set actionDetailCode
     * 
     * @param int $value actionDetailCode
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setActionDetailCode($value)
    {
        $this->actionDetailCode = $value;
        return $this;
    }
    
    /**
     * Get actionDetailLabel
     * 
     * @return string
     */
    public function getActionDetailLabel()
    {
        return $this->actionDetailLabel;
    }
    
    /**
     * Set actionDetailLabel
     * 
     * @param string $value actionDetailLabel
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setActionDetailLabel($value)
    {
        $this->actionDetailLabel = $value;
        return $this;
    }
    
    /**
     * Get actionDetailType
     * 
     * @return int
     */
    public function getActionDetailType()
    {
        return $this->actionDetailType;
    }
    
    /**
     * Set actionDetailType
     * 
     * @param int $value actionDetailType
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setActionDetailType($value)
    {
        $this->actionDetailType = $value;
        return $this;
    }
    
    /**
     * Get actionDetailTypeLabel
     * 
     * @return string
     */
    public function getActionDetailTypeLabel()
    {
        return $this->actionDetailTypeLabel;
    }
    
    /**
     * Set actionDetailTypeLabel
     * 
     * @param string $value actionDetailTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setActionDetailTypeLabel($value)
    {
        $this->actionDetailTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
