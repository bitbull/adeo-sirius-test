<?php
/**
 * Parent model for GetHistoryResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class GetHistoryResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GetHistoryResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
