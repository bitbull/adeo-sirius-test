<?php
/**
 * Parent model for HistoryOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class HistoryOutDTO
{
    
    /**
     * HistoryList collection
     * 
     * @var \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO[]
     */
    public $historyList = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get historyList
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO[]
     */
    public function getHistoryList()
    {
        return $this->historyList;
    }
    
    /**
     * Add element on historyList collection
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInformationOutDTO[] $value historyList
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO
     */
    public function setHistoryList($value = null)
    {
        $this->historyList[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryOutDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
