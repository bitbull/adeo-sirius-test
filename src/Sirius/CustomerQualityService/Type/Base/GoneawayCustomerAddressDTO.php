<?php
/**
 * Parent model for GoneawayCustomerAddressDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class GoneawayCustomerAddressDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * AddressIdentifier
     * 
     * @var int
     */
    public $addressIdentifier;
    
    /**
     * EmailIdentifier
     * 
     * @var int
     */
    public $emailIdentifier;
    
    /**
     * Value
     * 
     * @var int
     */
    public $value;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get addressIdentifier
     * 
     * @return int
     */
    public function getAddressIdentifier()
    {
        return $this->addressIdentifier;
    }
    
    /**
     * Set addressIdentifier
     * 
     * @param int $value addressIdentifier
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setAddressIdentifier($value)
    {
        $this->addressIdentifier = $value;
        return $this;
    }
    
    /**
     * Get emailIdentifier
     * 
     * @return int
     */
    public function getEmailIdentifier()
    {
        return $this->emailIdentifier;
    }
    
    /**
     * Set emailIdentifier
     * 
     * @param int $value emailIdentifier
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setEmailIdentifier($value)
    {
        $this->emailIdentifier = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param int $value value
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerQualityService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GoneawayCustomerAddressDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
