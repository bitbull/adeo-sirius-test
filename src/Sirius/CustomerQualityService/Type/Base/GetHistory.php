<?php
/**
 * Parent model for GetHistory
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerQualityService\Type\Base;

abstract class GetHistory
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerQualityService\Type\Base\CustomerIdentificationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\CustomerIdentificationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\Base\CustomerIdentificationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\Base\GetHistory
     */
    public function setArg0(\Adeo\Sirius\CustomerQualityService\Type\Base\CustomerIdentificationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
