<?php
/**
 * Proxy for CustomerGetLoyaltyService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerGetLoyaltyService as CustomerGetLoyaltyServiceBase;
use \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerRequest;

class CustomerGetLoyaltyService
    extends CustomerGetLoyaltyServiceBase
{
    
    /**
     * Method getLoyaltyCustomer
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerRequest $request Value of request
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\GetLoyaltyCustomerResponse
     */
    public function getLoyaltyCustomer(GetLoyaltyCustomerRequest $request)
    {
        $response = parent::_getLoyaltyCustomer($request);
        return $response;
    }
}
