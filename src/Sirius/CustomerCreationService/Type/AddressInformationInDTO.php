<?php
/**
 * Model for AddressInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type;

use \Adeo\Sirius\CustomerCreationService\Type\Base\AddressInformationInDTO as AddressInformationInDTOBase;

class AddressInformationInDTO
    extends addressInformationInDTOBase
{
}
