<?php
/**
 * Parent model for FullLegalCustomerDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class FullLegalCustomerDTO
{
    
    /**
     * CustomerDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public $customerDTO;
    
    /**
     * AddressDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO
     */
    public $addressDTO;
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalContactDTO[]
     */
    public $contacts = array();
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Classifications
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public $classifications;
    
    /**
     * ExternalIdentifiers
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public $externalIdentifiers;
    
    /**
     * Leisures
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public $leisures;
    
    /**
     * Communities
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public $communities;
    
    /**
     * NotePad
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public $notePad;
    
    /**
     * Get customerDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function getCustomerDTO()
    {
        return $this->customerDTO;
    }
    
    /**
     * Set customerDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO $value customerDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setCustomerDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO $value)
    {
        $this->customerDTO = $value;
        return $this;
    }
    
    /**
     * Get addressDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO
     */
    public function getAddressDTO()
    {
        return $this->addressDTO;
    }
    
    /**
     * Set addressDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO $value addressDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setAddressDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO $value)
    {
        $this->addressDTO = $value;
        return $this;
    }
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalContactDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalContactDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Set classifications
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setClassifications(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value)
    {
        $this->classifications = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Set externalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setExternalIdentifiers(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value)
    {
        $this->externalIdentifiers = $value;
        return $this;
    }
    
    /**
     * Get leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public function getLeisures()
    {
        return $this->leisures;
    }
    
    /**
     * Set leisures
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setLeisures(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value)
    {
        $this->leisures = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Set communities
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setCommunities(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value)
    {
        $this->communities = $value;
        return $this;
    }
    
    /**
     * Get notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function getNotePad()
    {
        return $this->notePad;
    }
    
    /**
     * Set notePad
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function setNotePad(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value)
    {
        $this->notePad = $value;
        return $this;
    }
}
