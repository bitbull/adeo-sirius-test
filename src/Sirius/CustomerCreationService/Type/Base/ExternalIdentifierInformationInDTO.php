<?php
/**
 * Parent model for ExternalIdentifierInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class ExternalIdentifierInformationInDTO
{
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * Date
     * 
     * @var string
     */
    public $date;
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ExternalIdentifierInformationInDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ExternalIdentifierInformationInDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get date
     * 
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set date
     * 
     * @param string $value date
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ExternalIdentifierInformationInDTO
     */
    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ExternalIdentifierInformationInDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ExternalIdentifierInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
