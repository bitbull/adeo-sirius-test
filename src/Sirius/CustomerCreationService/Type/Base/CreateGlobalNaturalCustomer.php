<?php
/**
 * Parent model for CreateGlobalNaturalCustomer
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateGlobalNaturalCustomer
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateGlobalNaturalCustomer
     */
    public function setArg0(\Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
