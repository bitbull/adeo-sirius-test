<?php
/**
 * Parent model for CustomerCommunitiesDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CustomerCommunitiesDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Communities collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CommunityInformationInDTO[]
     */
    public $communities = array();
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunityInformationInDTO[]
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Add element on communities collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CommunityInformationInDTO[] $value communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function setCommunities($value = null)
    {
        $this->communities[] = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
