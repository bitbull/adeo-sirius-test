<?php
/**
 * Parent model for GlobalOptInInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class GlobalOptInInformationInDTO
{
    
    /**
     * OptInValue
     * 
     * @var boolean
     */
    public $optInValue;
    
    /**
     * OptInTypeCode
     * 
     * @var int
     */
    public $optInTypeCode;
    
    /**
     * UpdateDate
     * 
     * @var string
     */
    public $updateDate;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get optInValue
     * 
     * @return boolean
     */
    public function getOptInValue()
    {
        return $this->optInValue;
    }
    
    /**
     * Set optInValue
     * 
     * @param boolean $value optInValue
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO
     */
    public function setOptInValue($value)
    {
        $this->optInValue = $value;
        return $this;
    }
    
    /**
     * Get optInTypeCode
     * 
     * @return int
     */
    public function getOptInTypeCode()
    {
        return $this->optInTypeCode;
    }
    
    /**
     * Set optInTypeCode
     * 
     * @param int $value optInTypeCode
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO
     */
    public function setOptInTypeCode($value)
    {
        $this->optInTypeCode = $value;
        return $this;
    }
    
    /**
     * Get updateDate
     * 
     * @return string
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
    
    /**
     * Set updateDate
     * 
     * @param string $value updateDate
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO
     */
    public function setUpdateDate($value)
    {
        $this->updateDate = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
