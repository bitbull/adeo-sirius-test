<?php
/**
 * Parent model for CreateGlobalLegalEntityResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateGlobalLegalEntityResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateGlobalLegalEntityResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
