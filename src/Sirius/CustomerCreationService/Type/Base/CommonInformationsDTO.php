<?php
/**
 * Parent model for CommonInformationsDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CommonInformationsDTO
{
    
    /**
     * Classifications
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public $classifications;
    
    /**
     * ExternalIdentifiers
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public $externalIdentifiers;
    
    /**
     * Leisures
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public $leisures;
    
    /**
     * Communities
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public $communities;
    
    /**
     * NotePad
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public $notePad;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Set classifications
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setClassifications(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value)
    {
        $this->classifications = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Set externalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setExternalIdentifiers(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value)
    {
        $this->externalIdentifiers = $value;
        return $this;
    }
    
    /**
     * Get leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public function getLeisures()
    {
        return $this->leisures;
    }
    
    /**
     * Set leisures
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setLeisures(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value)
    {
        $this->leisures = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Set communities
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setCommunities(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value)
    {
        $this->communities = $value;
        return $this;
    }
    
    /**
     * Get notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function getNotePad()
    {
        return $this->notePad;
    }
    
    /**
     * Set notePad
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setNotePad(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value)
    {
        $this->notePad = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommonInformationsDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
