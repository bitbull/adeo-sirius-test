<?php
/**
 * Parent model for CreateGlobalLegalEntity
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateGlobalLegalEntity
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateGlobalLegalEntity
     */
    public function setArg0(\Adeo\Sirius\CustomerCreationService\Type\Base\FullLegalCustomerDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
