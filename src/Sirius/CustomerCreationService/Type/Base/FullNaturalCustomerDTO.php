<?php
/**
 * Parent model for FullNaturalCustomerDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class FullNaturalCustomerDTO
{
    
    /**
     * CustomerDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public $customerDTO;
    
    /**
     * Communications
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public $communications;
    
    /**
     * Classifications
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public $classifications;
    
    /**
     * ExternalIdentifiers
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public $externalIdentifiers;
    
    /**
     * Leisures
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public $leisures;
    
    /**
     * Communities
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public $communities;
    
    /**
     * NotePad
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public $notePad;
    
    /**
     * Get customerDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function getCustomerDTO()
    {
        return $this->customerDTO;
    }
    
    /**
     * Set customerDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO $value customerDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setCustomerDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO $value)
    {
        $this->customerDTO = $value;
        return $this;
    }
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Set communications
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO $value communications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setCommunications(\Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO $value)
    {
        $this->communications = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Set classifications
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setClassifications(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO $value)
    {
        $this->classifications = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Set externalIdentifiers
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setExternalIdentifiers(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerExternalIdentifiersDTO $value)
    {
        $this->externalIdentifiers = $value;
        return $this;
    }
    
    /**
     * Get leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO
     */
    public function getLeisures()
    {
        return $this->leisures;
    }
    
    /**
     * Set leisures
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value leisures
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setLeisures(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerLeisuresDTO $value)
    {
        $this->leisures = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Set communities
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value communities
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setCommunities(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerCommunitiesDTO $value)
    {
        $this->communities = $value;
        return $this;
    }
    
    /**
     * Get notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function getNotePad()
    {
        return $this->notePad;
    }
    
    /**
     * Set notePad
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value notePad
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO
     */
    public function setNotePad(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO $value)
    {
        $this->notePad = $value;
        return $this;
    }
}
