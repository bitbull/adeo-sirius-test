<?php
/**
 * Parent model for CreateGlobalNaturalCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateGlobalNaturalCustomerResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateGlobalNaturalCustomerResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
