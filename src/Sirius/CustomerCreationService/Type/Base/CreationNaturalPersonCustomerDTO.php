<?php
/**
 * Parent model for CreationNaturalPersonCustomerDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreationNaturalPersonCustomerDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * Title
     * 
     * @var int
     */
    public $title;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * BirthName
     * 
     * @var string
     */
    public $birthName;
    
    /**
     * BirthDate
     * 
     * @var string
     */
    public $birthDate;
    
    /**
     * ManagementEntityNumber
     * 
     * @var int
     */
    public $managementEntityNumber;
    
    /**
     * HouseholdIdentifier
     * 
     * @var int
     */
    public $householdIdentifier;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * MaritalStatus
     * 
     * @var int
     */
    public $maritalStatus;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * GlobalOptInList collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $globalOptInList = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get title
     * 
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set title
     * 
     * @param int $value title
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get birthName
     * 
     * @return string
     */
    public function getBirthName()
    {
        return $this->birthName;
    }
    
    /**
     * Set birthName
     * 
     * @param string $value birthName
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setBirthName($value)
    {
        $this->birthName = $value;
        return $this;
    }
    
    /**
     * Get birthDate
     * 
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /**
     * Set birthDate
     * 
     * @param string $value birthDate
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $value;
        return $this;
    }
    
    /**
     * Get managementEntityNumber
     * 
     * @return int
     */
    public function getManagementEntityNumber()
    {
        return $this->managementEntityNumber;
    }
    
    /**
     * Set managementEntityNumber
     * 
     * @param int $value managementEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setManagementEntityNumber($value)
    {
        $this->managementEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get householdIdentifier
     * 
     * @return int
     */
    public function getHouseholdIdentifier()
    {
        return $this->householdIdentifier;
    }
    
    /**
     * Set householdIdentifier
     * 
     * @param int $value householdIdentifier
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setHouseholdIdentifier($value)
    {
        $this->householdIdentifier = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get maritalStatus
     * 
     * @return int
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }
    
    /**
     * Set maritalStatus
     * 
     * @param int $value maritalStatus
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setMaritalStatus($value)
    {
        $this->maritalStatus = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get globalOptInList
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getGlobalOptInList()
    {
        return $this->globalOptInList;
    }
    
    /**
     * Add element on globalOptInList collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[] $value globalOptInList
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setGlobalOptInList($value = null)
    {
        $this->globalOptInList[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
