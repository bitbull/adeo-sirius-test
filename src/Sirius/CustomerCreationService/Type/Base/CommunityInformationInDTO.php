<?php
/**
 * Parent model for CommunityInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CommunityInformationInDTO
{
    
    /**
     * ValueCode
     * 
     * @var int
     */
    public $valueCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get valueCode
     * 
     * @return int
     */
    public function getValueCode()
    {
        return $this->valueCode;
    }
    
    /**
     * Set valueCode
     * 
     * @param int $value valueCode
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunityInformationInDTO
     */
    public function setValueCode($value)
    {
        $this->valueCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunityInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
