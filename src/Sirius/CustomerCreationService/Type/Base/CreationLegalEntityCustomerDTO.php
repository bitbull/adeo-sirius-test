<?php
/**
 * Parent model for CreationLegalEntityCustomerDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreationLegalEntityCustomerDTO
{
    
    /**
     * LegalEntityName
     * 
     * @var string
     */
    public $legalEntityName;
    
    /**
     * ManagementEntityNumber
     * 
     * @var int
     */
    public $managementEntityNumber;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * GlobalOptins collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $globalOptins = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get legalEntityName
     * 
     * @return string
     */
    public function getLegalEntityName()
    {
        return $this->legalEntityName;
    }
    
    /**
     * Set legalEntityName
     * 
     * @param string $value legalEntityName
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setLegalEntityName($value)
    {
        $this->legalEntityName = $value;
        return $this;
    }
    
    /**
     * Get managementEntityNumber
     * 
     * @return int
     */
    public function getManagementEntityNumber()
    {
        return $this->managementEntityNumber;
    }
    
    /**
     * Set managementEntityNumber
     * 
     * @param int $value managementEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setManagementEntityNumber($value)
    {
        $this->managementEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get globalOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getGlobalOptins()
    {
        return $this->globalOptins;
    }
    
    /**
     * Add element on globalOptins collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[] $value globalOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setGlobalOptins($value = null)
    {
        $this->globalOptins[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
