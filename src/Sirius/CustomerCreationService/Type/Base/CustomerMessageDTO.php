<?php
/**
 * Parent model for CustomerMessageDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CustomerMessageDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Subject
     * 
     * @var string
     */
    public $subject;
    
    /**
     * Message
     * 
     * @var string
     */
    public $message;
    
    /**
     * NotePadIdentifier
     * 
     * @var int
     */
    public $notePadIdentifier;
    
    /**
     * HistoryInputDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $historyInputDTO;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get subject
     * 
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * Set subject
     * 
     * @param string $value subject
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setSubject($value)
    {
        $this->subject = $value;
        return $this;
    }
    
    /**
     * Get message
     * 
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     * 
     * @param string $value message
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }
    
    /**
     * Get notePadIdentifier
     * 
     * @return int
     */
    public function getNotePadIdentifier()
    {
        return $this->notePadIdentifier;
    }
    
    /**
     * Set notePadIdentifier
     * 
     * @param int $value notePadIdentifier
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setNotePadIdentifier($value)
    {
        $this->notePadIdentifier = $value;
        return $this;
    }
    
    /**
     * Get historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInputDTO()
    {
        return $this->historyInputDTO;
    }
    
    /**
     * Set historyInputDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setHistoryInputDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInputDTO = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerMessageDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
