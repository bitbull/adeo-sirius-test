<?php
/**
 * Parent model for CreateLegalEntityCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateLegalEntityCustomerResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateLegalEntityCustomerResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerCreationService\Type\Base\CreationLegalEntityCustomerDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
