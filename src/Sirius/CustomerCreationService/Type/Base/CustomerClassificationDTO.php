<?php
/**
 * Parent model for CustomerClassificationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CustomerClassificationDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\ClassificationInformationInDTO[]
     */
    public $classifications = array();
    
    /**
     * HistoryInputDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $historyInputDTO;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\ClassificationInformationInDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\ClassificationInformationInDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInputDTO()
    {
        return $this->historyInputDTO;
    }
    
    /**
     * Set historyInputDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function setHistoryInputDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInputDTO = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerClassificationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
