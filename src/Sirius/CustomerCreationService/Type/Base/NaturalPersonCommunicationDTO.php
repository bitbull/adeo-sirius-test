<?php
/**
 * Parent model for NaturalPersonCommunicationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class NaturalPersonCommunicationDTO
{
    
    /**
     * Communications collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\SetCommunicationDTO[]
     */
    public $communications = array();
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * GlobalOptins collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO[]
     */
    public $globalOptins = array();
    
    /**
     * HistoryInputDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $historyInputDTO;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\SetCommunicationDTO[]
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Add element on communications collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\SetCommunicationDTO[] $value communications
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function setCommunications($value = null)
    {
        $this->communications[] = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get globalOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO[]
     */
    public function getGlobalOptins()
    {
        return $this->globalOptins;
    }
    
    /**
     * Add element on globalOptins collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO[] $value globalOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function setGlobalOptins($value = null)
    {
        $this->globalOptins[] = $value;
        return $this;
    }
    
    /**
     * Get historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistoryInputDTO()
    {
        return $this->historyInputDTO;
    }
    
    /**
     * Set historyInputDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value historyInputDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function setHistoryInputDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->historyInputDTO = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\NaturalPersonCommunicationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
