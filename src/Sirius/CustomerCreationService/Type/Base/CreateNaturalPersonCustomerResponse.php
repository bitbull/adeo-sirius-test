<?php
/**
 * Parent model for CreateNaturalPersonCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CreateNaturalPersonCustomerResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CreateNaturalPersonCustomerResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerCreationService\Type\Base\CreationNaturalPersonCustomerDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
