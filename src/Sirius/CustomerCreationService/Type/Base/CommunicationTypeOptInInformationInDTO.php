<?php
/**
 * Parent model for CommunicationTypeOptInInformationInDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class CommunicationTypeOptInInformationInDTO
{
    
    /**
     * CommunicationTypeCode
     * 
     * @var int
     */
    public $communicationTypeCode;
    
    /**
     * CommunicationTypeOptins collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public $communicationTypeOptins = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get communicationTypeCode
     * 
     * @return int
     */
    public function getCommunicationTypeCode()
    {
        return $this->communicationTypeCode;
    }
    
    /**
     * Set communicationTypeCode
     * 
     * @param int $value communicationTypeCode
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO
     */
    public function setCommunicationTypeCode($value)
    {
        $this->communicationTypeCode = $value;
        return $this;
    }
    
    /**
     * Get communicationTypeOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[]
     */
    public function getCommunicationTypeOptins()
    {
        return $this->communicationTypeOptins;
    }
    
    /**
     * Add element on communicationTypeOptins collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\GlobalOptInInformationInDTO[] $value communicationTypeOptins
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO
     */
    public function setCommunicationTypeOptins($value = null)
    {
        $this->communicationTypeOptins[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CommunicationTypeOptInInformationInDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
