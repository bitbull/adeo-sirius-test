<?php
/**
 * Parent model for FullNaturalCustomersCreationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerCreationService\Type\Base;

abstract class FullNaturalCustomersCreationDTO
{
    
    /**
     * HouseholdId
     * 
     * @var int
     */
    public $householdId;
    
    /**
     * AddressDTO
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO
     */
    public $addressDTO;
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO[]
     */
    public $contacts = array();
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public $history;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get householdId
     * 
     * @return int
     */
    public function getHouseholdId()
    {
        return $this->householdId;
    }
    
    /**
     * Set householdId
     * 
     * @param int $value householdId
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function setHouseholdId($value)
    {
        $this->householdId = $value;
        return $this;
    }
    
    /**
     * Get addressDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO
     */
    public function getAddressDTO()
    {
        return $this->addressDTO;
    }
    
    /**
     * Set addressDTO
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO $value addressDTO
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function setAddressDTO(\Adeo\Sirius\CustomerCreationService\Type\Base\CustomerAddressDTO $value)
    {
        $this->addressDTO = $value;
        return $this;
    }
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomerDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerCreationService\Type\Base\HistoryInputDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\Base\FullNaturalCustomersCreationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
