<?php
/**
 * Proxy for QuotationWebServiceImplService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\QuotationWebServiceImplService as QuotationWebServiceImplServiceBase;
use \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationRequest;

class QuotationWebServiceImplService
    extends QuotationWebServiceImplServiceBase
{
    
    /**
     * Method performQuotation
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationRequest $quotationRequest Value of quotationRequest
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\QuotationResponse
     */
    public function performQuotation(QuotationRequest $quotationRequest)
    {
        $response = parent::_performQuotation($quotationRequest);
        return $response;
    }
}
