<?php
/**
 * Proxy for CustomerGlobalService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerGlobalService as CustomerGlobalServiceBase;
use \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\UpdateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\GlobalSearch;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\SendJMS;
use \Adeo\Sirius\CustomerGlobalService\Type\CreateGlobalLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntity;
use \Adeo\Sirius\CustomerGlobalService\Type\GetHousehold;
use \Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO;

class CustomerGlobalService
    extends CustomerGlobalServiceBase
{
    /**
     * Method updateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO $houseold Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO
     */
    public function updateGlobalHousehold(FullHouseholdDTO $houseold)
    {
        $updateGlobalHousehold = new UpdateGlobalHousehold();
        $updateGlobalHousehold->setArg0($houseold);
        $response = parent::_updateGlobalHousehold($updateGlobalHousehold);
        return $response->getReturn();
    }
    
    /**
     * Method updateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO $legalEntity Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO
     */
    public function updateGlobalLegalEntity(FullLegalEntityDTO $legalEntity)
    {
        $updateGlobalLegalEntity = new UpdateGlobalLegalEntity();
        $updateGlobalLegalEntity->setArg0($legalEntity);
        $response = parent::_updateGlobalLegalEntity($updateGlobalLegalEntity);
        return $response->getReturn();
    }
    
    /**
     * Method globalSearch
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\SearchCriteriaDTO $criterias Search criterias
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\SearchResultDTO
     */
    public function globalSearch(\Adeo\Sirius\CustomerGlobalService\Type\SearchCriteriaDTO $criterias)
    {
        $globalSearch = new GlobalSearch();
        $globalSearch->setArg0($criterias);
        $response = parent::_globalSearch($globalSearch);
        return $response->getReturn();
    }
    
    /**
     * Method validateUpdateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalLegalEntityResponse
     */
    public function validateUpdateGlobalLegalEntity(ValidateUpdateGlobalLegalEntity $parameters)
    {
        $response = parent::_validateUpdateGlobalLegalEntity($parameters);
        return $response;
    }
    
    /**
     * Method createGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO $houseold Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\FullHouseholdDTO
     */
    public function createGlobalHousehold(FullHouseholdDTO $houseold)
    {
        $createGlobalHousehold = new CreateGlobalHousehold();
        $createGlobalHousehold->setArg0($houseold);
        $response = parent::_createGlobalHousehold($createGlobalHousehold);
        return $response->getReturn();
    }
    
    /**
     * Method validateCreateGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalLegalEntityResponse
     */
    public function validateCreateGlobalLegalEntity(ValidateCreateGlobalLegalEntity $parameters)
    {
        $response = parent::_validateCreateGlobalLegalEntity($parameters);
        return $response;
    }
    
    /**
     * Method validateCreateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateCreateGlobalHouseholdResponse
     */
    public function validateCreateGlobalHousehold(ValidateCreateGlobalHousehold $parameters)
    {
        $response = parent::_validateCreateGlobalHousehold($parameters);
        return $response;
    }
    
    /**
     * Method sendJMS
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\SendJMS $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\SendJMSResponse
     */
    public function sendJMS(SendJMS $parameters)
    {
        $response = parent::_sendJMS($parameters);
        return $response;
    }
    
    /**
     * Method createGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO $entity Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\FullLegalEntityDTO
     */
    public function createGlobalLegalEntity(FullLegalEntityDTO $entity)
    {
        $createGlobalLegalEntity = new CreateGlobalLegalEntity();
        $createGlobalLegalEntity->setArg0($entity);
        $response = parent::_createGlobalLegalEntity($createGlobalLegalEntity);
        return $response->getReturn();
    }
    
    /**
     * Method validateUpdateGlobalHousehold
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHousehold $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\ValidateUpdateGlobalHouseholdResponse
     */
    public function validateUpdateGlobalHousehold(ValidateUpdateGlobalHousehold $parameters)
    {
        $response = parent::_validateUpdateGlobalHousehold($parameters);
        return $response;
    }
    
    /**
     * Method getLegalEntity
     * 
     * @param int $legalEntityNumber Value of legalEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\GetLegalEntityResponse
     */
    public function getLegalEntity($legalEntityNumber)
    {
        $request = new GetLegalEntity();
        $request->legalEntityNumber = $legalEntityNumber;
        return parent::_getLegalEntity($request);
    }
    
    /**
     * Method getHousehold
     * 
     * @param int     $customerOrHouseholdNumber Value of customerOrHouseholdNumber
     * @param boolean $isCustomerNumber          Value of isCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\GetHouseholdResponse
     */
    public function getHousehold($customerOrHouseholdNumber, $isCustomerNumber)
    {
        $request = new GetHousehold();
        $request->customerOrHouseholdNumber = $customerOrHouseholdNumber;
        $request->isCustomerNumber = $isCustomerNumber;
        $response = parent::_getHousehold($request);
        return $response->getReturn();
    }
}
