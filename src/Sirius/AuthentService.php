<?php
/**
 * Proxy for AuthentService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\AuthentService as AuthentServiceBase;
use \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByte;
use \Adeo\Sirius\AuthentService\Type\DoRequestLDAP;
use \Adeo\Sirius\AuthentService\Type\DoAuthentification;
use \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByte;
use \Adeo\Sirius\AuthentService\Type\Ping;

class AuthentService
    extends AuthentServiceBase
{
    
    /**
     * Method doAuthentificationToByte
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByte $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoAuthentificationToByteResponse
     */
    public function doAuthentificationToByte(DoAuthentificationToByte $parameters)
    {
        $response = parent::_doAuthentificationToByte($parameters);
        return $response;
    }
    
    /**
     * Method doRequestLDAP
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoRequestLDAP $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoRequestLDAPResponse
     */
    public function doRequestLDAP(DoRequestLDAP $parameters)
    {
        $response = parent::_doRequestLDAP($parameters);
        return $response;
    }
    
    /**
     * Method doAuthentification
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoAuthentification $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoAuthentificationResponse
     */
    public function doAuthentification(DoAuthentification $parameters)
    {
        $response = parent::_doAuthentification($parameters);
        return $response;
    }
    
    /**
     * Method doRequestLDAPToByte
     * 
     * @param \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByte $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\DoRequestLDAPToByteResponse
     */
    public function doRequestLDAPToByte(DoRequestLDAPToByte $parameters)
    {
        $response = parent::_doRequestLDAPToByte($parameters);
        return $response;
    }
    
    /**
     * Method ping
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Ping $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\AuthentService\Type\PingResponse
     */
    public function ping(Ping $parameters)
    {
        $response = parent::_ping($parameters);
        return $response;
    }
}
