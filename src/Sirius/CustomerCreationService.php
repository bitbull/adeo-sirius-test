<?php
/**
 * Proxy for CustomerCreationService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerCreationService as CustomerCreationServiceBase;
use \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomer;
use \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntity;

class CustomerCreationService
    extends CustomerCreationServiceBase
{
    
    /**
     * Method createLegalEntityCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateLegalEntityCustomerResponse
     */
    public function createLegalEntityCustomer(CreateLegalEntityCustomer $parameters)
    {
        $response = parent::_createLegalEntityCustomer($parameters);
        return $response;
    }
    
    /**
     * Method createNaturalPersonCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateNaturalPersonCustomerResponse
     */
    public function createNaturalPersonCustomer(CreateNaturalPersonCustomer $parameters)
    {
        $response = parent::_createNaturalPersonCustomer($parameters);
        return $response;
    }
    
    /**
     * Method createGlobalNaturalCustomer
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalNaturalCustomerResponse
     */
    public function createGlobalNaturalCustomer(CreateGlobalNaturalCustomer $parameters)
    {
        $response = parent::_createGlobalNaturalCustomer($parameters);
        return $response;
    }
    
    /**
     * Method createGlobalLegalEntity
     * 
     * @param \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntity $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerCreationService\Type\CreateGlobalLegalEntityResponse
     */
    public function createGlobalLegalEntity(CreateGlobalLegalEntity $parameters)
    {
        $response = parent::_createGlobalLegalEntity($parameters);
        return $response;
    }
}
