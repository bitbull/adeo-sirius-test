<?php
/**
 * Proxy for CustomerSearchService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerSearchService as CustomerSearchServiceBase;
use \Adeo\Sirius\CustomerSearchService\Type\SearchCustomers;
use \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroups;
use \Adeo\Sirius\CustomerSearchService\Type\SearchContacts;

class CustomerSearchService
    extends CustomerSearchServiceBase
{
    
    /**
     * Method searchCustomers
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchCustomers $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchCustomersResponse
     */
    public function searchCustomers(SearchCustomers $parameters)
    {
        $response = parent::_searchCustomers($parameters);
        return $response;
    }
    
    /**
     * Method searchCustomerGroups
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroups $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchCustomerGroupsResponse
     */
    public function searchCustomerGroups(SearchCustomerGroups $parameters)
    {
        $response = parent::_searchCustomerGroups($parameters);
        return $response;
    }
    
    /**
     * Method searchContacts
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\SearchContacts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\SearchContactsResponse
     */
    public function searchContacts(SearchContacts $parameters)
    {
        $response = parent::_searchContacts($parameters);
        return $response;
    }
}
