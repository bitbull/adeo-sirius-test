<?php
/**
 * Proxy for CustomerPreExistingSearchService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerPreExistingSearchService as CustomerPreExistingSearchServiceBase;
use \Adeo\Sirius\CustomerPreExistingSearchService\Type\SearchPreExistingCustomer;
use \Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingSearchCriteriaDTO;

class CustomerPreExistingSearchService
    extends CustomerPreExistingSearchServiceBase
{
    
    /**
     * Method searchPreExistingCustomer
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\CustomerPreExistingSearchCriteriaDTO $customerPreExistingSearchCriteriaDTO Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO
     */
    public function searchPreExistingCustomer(CustomerPreExistingSearchCriteriaDTO $customerPreExistingSearchCriteriaDTO)
    {
        $searchPreExistingCustomer = new SearchPreExistingCustomer();
        $searchPreExistingCustomer->setArg0($customerPreExistingSearchCriteriaDTO);
        $response = parent::_searchPreExistingCustomer($searchPreExistingCustomer);
        return $response->getReturn();
    }
}
