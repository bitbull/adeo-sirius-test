<?php
/**
 * Parent model for GetAllPropertiesResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerParameterService\Type\Base;

abstract class GetAllPropertiesResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO[] $value return
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\Base\GetAllPropertiesResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
