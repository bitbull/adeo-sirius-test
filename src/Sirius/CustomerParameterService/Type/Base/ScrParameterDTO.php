<?php
/**
 * Parent model for ScrParameterDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerParameterService\Type\Base;

abstract class ScrParameterDTO
{
    
    /**
     * Key
     * 
     * @var string
     */
    public $key;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get key
     * 
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }
    
    /**
     * Set key
     * 
     * @param string $value key
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO
     */
    public function setKey($value)
    {
        $this->key = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerParameterService\Type\Base\ScrParameterDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
