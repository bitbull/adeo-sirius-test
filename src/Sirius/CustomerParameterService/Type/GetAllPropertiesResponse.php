<?php
/**
 * Model for GetAllPropertiesResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerParameterService\Type;

use \Adeo\Sirius\CustomerParameterService\Type\Base\GetAllPropertiesResponse as GetAllPropertiesResponseBase;

class GetAllPropertiesResponse
    extends getAllPropertiesResponseBase
{
}
