<?php
/**
 * Parent model for OptinDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class OptinDTO
{
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * Value
     * 
     * @var boolean
     */
    public $value;
    
    /**
     * CreationDate
     * 
     * @var string
     */
    public $creationDate;
    
    /**
     * UpdateDate
     * 
     * @var string
     */
    public $updateDate;
    
    /**
     * CommunicationType
     * 
     * @var int
     */
    public $communicationType;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param boolean $value value
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get creationDate
     * 
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    /**
     * Set creationDate
     * 
     * @param string $value creationDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setCreationDate($value)
    {
        $this->creationDate = $value;
        return $this;
    }
    
    /**
     * Get updateDate
     * 
     * @return string
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
    
    /**
     * Set updateDate
     * 
     * @param string $value updateDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setUpdateDate($value)
    {
        $this->updateDate = $value;
        return $this;
    }
    
    /**
     * Get communicationType
     * 
     * @return int
     */
    public function getCommunicationType()
    {
        return $this->communicationType;
    }
    
    /**
     * Set communicationType
     * 
     * @param int $value communicationType
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setCommunicationType($value)
    {
        $this->communicationType = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
