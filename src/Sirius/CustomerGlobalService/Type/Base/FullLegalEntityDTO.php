<?php
/**
 * Parent model for FullLegalEntityDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class FullLegalEntityDTO
{
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public $history;
    
    /**
     * LegalCustomerNumber
     * 
     * @var int
     */
    public $legalCustomerNumber;
    
    /**
     * MasterNumber
     * 
     * @var int
     */
    public $masterNumber;
    
    /**
     * LegalIdentity
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public $legalIdentity;
    
    /**
     * Addresses collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[]
     */
    public $addresses = array();
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO[]
     */
    public $contacts = array();
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public $optins = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[]
     */
    public $classifications = array();
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Communities collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[]
     */
    public $communities = array();
    
    /**
     * NotePads collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[]
     */
    public $notePads = array();
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get legalCustomerNumber
     * 
     * @return int
     */
    public function getLegalCustomerNumber()
    {
        return $this->legalCustomerNumber;
    }
    
    /**
     * Set legalCustomerNumber
     * 
     * @param int $value legalCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setLegalCustomerNumber($value)
    {
        $this->legalCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get masterNumber
     * 
     * @return int
     */
    public function getMasterNumber()
    {
        return $this->masterNumber;
    }
    
    /**
     * Set masterNumber
     * 
     * @param int $value masterNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setMasterNumber($value)
    {
        $this->masterNumber = $value;
        return $this;
    }
    
    /**
     * Get legalIdentity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function getLegalIdentity()
    {
        return $this->legalIdentity;
    }
    
    /**
     * Set legalIdentity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO $value legalIdentity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setLegalIdentity(\Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO $value)
    {
        $this->legalIdentity = $value;
        return $this;
    }
    
    /**
     * Get addresses
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    
    /**
     * Add element on addresses collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[] $value addresses
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setAddresses($value = null)
    {
        $this->addresses[] = $value;
        return $this;
    }
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[]
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Add element on communities collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[] $value communities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setCommunities($value = null)
    {
        $this->communities[] = $value;
        return $this;
    }
    
    /**
     * Get notePads
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[]
     */
    public function getNotePads()
    {
        return $this->notePads;
    }
    
    /**
     * Add element on notePads collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[] $value notePads
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setNotePads($value = null)
    {
        $this->notePads[] = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
