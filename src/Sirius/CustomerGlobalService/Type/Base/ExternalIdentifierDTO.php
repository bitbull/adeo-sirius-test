<?php
/**
 * Parent model for ExternalIdentifierDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class ExternalIdentifierDTO
{
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * ExternalDate
     * 
     * @var string
     */
    public $externalDate;
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get externalDate
     * 
     * @return string
     */
    public function getExternalDate()
    {
        return $this->externalDate;
    }
    
    /**
     * Set externalDate
     * 
     * @param string $value externalDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setExternalDate($value)
    {
        $this->externalDate = $value;
        return $this;
    }
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
