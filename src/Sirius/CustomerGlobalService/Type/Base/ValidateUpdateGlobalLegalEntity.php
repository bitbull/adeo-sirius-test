<?php
/**
 * Parent model for ValidateUpdateGlobalLegalEntity
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class ValidateUpdateGlobalLegalEntity
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ValidateUpdateGlobalLegalEntity
     */
    public function setArg0(\Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
