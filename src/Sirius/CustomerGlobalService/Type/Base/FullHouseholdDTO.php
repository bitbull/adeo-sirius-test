<?php
/**
 * Parent model for FullHouseholdDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class FullHouseholdDTO
{
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public $history;
    
    /**
     * HouseholdId
     * 
     * @var int
     */
    public $householdId;
    
    /**
     * Addresses collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[]
     */
    public $addresses = array();
    
    /**
     * Contacts collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullNaturalCustomerDTO[]
     */
    public $contacts = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function setHistory(\Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value)
    {
        $this->history = $value;
        return $this;
    }
    
    /**
     * Get householdId
     * 
     * @return int
     */
    public function getHouseholdId()
    {
        return $this->householdId;
    }
    
    /**
     * Set householdId
     * 
     * @param int $value householdId
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function setHouseholdId($value)
    {
        $this->householdId = $value;
        return $this;
    }
    
    /**
     * Get addresses
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    
    /**
     * Add element on addresses collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO[] $value addresses
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function setAddresses($value = null)
    {
        $this->addresses[] = $value;
        return $this;
    }
    
    /**
     * Get contacts
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullNaturalCustomerDTO[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    
    /**
     * Add element on contacts collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullNaturalCustomerDTO[] $value contacts
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function setContacts($value = null)
    {
        $this->contacts[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
