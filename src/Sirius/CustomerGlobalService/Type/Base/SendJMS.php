<?php
/**
 * Parent model for SendJMS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class SendJMS
{
    
    /**
     * HouseholdId
     * 
     * @var int
     */
    public $householdId;
    
    /**
     * LegalEntityNumber
     * 
     * @var int
     */
    public $legalEntityNumber;
    
    /**
     * History
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public $history;
    
    /**
     * Get householdId
     * 
     * @return int
     */
    public function getHouseholdId()
    {
        return $this->householdId;
    }
    
    /**
     * Set householdId
     * 
     * @param int $value householdId
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SendJMS
     */
    public function setHouseholdId($value)
    {
        $this->householdId = $value;
        return $this;
    }
    
    /**
     * Get legalEntityNumber
     * 
     * @return int
     */
    public function getLegalEntityNumber()
    {
        return $this->legalEntityNumber;
    }
    
    /**
     * Set legalEntityNumber
     * 
     * @param int $value legalEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SendJMS
     */
    public function setLegalEntityNumber($value)
    {
        $this->legalEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    /**
     * Set history
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value history
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SendJMS
     */
    public function setHistory(\Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO $value)
    {
        $this->history = $value;
        return $this;
    }
}
