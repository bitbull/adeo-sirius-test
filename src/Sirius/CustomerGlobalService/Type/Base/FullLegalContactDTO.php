<?php
/**
 * Parent model for FullLegalContactDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class FullLegalContactDTO
{
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * MasterNumber
     * 
     * @var int
     */
    public $masterNumber;
    
    /**
     * Identity
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public $identity;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public $optins = array();
    
    /**
     * Communications collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO[]
     */
    public $communications = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[]
     */
    public $classifications = array();
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Communities collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[]
     */
    public $communities = array();
    
    /**
     * NotePads collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[]
     */
    public $notePads = array();
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get masterNumber
     * 
     * @return int
     */
    public function getMasterNumber()
    {
        return $this->masterNumber;
    }
    
    /**
     * Set masterNumber
     * 
     * @param int $value masterNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setMasterNumber($value)
    {
        $this->masterNumber = $value;
        return $this;
    }
    
    /**
     * Get identity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function getIdentity()
    {
        return $this->identity;
    }
    
    /**
     * Set identity
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO $value identity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setIdentity(\Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO $value)
    {
        $this->identity = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO[]
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Add element on communications collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO[] $value communications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setCommunications($value = null)
    {
        $this->communications[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ClassificationDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ExternalIdentifierDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get communities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[]
     */
    public function getCommunities()
    {
        return $this->communities;
    }
    
    /**
     * Add element on communities collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunityDTO[] $value communities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setCommunities($value = null)
    {
        $this->communities[] = $value;
        return $this;
    }
    
    /**
     * Get notePads
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[]
     */
    public function getNotePads()
    {
        return $this->notePads;
    }
    
    /**
     * Add element on notePads collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO[] $value notePads
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setNotePads($value = null)
    {
        $this->notePads[] = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalContactDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
