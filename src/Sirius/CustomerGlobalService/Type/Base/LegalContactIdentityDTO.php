<?php
/**
 * Parent model for LegalContactIdentityDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class LegalContactIdentityDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * Title
     * 
     * @var int
     */
    public $title;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * BirthName
     * 
     * @var string
     */
    public $birthName;
    
    /**
     * BirthDate
     * 
     * @var string
     */
    public $birthDate;
    
    /**
     * ManagementEntity
     * 
     * @var int
     */
    public $managementEntity;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * SortOrder
     * 
     * @var int
     */
    public $sortOrder;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get title
     * 
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set title
     * 
     * @param int $value title
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get birthName
     * 
     * @return string
     */
    public function getBirthName()
    {
        return $this->birthName;
    }
    
    /**
     * Set birthName
     * 
     * @param string $value birthName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setBirthName($value)
    {
        $this->birthName = $value;
        return $this;
    }
    
    /**
     * Get birthDate
     * 
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /**
     * Set birthDate
     * 
     * @param string $value birthDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $value;
        return $this;
    }
    
    /**
     * Get managementEntity
     * 
     * @return int
     */
    public function getManagementEntity()
    {
        return $this->managementEntity;
    }
    
    /**
     * Set managementEntity
     * 
     * @param int $value managementEntity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setManagementEntity($value)
    {
        $this->managementEntity = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get sortOrder
     * 
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    
    /**
     * Set sortOrder
     * 
     * @param int $value sortOrder
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setSortOrder($value)
    {
        $this->sortOrder = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalContactIdentityDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
