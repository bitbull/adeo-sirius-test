<?php
/**
 * Parent model for GetHouseholdResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class GetHouseholdResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GetHouseholdResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
