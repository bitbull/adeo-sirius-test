<?php
/**
 * Parent model for SiriusBusinessFault
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class SiriusBusinessFault
{
    
    /**
     * ErrorCodes collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\String[]
     */
    public $errorCodes = array();
    
    /**
     * Message
     * 
     * @var string
     */
    public $message;
    
    /**
     * Get errorCodes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\String[]
     */
    public function getErrorCodes()
    {
        return $this->errorCodes;
    }
    
    /**
     * Add element on errorCodes collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\String[] $value errorCodes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SiriusBusinessFault
     */
    public function setErrorCodes($value)
    {
        $this->errorCodes[] = $value;
        return $this;
    }
    
    /**
     * Get message
     * 
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     * 
     * @param string $value message
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SiriusBusinessFault
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }
}
