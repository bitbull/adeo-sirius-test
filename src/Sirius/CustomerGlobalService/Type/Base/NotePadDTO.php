<?php
/**
 * Parent model for NotePadDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class NotePadDTO
{
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Subject
     * 
     * @var string
     */
    public $subject;
    
    /**
     * Message
     * 
     * @var string
     */
    public $message;
    
    /**
     * FullName
     * 
     * @var string
     */
    public $fullName;
    
    /**
     * Ldap
     * 
     * @var string
     */
    public $ldap;
    
    /**
     * CreationDate
     * 
     * @var string
     */
    public $creationDate;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get subject
     * 
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * Set subject
     * 
     * @param string $value subject
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setSubject($value)
    {
        $this->subject = $value;
        return $this;
    }
    
    /**
     * Get message
     * 
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     * 
     * @param string $value message
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }
    
    /**
     * Get fullName
     * 
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }
    
    /**
     * Set fullName
     * 
     * @param string $value fullName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setFullName($value)
    {
        $this->fullName = $value;
        return $this;
    }
    
    /**
     * Get ldap
     * 
     * @return string
     */
    public function getLdap()
    {
        return $this->ldap;
    }
    
    /**
     * Set ldap
     * 
     * @param string $value ldap
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setLdap($value)
    {
        $this->ldap = $value;
        return $this;
    }
    
    /**
     * Get creationDate
     * 
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    /**
     * Set creationDate
     * 
     * @param string $value creationDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setCreationDate($value)
    {
        $this->creationDate = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\NotePadDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
