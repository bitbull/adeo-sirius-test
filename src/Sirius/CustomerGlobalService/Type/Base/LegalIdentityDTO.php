<?php
/**
 * Parent model for LegalIdentityDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class LegalIdentityDTO
{
    
    /**
     * CorporateName
     * 
     * @var string
     */
    public $corporateName;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * ManagementEntity
     * 
     * @var int
     */
    public $managementEntity;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get corporateName
     * 
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }
    
    /**
     * Set corporateName
     * 
     * @param string $value corporateName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function setCorporateName($value)
    {
        $this->corporateName = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get managementEntity
     * 
     * @return int
     */
    public function getManagementEntity()
    {
        return $this->managementEntity;
    }
    
    /**
     * Set managementEntity
     * 
     * @param int $value managementEntity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function setManagementEntity($value)
    {
        $this->managementEntity = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\LegalIdentityDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
