<?php
/**
 * Parent model for HousingDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class HousingDTO
{
    
    /**
     * Detail
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public $detail;
    
    /**
     * Characteristics collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO[]
     */
    public $characteristics = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Set detail
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO $value detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO
     */
    public function setDetail(\Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO $value)
    {
        $this->detail = $value;
        return $this;
    }
    
    /**
     * Get characteristics
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO[]
     */
    public function getCharacteristics()
    {
        return $this->characteristics;
    }
    
    /**
     * Add element on characteristics collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO[] $value characteristics
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO
     */
    public function setCharacteristics($value = null)
    {
        $this->characteristics[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
