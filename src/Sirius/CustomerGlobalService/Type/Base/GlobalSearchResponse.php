<?php
/**
 * Parent model for GlobalSearchResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class GlobalSearchResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GlobalSearchResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
