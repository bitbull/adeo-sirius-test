<?php
/**
 * Parent model for GlobalSearch
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class GlobalSearch
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GlobalSearch
     */
    public function setArg0(\Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
