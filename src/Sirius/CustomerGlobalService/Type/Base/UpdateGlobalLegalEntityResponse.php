<?php
/**
 * Parent model for UpdateGlobalLegalEntityResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class UpdateGlobalLegalEntityResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\UpdateGlobalLegalEntityResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
