<?php
/**
 * Parent model for ErrorCodeAndDetail
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class ErrorCodeAndDetail
{
    
    /**
     * Detail
     * 
     * @var string
     */
    public $detail;
    
    /**
     * ErrorCode
     * 
     * @var string
     */
    public $errorCode;
    
    /**
     * Get detail
     * 
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Set detail
     * 
     * @param string $value detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ErrorCodeAndDetail
     */
    public function setDetail($value)
    {
        $this->detail = $value;
        return $this;
    }
    
    /**
     * Get errorCode
     * 
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    
    /**
     * Set errorCode
     * 
     * @param string $value errorCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ErrorCodeAndDetail
     */
    public function setErrorCode($value)
    {
        $this->errorCode = $value;
        return $this;
    }
}
