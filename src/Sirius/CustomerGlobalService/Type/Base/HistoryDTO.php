<?php
/**
 * Parent model for HistoryDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class HistoryDTO
{
    
    /**
     * OperatorIdentifier
     * 
     * @var string
     */
    public $operatorIdentifier;
    
    /**
     * OperatorFullName
     * 
     * @var string
     */
    public $operatorFullName;
    
    /**
     * ApplicationCode
     * 
     * @var string
     */
    public $applicationCode;
    
    /**
     * EntityNumber
     * 
     * @var int
     */
    public $entityNumber;
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * OperatorIdentifierType
     * 
     * @var string
     */
    public $operatorIdentifierType;
    
    /**
     * BusinessAction
     * 
     * @var string
     */
    public $businessAction;
    
    /**
     * BusinessDate
     * 
     * @var string
     */
    public $businessDate;
    
    /**
     * ForcedAction
     * 
     * @var boolean
     */
    public $forcedAction;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get operatorIdentifier
     * 
     * @return string
     */
    public function getOperatorIdentifier()
    {
        return $this->operatorIdentifier;
    }
    
    /**
     * Set operatorIdentifier
     * 
     * @param string $value operatorIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setOperatorIdentifier($value)
    {
        $this->operatorIdentifier = $value;
        return $this;
    }
    
    /**
     * Get operatorFullName
     * 
     * @return string
     */
    public function getOperatorFullName()
    {
        return $this->operatorFullName;
    }
    
    /**
     * Set operatorFullName
     * 
     * @param string $value operatorFullName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setOperatorFullName($value)
    {
        $this->operatorFullName = $value;
        return $this;
    }
    
    /**
     * Get applicationCode
     * 
     * @return string
     */
    public function getApplicationCode()
    {
        return $this->applicationCode;
    }
    
    /**
     * Set applicationCode
     * 
     * @param string $value applicationCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setApplicationCode($value)
    {
        $this->applicationCode = $value;
        return $this;
    }
    
    /**
     * Get entityNumber
     * 
     * @return int
     */
    public function getEntityNumber()
    {
        return $this->entityNumber;
    }
    
    /**
     * Set entityNumber
     * 
     * @param int $value entityNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setEntityNumber($value)
    {
        $this->entityNumber = $value;
        return $this;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get operatorIdentifierType
     * 
     * @return string
     */
    public function getOperatorIdentifierType()
    {
        return $this->operatorIdentifierType;
    }
    
    /**
     * Set operatorIdentifierType
     * 
     * @param string $value operatorIdentifierType
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setOperatorIdentifierType($value)
    {
        $this->operatorIdentifierType = $value;
        return $this;
    }
    
    /**
     * Get businessAction
     * 
     * @return string
     */
    public function getBusinessAction()
    {
        return $this->businessAction;
    }
    
    /**
     * Set businessAction
     * 
     * @param string $value businessAction
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setBusinessAction($value)
    {
        $this->businessAction = $value;
        return $this;
    }
    
    /**
     * Get businessDate
     * 
     * @return string
     */
    public function getBusinessDate()
    {
        return $this->businessDate;
    }
    
    /**
     * Set businessDate
     * 
     * @param string $value businessDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setBusinessDate($value)
    {
        $this->businessDate = $value;
        return $this;
    }
    
    /**
     * Get forcedAction
     * 
     * @return boolean
     */
    public function getForcedAction()
    {
        return $this->forcedAction;
    }
    
    /**
     * Set forcedAction
     * 
     * @param boolean $value forcedAction
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setForcedAction($value)
    {
        $this->forcedAction = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HistoryDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
