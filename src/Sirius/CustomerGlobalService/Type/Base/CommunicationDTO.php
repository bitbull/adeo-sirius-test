<?php
/**
 * Parent model for CommunicationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class CommunicationDTO
{
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Detail
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public $detail;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public $optins = array();
    
    /**
     * Scopes collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ScopeDTO[]
     */
    public $scopes = array();
    
    /**
     * GoneAway
     * 
     * @var boolean
     */
    public $goneAway;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Set detail
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO $value detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setDetail(\Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO $value)
    {
        $this->detail = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get scopes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ScopeDTO[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }
    
    /**
     * Add element on scopes collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ScopeDTO[] $value scopes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setScopes($value = null)
    {
        $this->scopes[] = $value;
        return $this;
    }
    
    /**
     * Get goneAway
     * 
     * @return boolean
     */
    public function getGoneAway()
    {
        return $this->goneAway;
    }
    
    /**
     * Set goneAway
     * 
     * @param boolean $value goneAway
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setGoneAway($value)
    {
        $this->goneAway = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
