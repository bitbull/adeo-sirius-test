<?php
/**
 * Parent model for GetLegalEntity
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class GetLegalEntity
{
    
    /**
     * LegalEntityNumber
     * 
     * @var int
     */
    public $legalEntityNumber;
    
    /**
     * Get legalEntityNumber
     * 
     * @return int
     */
    public function getLegalEntityNumber()
    {
        return $this->legalEntityNumber;
    }
    
    /**
     * Set legalEntityNumber
     * 
     * @param int $value legalEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GetLegalEntity
     */
    public function setLegalEntityNumber($value)
    {
        $this->legalEntityNumber = $value;
        return $this;
    }
}
