<?php
/**
 * Parent model for SearchCriteriaDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class SearchCriteriaDTO
{
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * PostalCode
     * 
     * @var string
     */
    public $postalCode;
    
    /**
     * CityOrProvince
     * 
     * @var string
     */
    public $cityOrProvince;
    
    /**
     * Country
     * 
     * @var string
     */
    public $country;
    
    /**
     * CustomerStatus
     * 
     * @var int
     */
    public $customerStatus;
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * AddressExternalCode1
     * 
     * @var string
     */
    public $addressExternalCode1;
    
    /**
     * AddressExternalCode2
     * 
     * @var string
     */
    public $addressExternalCode2;
    
    /**
     * MaxNumberOfResults
     * 
     * @var int
     */
    public $maxNumberOfResults;
    
    /**
     * DiscriminatingCriteria
     * 
     * @var string
     */
    public $discriminatingCriteria;
    
    /**
     * DiscriminatingCriteriaType
     * 
     * @var string
     */
    public $discriminatingCriteriaType;
    
    /**
     * ExternalIdTypeOrCommunicationType
     * 
     * @var int
     */
    public $externalIdTypeOrCommunicationType;
    
    /**
     * DetailedSearch
     * 
     * @var boolean
     */
    public $detailedSearch;
    
    /**
     * DeletedAlso
     * 
     * @var boolean
     */
    public $deletedAlso;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get postalCode
     * 
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * Set postalCode
     * 
     * @param string $value postalCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setPostalCode($value)
    {
        $this->postalCode = $value;
        return $this;
    }
    
    /**
     * Get cityOrProvince
     * 
     * @return string
     */
    public function getCityOrProvince()
    {
        return $this->cityOrProvince;
    }
    
    /**
     * Set cityOrProvince
     * 
     * @param string $value cityOrProvince
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setCityOrProvince($value)
    {
        $this->cityOrProvince = $value;
        return $this;
    }
    
    /**
     * Get country
     * 
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set country
     * 
     * @param string $value country
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setCountry($value)
    {
        $this->country = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return int
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Set customerStatus
     * 
     * @param int $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setCustomerStatus($value)
    {
        $this->customerStatus = $value;
        return $this;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get addressExternalCode1
     * 
     * @return string
     */
    public function getAddressExternalCode1()
    {
        return $this->addressExternalCode1;
    }
    
    /**
     * Set addressExternalCode1
     * 
     * @param string $value addressExternalCode1
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setAddressExternalCode1($value)
    {
        $this->addressExternalCode1 = $value;
        return $this;
    }
    
    /**
     * Get addressExternalCode2
     * 
     * @return string
     */
    public function getAddressExternalCode2()
    {
        return $this->addressExternalCode2;
    }
    
    /**
     * Set addressExternalCode2
     * 
     * @param string $value addressExternalCode2
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setAddressExternalCode2($value)
    {
        $this->addressExternalCode2 = $value;
        return $this;
    }
    
    /**
     * Get maxNumberOfResults
     * 
     * @return int
     */
    public function getMaxNumberOfResults()
    {
        return $this->maxNumberOfResults;
    }
    
    /**
     * Set maxNumberOfResults
     * 
     * @param int $value maxNumberOfResults
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setMaxNumberOfResults($value)
    {
        $this->maxNumberOfResults = $value;
        return $this;
    }
    
    /**
     * Get discriminatingCriteria
     * 
     * @return string
     */
    public function getDiscriminatingCriteria()
    {
        return $this->discriminatingCriteria;
    }
    
    /**
     * Set discriminatingCriteria
     * 
     * @param string $value discriminatingCriteria
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setDiscriminatingCriteria($value)
    {
        $this->discriminatingCriteria = $value;
        return $this;
    }
    
    /**
     * Get discriminatingCriteriaType
     * 
     * @return string
     */
    public function getDiscriminatingCriteriaType()
    {
        return $this->discriminatingCriteriaType;
    }
    
    /**
     * Set discriminatingCriteriaType
     * 
     * @param string $value discriminatingCriteriaType
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setDiscriminatingCriteriaType($value)
    {
        $this->discriminatingCriteriaType = $value;
        return $this;
    }
    
    /**
     * Get externalIdTypeOrCommunicationType
     * 
     * @return int
     */
    public function getExternalIdTypeOrCommunicationType()
    {
        return $this->externalIdTypeOrCommunicationType;
    }
    
    /**
     * Set externalIdTypeOrCommunicationType
     * 
     * @param int $value externalIdTypeOrCommunicationType
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setExternalIdTypeOrCommunicationType($value)
    {
        $this->externalIdTypeOrCommunicationType = $value;
        return $this;
    }
    
    /**
     * Get detailedSearch
     * 
     * @return boolean
     */
    public function getDetailedSearch()
    {
        return $this->detailedSearch;
    }
    
    /**
     * Set detailedSearch
     * 
     * @param boolean $value detailedSearch
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setDetailedSearch($value)
    {
        $this->detailedSearch = $value;
        return $this;
    }
    
    /**
     * Get deletedAlso
     * 
     * @return boolean
     */
    public function getDeletedAlso()
    {
        return $this->deletedAlso;
    }
    
    /**
     * Set deletedAlso
     * 
     * @param boolean $value deletedAlso
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setDeletedAlso($value)
    {
        $this->deletedAlso = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchCriteriaDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
