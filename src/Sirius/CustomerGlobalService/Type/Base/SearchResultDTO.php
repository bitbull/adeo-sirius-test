<?php
/**
 * Parent model for SearchResultDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class SearchResultDTO
{
    
    /**
     * Households collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO[]
     */
    public $households = array();
    
    /**
     * LegalEntities collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO[]
     */
    public $legalEntities = array();
    
    /**
     * CustomersFound collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[]
     */
    public $customersFound = array();
    
    /**
     * UndeletableCustomers collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[]
     */
    public $undeletableCustomers = array();
    
    /**
     * MoreResultsAvailable
     * 
     * @var boolean
     */
    public $moreResultsAvailable;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get households
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO[]
     */
    public function getHouseholds()
    {
        return $this->households;
    }
    
    /**
     * Add element on households collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO[] $value households
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setHouseholds($value = null)
    {
        $this->households[] = $value;
        return $this;
    }
    
    /**
     * Get legalEntities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO[]
     */
    public function getLegalEntities()
    {
        return $this->legalEntities;
    }
    
    /**
     * Add element on legalEntities collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullLegalEntityDTO[] $value legalEntities
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setLegalEntities($value = null)
    {
        $this->legalEntities[] = $value;
        return $this;
    }
    
    /**
     * Get customersFound
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[]
     */
    public function getCustomersFound()
    {
        return $this->customersFound;
    }
    
    /**
     * Add element on customersFound collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[] $value customersFound
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setCustomersFound($value = null)
    {
        $this->customersFound[] = $value;
        return $this;
    }
    
    /**
     * Get undeletableCustomers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[]
     */
    public function getUndeletableCustomers()
    {
        return $this->undeletableCustomers;
    }
    
    /**
     * Add element on undeletableCustomers collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\Int[] $value undeletableCustomers
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setUndeletableCustomers($value = null)
    {
        $this->undeletableCustomers[] = $value;
        return $this;
    }
    
    /**
     * Get moreResultsAvailable
     * 
     * @return boolean
     */
    public function getMoreResultsAvailable()
    {
        return $this->moreResultsAvailable;
    }
    
    /**
     * Set moreResultsAvailable
     * 
     * @param boolean $value moreResultsAvailable
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setMoreResultsAvailable($value)
    {
        $this->moreResultsAvailable = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SearchResultDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
