<?php
/**
 * Parent model for ValidateCreateGlobalHouseholdResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class ValidateCreateGlobalHouseholdResponse
{
    
    /**
     * Return collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\ErrorCodeAndDetail[]
     */
    public $return = array();
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ErrorCodeAndDetail[]
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Add element on return collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\ErrorCodeAndDetail[] $value return
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\ValidateCreateGlobalHouseholdResponse
     */
    public function setReturn($value)
    {
        $this->return[] = $value;
        return $this;
    }
}
