<?php
/**
 * Parent model for HousingCharacteristicDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class HousingCharacteristicDTO
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code;
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingCharacteristicDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
