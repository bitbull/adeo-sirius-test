<?php
/**
 * Parent model for UpdateGlobalHousehold
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class UpdateGlobalHousehold
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\UpdateGlobalHousehold
     */
    public function setArg0(\Adeo\Sirius\CustomerGlobalService\Type\Base\FullHouseholdDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
