<?php
/**
 * Parent model for AddressDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class AddressDTO
{
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Detail
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public $detail;
    
    /**
     * GoneAway
     * 
     * @var boolean
     */
    public $goneAway;
    
    /**
     * Optins collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public $optins = array();
    
    /**
     * Types collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressTypeDTO[]
     */
    public $types = array();
    
    /**
     * ExternalCodes collection
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO[]
     */
    public $externalCodes = array();
    
    /**
     * Housing
     * 
     * @var \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO
     */
    public $housing;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Set detail
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO $value detail
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setDetail(\Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO $value)
    {
        $this->detail = $value;
        return $this;
    }
    
    /**
     * Get goneAway
     * 
     * @return boolean
     */
    public function getGoneAway()
    {
        return $this->goneAway;
    }
    
    /**
     * Set goneAway
     * 
     * @param boolean $value goneAway
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setGoneAway($value)
    {
        $this->goneAway = $value;
        return $this;
    }
    
    /**
     * Get optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[]
     */
    public function getOptins()
    {
        return $this->optins;
    }
    
    /**
     * Add element on optins collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\OptinDTO[] $value optins
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setOptins($value = null)
    {
        $this->optins[] = $value;
        return $this;
    }
    
    /**
     * Get types
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressTypeDTO[]
     */
    public function getTypes()
    {
        return $this->types;
    }
    
    /**
     * Add element on types collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressTypeDTO[] $value types
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setTypes($value = null)
    {
        $this->types[] = $value;
        return $this;
    }
    
    /**
     * Get externalCodes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO[]
     */
    public function getExternalCodes()
    {
        return $this->externalCodes;
    }
    
    /**
     * Add element on externalCodes collection
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO[] $value externalCodes
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setExternalCodes($value = null)
    {
        $this->externalCodes[] = $value;
        return $this;
    }
    
    /**
     * Get housing
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO
     */
    public function getHousing()
    {
        return $this->housing;
    }
    
    /**
     * Set housing
     * 
     * @param \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO $value housing
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setHousing(\Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDTO $value)
    {
        $this->housing = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
