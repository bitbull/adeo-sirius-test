<?php
/**
 * Parent model for CommunicationDetailDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class CommunicationDetailDTO
{

    /**
     * Value
     *
     * @var string
     */
    public $value;

    /**
     * Type
     *
     * @var int
     */
    public $type;

    /**
     * PhoneType
     *
     * @var int
     */
    public $phoneType;

    /**
     * Order
     *
     * @var int
     */
    public $order;

    /**
     * Main
     *
     * @var boolean
     */
    public $main;

    /**
     * Country
     *
     * @var string
     */
    public $country;

    /**
     * Any
     *
     * @var \SoapVar
     */
    public $any;

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @param string $value value
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param int $value type
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    /**
     * Get phoneType
     *
     * @return int
     */
    public function getPhoneType()
    {
        return $this->phoneType;
    }

    /**
     * Set phoneType
     *
     * @param int $value phoneType
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setPhoneType($value)
    {
        $this->phoneType = $value;
        return $this;
    }

    /**
     * Get order
     *
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param int $value order
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setOrder($value)
    {
        $this->order = $value;
        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set main
     *
     * @param boolean $value main
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setMain($value)
    {
        $this->main = $value;
        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param string $value country
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setCountry($value)
    {
        $this->country = $value;
        return $this;
    }

    /**
     * Get any
     *
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }

    /**
     * Set any
     *
     * @param \SoapVar $value any
     *
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\CommunicationDetailDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
