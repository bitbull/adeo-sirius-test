<?php
/**
 * Parent model for SiriusApplicationFault
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class SiriusApplicationFault
{
    
    /**
     * Message
     * 
     * @var string
     */
    public $message;
    
    /**
     * Get message
     * 
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set message
     * 
     * @param string $value message
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\SiriusApplicationFault
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }
}
