<?php
/**
 * Parent model for HousingDetailDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class HousingDetailDTO
{
    
    /**
     * MovingInDate
     * 
     * @var string
     */
    public $movingInDate;
    
    /**
     * PurchaseDate
     * 
     * @var string
     */
    public $purchaseDate;
    
    /**
     * StillIn
     * 
     * @var boolean
     */
    public $stillIn;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get movingInDate
     * 
     * @return string
     */
    public function getMovingInDate()
    {
        return $this->movingInDate;
    }
    
    /**
     * Set movingInDate
     * 
     * @param string $value movingInDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public function setMovingInDate($value)
    {
        $this->movingInDate = $value;
        return $this;
    }
    
    /**
     * Get purchaseDate
     * 
     * @return string
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }
    
    /**
     * Set purchaseDate
     * 
     * @param string $value purchaseDate
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public function setPurchaseDate($value)
    {
        $this->purchaseDate = $value;
        return $this;
    }
    
    /**
     * Get stillIn
     * 
     * @return boolean
     */
    public function getStillIn()
    {
        return $this->stillIn;
    }
    
    /**
     * Set stillIn
     * 
     * @param boolean $value stillIn
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public function setStillIn($value)
    {
        $this->stillIn = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\HousingDetailDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
