<?php
/**
 * Parent model for AddressExternalCodeDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class AddressExternalCodeDTO
{
    
    /**
     * Code1
     * 
     * @var string
     */
    public $code1;
    
    /**
     * Code2
     * 
     * @var string
     */
    public $code2;
    
    /**
     * StateCode
     * 
     * @var int
     */
    public $stateCode;
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * Get code1
     * 
     * @return string
     */
    public function getCode1()
    {
        return $this->code1;
    }
    
    /**
     * Set code1
     * 
     * @param string $value code1
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO
     */
    public function setCode1($value)
    {
        $this->code1 = $value;
        return $this;
    }
    
    /**
     * Get code2
     * 
     * @return string
     */
    public function getCode2()
    {
        return $this->code2;
    }
    
    /**
     * Set code2
     * 
     * @param string $value code2
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO
     */
    public function setCode2($value)
    {
        $this->code2 = $value;
        return $this;
    }
    
    /**
     * Get stateCode
     * 
     * @return int
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }
    
    /**
     * Set stateCode
     * 
     * @param int $value stateCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO
     */
    public function setStateCode($value)
    {
        $this->stateCode = $value;
        return $this;
    }
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressExternalCodeDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
}
