<?php
/**
 * Parent model for GetHousehold
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class GetHousehold
{
    
    /**
     * CustomerOrHouseholdNumber
     * 
     * @var int
     */
    public $customerOrHouseholdNumber;
    
    /**
     * IsCustomerNumber
     * 
     * @var boolean
     */
    public $isCustomerNumber;
    
    /**
     * Get customerOrHouseholdNumber
     * 
     * @return int
     */
    public function getCustomerOrHouseholdNumber()
    {
        return $this->customerOrHouseholdNumber;
    }
    
    /**
     * Set customerOrHouseholdNumber
     * 
     * @param int $value customerOrHouseholdNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GetHousehold
     */
    public function setCustomerOrHouseholdNumber($value)
    {
        $this->customerOrHouseholdNumber = $value;
        return $this;
    }
    
    /**
     * Get isCustomerNumber
     * 
     * @return boolean
     */
    public function getIsCustomerNumber()
    {
        return $this->isCustomerNumber;
    }
    
    /**
     * Set isCustomerNumber
     * 
     * @param boolean $value isCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\GetHousehold
     */
    public function setIsCustomerNumber($value)
    {
        $this->isCustomerNumber = $value;
        return $this;
    }
}
