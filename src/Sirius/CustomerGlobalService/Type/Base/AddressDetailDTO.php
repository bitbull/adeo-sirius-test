<?php
/**
 * Parent model for AddressDetailDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type\Base;

abstract class AddressDetailDTO
{
    
    /**
     * Id
     * 
     * @var int
     */
    public $id;
    
    /**
     * Line1
     * 
     * @var string
     */
    public $line1;
    
    /**
     * Line2
     * 
     * @var string
     */
    public $line2;
    
    /**
     * Line3
     * 
     * @var string
     */
    public $line3;
    
    /**
     * Line4
     * 
     * @var string
     */
    public $line4;
    
    /**
     * PostalCode
     * 
     * @var string
     */
    public $postalCode;
    
    /**
     * City
     * 
     * @var string
     */
    public $city;
    
    /**
     * Province
     * 
     * @var string
     */
    public $province;
    
    /**
     * Country
     * 
     * @var string
     */
    public $country;
    
    /**
     * MainAddress
     * 
     * @var boolean
     */
    public $mainAddress;
    
    /**
     * ContactEntity
     * 
     * @var int
     */
    public $contactEntity;
    
    /**
     * Comment
     * 
     * @var string
     */
    public $comment;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * 
     * @param int $value id
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }
    
    /**
     * Get line1
     * 
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }
    
    /**
     * Set line1
     * 
     * @param string $value line1
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setLine1($value)
    {
        $this->line1 = $value;
        return $this;
    }
    
    /**
     * Get line2
     * 
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }
    
    /**
     * Set line2
     * 
     * @param string $value line2
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setLine2($value)
    {
        $this->line2 = $value;
        return $this;
    }
    
    /**
     * Get line3
     * 
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }
    
    /**
     * Set line3
     * 
     * @param string $value line3
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setLine3($value)
    {
        $this->line3 = $value;
        return $this;
    }
    
    /**
     * Get line4
     * 
     * @return string
     */
    public function getLine4()
    {
        return $this->line4;
    }
    
    /**
     * Set line4
     * 
     * @param string $value line4
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setLine4($value)
    {
        $this->line4 = $value;
        return $this;
    }
    
    /**
     * Get postalCode
     * 
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * Set postalCode
     * 
     * @param string $value postalCode
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setPostalCode($value)
    {
        $this->postalCode = $value;
        return $this;
    }
    
    /**
     * Get city
     * 
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Set city
     * 
     * @param string $value city
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setCity($value)
    {
        $this->city = $value;
        return $this;
    }
    
    /**
     * Get province
     * 
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    /**
     * Set province
     * 
     * @param string $value province
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setProvince($value)
    {
        $this->province = $value;
        return $this;
    }
    
    /**
     * Get country
     * 
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set country
     * 
     * @param string $value country
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setCountry($value)
    {
        $this->country = $value;
        return $this;
    }
    
    /**
     * Get mainAddress
     * 
     * @return boolean
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }
    
    /**
     * Set mainAddress
     * 
     * @param boolean $value mainAddress
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setMainAddress($value)
    {
        $this->mainAddress = $value;
        return $this;
    }
    
    /**
     * Get contactEntity
     * 
     * @return int
     */
    public function getContactEntity()
    {
        return $this->contactEntity;
    }
    
    /**
     * Set contactEntity
     * 
     * @param int $value contactEntity
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setContactEntity($value)
    {
        $this->contactEntity = $value;
        return $this;
    }
    
    /**
     * Get comment
     * 
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Set comment
     * 
     * @param string $value comment
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setComment($value)
    {
        $this->comment = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGlobalService\Type\Base\AddressDetailDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
