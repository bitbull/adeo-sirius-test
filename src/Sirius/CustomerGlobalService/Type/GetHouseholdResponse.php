<?php
/**
 * Model for GetHouseholdResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGlobalService\Type;

use \Adeo\Sirius\CustomerGlobalService\Type\Base\GetHouseholdResponse as GetHouseholdResponseBase;

class GetHouseholdResponse
    extends getHouseholdResponseBase
{
}
