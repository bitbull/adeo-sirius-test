<?php
/**
 * Parent model for Customer
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class Customer
{
    
    /**
     * CustomerNumber
     * 
     * @var string
     */
    public $customerNumber;
    
    /**
     * Status
     * 
     * @var string
     */
    public $status;
    
    /**
     * StatusLabel
     * 
     * @var string
     */
    public $statusLabel;
    
    /**
     * LastName
     * 
     * @var string
     */
    public $lastName;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * AccessionPosCode
     * 
     * @var string
     */
    public $accessionPosCode;
    
    /**
     * Cards collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card[]
     */
    public $cards = array();
    
    /**
     * ExtendedInformations collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public $extendedInformations = array();
    
    /**
     * Get customerNumber
     * 
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param string $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set status
     * 
     * @param string $value status
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }
    
    /**
     * Get statusLabel
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }
    
    /**
     * Set statusLabel
     * 
     * @param string $value statusLabel
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setStatusLabel($value)
    {
        $this->statusLabel = $value;
        return $this;
    }
    
    /**
     * Get lastName
     * 
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    /**
     * Set lastName
     * 
     * @param string $value lastName
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setLastName($value)
    {
        $this->lastName = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get accessionPosCode
     * 
     * @return string
     */
    public function getAccessionPosCode()
    {
        return $this->accessionPosCode;
    }
    
    /**
     * Set accessionPosCode
     * 
     * @param string $value accessionPosCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setAccessionPosCode($value)
    {
        $this->accessionPosCode = $value;
        return $this;
    }
    
    /**
     * Get cards
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card[]
     */
    public function getCards()
    {
        return $this->cards;
    }
    
    /**
     * Add element on cards collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card[] $value cards
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setCards($value)
    {
        $this->cards[] = $value;
        return $this;
    }
    
    /**
     * Get extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public function getExtendedInformations()
    {
        return $this->extendedInformations;
    }
    
    /**
     * Add element on extendedInformations collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[] $value extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer
     */
    public function setExtendedInformations($value = null)
    {
        $this->extendedInformations[] = $value;
        return $this;
    }
}
