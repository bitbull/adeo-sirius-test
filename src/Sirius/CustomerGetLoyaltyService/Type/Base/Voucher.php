<?php
/**
 * Parent model for Voucher
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class Voucher
{
    
    /**
     * VoucherCode
     * 
     * @var string
     */
    public $voucherCode;
    
    /**
     * VoucherTypeCode
     * 
     * @var string
     */
    public $voucherTypeCode;
    
    /**
     * UsageDuration
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration
     */
    public $usageDuration;
    
    /**
     * BurnValue
     * 
     * @var int
     */
    public $burnValue;
    
    /**
     * ActivationFlag
     * 
     * @var boolean
     */
    public $activationFlag;
    
    /**
     * ActivationDate
     * 
     * @var string
     */
    public $activationDate;
    
    /**
     * ValidityEndDate
     * 
     * @var string
     */
    public $validityEndDate;
    
    /**
     * ExpirationEndDate
     * 
     * @var string
     */
    public $expirationEndDate;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * Amount
     * 
     * @var float
     */
    public $amount;
    
    /**
     * AmountRewardType
     * 
     * @var string
     */
    public $amountRewardType;
    
    /**
     * AssociatedVoucherCode
     * 
     * @var string
     */
    public $associatedVoucherCode;
    
    /**
     * Get voucherCode
     * 
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }
    
    /**
     * Set voucherCode
     * 
     * @param string $value voucherCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setVoucherCode($value)
    {
        $this->voucherCode = $value;
        return $this;
    }
    
    /**
     * Get voucherTypeCode
     * 
     * @return string
     */
    public function getVoucherTypeCode()
    {
        return $this->voucherTypeCode;
    }
    
    /**
     * Set voucherTypeCode
     * 
     * @param string $value voucherTypeCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setVoucherTypeCode($value)
    {
        $this->voucherTypeCode = $value;
        return $this;
    }
    
    /**
     * Get usageDuration
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration
     */
    public function getUsageDuration()
    {
        return $this->usageDuration;
    }
    
    /**
     * Set usageDuration
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration $value usageDuration
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setUsageDuration(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration $value)
    {
        $this->usageDuration = $value;
        return $this;
    }
    
    /**
     * Get burnValue
     * 
     * @return int
     */
    public function getBurnValue()
    {
        return $this->burnValue;
    }
    
    /**
     * Set burnValue
     * 
     * @param int $value burnValue
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setBurnValue($value)
    {
        $this->burnValue = $value;
        return $this;
    }
    
    /**
     * Get activationFlag
     * 
     * @return boolean
     */
    public function getActivationFlag()
    {
        return $this->activationFlag;
    }
    
    /**
     * Set activationFlag
     * 
     * @param boolean $value activationFlag
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setActivationFlag($value)
    {
        $this->activationFlag = $value;
        return $this;
    }
    
    /**
     * Get activationDate
     * 
     * @return string
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }
    
    /**
     * Set activationDate
     * 
     * @param string $value activationDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setActivationDate($value)
    {
        $this->activationDate = $value;
        return $this;
    }
    
    /**
     * Get validityEndDate
     * 
     * @return string
     */
    public function getValidityEndDate()
    {
        return $this->validityEndDate;
    }
    
    /**
     * Set validityEndDate
     * 
     * @param string $value validityEndDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setValidityEndDate($value)
    {
        $this->validityEndDate = $value;
        return $this;
    }
    
    /**
     * Get expirationEndDate
     * 
     * @return string
     */
    public function getExpirationEndDate()
    {
        return $this->expirationEndDate;
    }
    
    /**
     * Set expirationEndDate
     * 
     * @param string $value expirationEndDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setExpirationEndDate($value)
    {
        $this->expirationEndDate = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get amount
     * 
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set amount
     * 
     * @param float $value amount
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setAmount($value)
    {
        $this->amount = $value;
        return $this;
    }
    
    /**
     * Get amountRewardType
     * 
     * @return string
     */
    public function getAmountRewardType()
    {
        return $this->amountRewardType;
    }
    
    /**
     * Set amountRewardType
     * 
     * @param string $value amountRewardType
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setAmountRewardType($value)
    {
        $this->amountRewardType = $value;
        return $this;
    }
    
    /**
     * Get associatedVoucherCode
     * 
     * @return string
     */
    public function getAssociatedVoucherCode()
    {
        return $this->associatedVoucherCode;
    }
    
    /**
     * Set associatedVoucherCode
     * 
     * @param string $value associatedVoucherCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher
     */
    public function setAssociatedVoucherCode($value)
    {
        $this->associatedVoucherCode = $value;
        return $this;
    }
}
