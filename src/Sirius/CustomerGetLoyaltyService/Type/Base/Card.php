<?php
/**
 * Parent model for Card
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class Card
{
    
    /**
     * CardNumber
     * 
     * @var string
     */
    public $cardNumber;
    
    /**
     * Status
     * 
     * @var string
     */
    public $status;
    
    /**
     * StatusLabel
     * 
     * @var string
     */
    public $statusLabel;
    
    /**
     * ValidityEndDate
     * 
     * @var string
     */
    public $validityEndDate;
    
    /**
     * ExtendedInformations collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public $extendedInformations = array();
    
    /**
     * Get cardNumber
     * 
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }
    
    /**
     * Set cardNumber
     * 
     * @param string $value cardNumber
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card
     */
    public function setCardNumber($value)
    {
        $this->cardNumber = $value;
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set status
     * 
     * @param string $value status
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }
    
    /**
     * Get statusLabel
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }
    
    /**
     * Set statusLabel
     * 
     * @param string $value statusLabel
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card
     */
    public function setStatusLabel($value)
    {
        $this->statusLabel = $value;
        return $this;
    }
    
    /**
     * Get validityEndDate
     * 
     * @return string
     */
    public function getValidityEndDate()
    {
        return $this->validityEndDate;
    }
    
    /**
     * Set validityEndDate
     * 
     * @param string $value validityEndDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card
     */
    public function setValidityEndDate($value)
    {
        $this->validityEndDate = $value;
        return $this;
    }
    
    /**
     * Get extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public function getExtendedInformations()
    {
        return $this->extendedInformations;
    }
    
    /**
     * Add element on extendedInformations collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[] $value extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Card
     */
    public function setExtendedInformations($value = null)
    {
        $this->extendedInformations[] = $value;
        return $this;
    }
}
