<?php
/**
 * Parent model for GetLoyaltyCustomerRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class GetLoyaltyCustomerRequest
{
    
    /**
     * CustomerIdentification
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification
     */
    public $customerIdentification;
    
    /**
     * Construct GetLoyaltyCustomerRequest
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification $customerIdentification Value of customerIdentification
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification $customerIdentification = null)
    {
        $this->customerIdentification = $customerIdentification;
    }
    
    /**
     * Get customerIdentification
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification
     */
    public function getCustomerIdentification()
    {
        return $this->customerIdentification;
    }
    
    /**
     * Set customerIdentification
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification $value customerIdentification
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\GetLoyaltyCustomerRequest
     */
    public function setCustomerIdentification(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification $value)
    {
        $this->customerIdentification = $value;
        return $this;
    }
}
