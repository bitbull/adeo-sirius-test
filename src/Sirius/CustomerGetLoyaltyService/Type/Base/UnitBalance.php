<?php
/**
 * Parent model for UnitBalance
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class UnitBalance
{
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Value
     * 
     * @var float
     */
    public $value;
    
    /**
     * Any collection
     * 
     * @var \SoapVar
     */
    public $any = array();
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param float $value value
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Add element on any collection
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance
     */
    public function setAny(\SoapVar $value)
    {
        $this->any[] = $value;
        return $this;
    }
}
