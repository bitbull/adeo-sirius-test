<?php
/**
 * Parent model for WLLResponseHeader
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class WLLResponseHeader
{
    
    /**
     * ReturnCode
     * 
     * @var string
     */
    public $returnCode;
    
    /**
     * ErrorCode
     * 
     * @var string
     */
    public $errorCode = null;
    
    /**
     * ErrorLabel
     * 
     * @var string
     */
    public $errorLabel = null;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Construct WLLResponseHeader
     * 
     * @param string $version Value of version
     * 
     * @return void
     */
    public function __construct( $version = null)
    {
        $this->version = $version;
    }
    
    /**
     * Get returnCode
     * 
     * @return string
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }
    
    /**
     * Set returnCode
     * 
     * @param string $value returnCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public function setReturnCode($value)
    {
        $this->returnCode = $value;
        return $this;
    }
    
    /**
     * Get errorCode
     * 
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    
    /**
     * Set errorCode
     * 
     * @param string $value errorCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public function setErrorCode( $value = null)
    {
        $this->errorCode = $value;
        return $this;
    }
    
    /**
     * Get errorLabel
     * 
     * @return string
     */
    public function getErrorLabel()
    {
        return $this->errorLabel;
    }
    
    /**
     * Set errorLabel
     * 
     * @param string $value errorLabel
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public function setErrorLabel( $value = null)
    {
        $this->errorLabel = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public function setVersion( $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
