<?php
/**
 * Parent model for GetLoyaltyCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class GetLoyaltyCustomerResponse
{
    
    /**
     * Header
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public $header;
    
    /**
     * Account
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public $account = null;
    
    /**
     * Construct GetLoyaltyCustomerResponse
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account $account Value of account
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account $account = null)
    {
        $this->account = $account;
    }
    
    /**
     * Get header
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader
     */
    public function getHeader()
    {
        return $this->header;
    }
    
    /**
     * Set header
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader $value header
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\GetLoyaltyCustomerResponse
     */
    public function setHeader(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\WLLResponseHeader $value)
    {
        $this->header = $value;
        return $this;
    }
    
    /**
     * Get account
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function getAccount()
    {
        return $this->account;
    }
    
    /**
     * Set account
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account $value account
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\GetLoyaltyCustomerResponse
     */
    public function setAccount(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account $value = null)
    {
        $this->account = $value;
        return $this;
    }
}
