<?php
/**
 * Parent model for ExtendedInformation
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class ExtendedInformation
{
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Date
     * 
     * @var string
     */
    public $date;
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get date
     * 
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set date
     * 
     * @param string $value date
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation
     */
    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }
}
