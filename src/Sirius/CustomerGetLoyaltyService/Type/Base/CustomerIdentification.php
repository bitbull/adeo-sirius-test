<?php
/**
 * Parent model for CustomerIdentification
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class CustomerIdentification
{
    
    /**
     * CustomerNumber
     * 
     * @var string
     */
    public $customerNumber;
    
    /**
     * Context
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context
     */
    public $context;
    
    /**
     * Construct CustomerIdentification
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context $context Value of context
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context $context = null)
    {
        $this->context = $context;
    }
    
    /**
     * Get customerNumber
     * 
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param string $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get context
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context
     */
    public function getContext()
    {
        return $this->context;
    }
    
    /**
     * Set context
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context $value context
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\CustomerIdentification
     */
    public function setContext(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context $value)
    {
        $this->context = $value;
        return $this;
    }
}
