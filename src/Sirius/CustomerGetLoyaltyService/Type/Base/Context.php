<?php
/**
 * Parent model for Context
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class Context
{
    
    /**
     * ApplicationCode
     * 
     * @var string
     */
    public $applicationCode;
    
    /**
     * OperatorIdentifier
     * 
     * @var string
     */
    public $operatorIdentifier;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Construct Context
     * 
     * @param string $language Value of language
     * 
     * @return void
     */
    public function __construct( $language = null)
    {
        $this->language = $language;
    }
    
    /**
     * Get applicationCode
     * 
     * @return string
     */
    public function getApplicationCode()
    {
        return $this->applicationCode;
    }
    
    /**
     * Set applicationCode
     * 
     * @param string $value applicationCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context
     */
    public function setApplicationCode($value)
    {
        $this->applicationCode = $value;
        return $this;
    }
    
    /**
     * Get operatorIdentifier
     * 
     * @return string
     */
    public function getOperatorIdentifier()
    {
        return $this->operatorIdentifier;
    }
    
    /**
     * Set operatorIdentifier
     * 
     * @param string $value operatorIdentifier
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context
     */
    public function setOperatorIdentifier($value)
    {
        $this->operatorIdentifier = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Context
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
}
