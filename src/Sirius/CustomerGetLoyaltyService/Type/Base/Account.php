<?php
/**
 * Parent model for Account
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type\Base;

abstract class Account
{
    
    /**
     * AccountNumber
     * 
     * @var string
     */
    public $accountNumber;
    
    /**
     * Status
     * 
     * @var string
     */
    public $status;
    
    /**
     * StatusLabel
     * 
     * @var string
     */
    public $statusLabel;
    
    /**
     * MembershipType
     * 
     * @var string
     */
    public $membershipType;
    
    /**
     * MembershipLabel
     * 
     * @var string
     */
    public $membershipLabel;
    
    /**
     * MemberShipDuration
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration
     */
    public $memberShipDuration;
    
    /**
     * AffiliationPosCode
     * 
     * @var string
     */
    public $affiliationPosCode;
    
    /**
     * LastAccessionDate
     * 
     * @var string
     */
    public $lastAccessionDate;
    
    /**
     * FirstAccessionDate
     * 
     * @var string
     */
    public $firstAccessionDate;
    
    /**
     * AccountManagerCustomerNumber
     * 
     * @var string
     */
    public $accountManagerCustomerNumber;
    
    /**
     * ValidityEndDate
     * 
     * @var string
     */
    public $validityEndDate;
    
    /**
     * Customers collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer[]
     */
    public $customers = array();
    
    /**
     * Vouchers collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher[]
     */
    public $vouchers = array();
    
    /**
     * UnitsBalance collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance[]
     */
    public $unitsBalance = array();
    
    /**
     * ExtendedInformations collection
     * 
     * @var \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public $extendedInformations = array();
    
    /**
     * Get accountNumber
     * 
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }
    
    /**
     * Set accountNumber
     * 
     * @param string $value accountNumber
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setAccountNumber($value)
    {
        $this->accountNumber = $value;
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set status
     * 
     * @param string $value status
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }
    
    /**
     * Get statusLabel
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }
    
    /**
     * Set statusLabel
     * 
     * @param string $value statusLabel
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setStatusLabel($value)
    {
        $this->statusLabel = $value;
        return $this;
    }
    
    /**
     * Get membershipType
     * 
     * @return string
     */
    public function getMembershipType()
    {
        return $this->membershipType;
    }
    
    /**
     * Set membershipType
     * 
     * @param string $value membershipType
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setMembershipType($value)
    {
        $this->membershipType = $value;
        return $this;
    }
    
    /**
     * Get membershipLabel
     * 
     * @return string
     */
    public function getMembershipLabel()
    {
        return $this->membershipLabel;
    }
    
    /**
     * Set membershipLabel
     * 
     * @param string $value membershipLabel
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setMembershipLabel($value)
    {
        $this->membershipLabel = $value;
        return $this;
    }
    
    /**
     * Get memberShipDuration
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration
     */
    public function getMemberShipDuration()
    {
        return $this->memberShipDuration;
    }
    
    /**
     * Set memberShipDuration
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration $value memberShipDuration
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setMemberShipDuration(\Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Duration $value)
    {
        $this->memberShipDuration = $value;
        return $this;
    }
    
    /**
     * Get affiliationPosCode
     * 
     * @return string
     */
    public function getAffiliationPosCode()
    {
        return $this->affiliationPosCode;
    }
    
    /**
     * Set affiliationPosCode
     * 
     * @param string $value affiliationPosCode
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setAffiliationPosCode($value)
    {
        $this->affiliationPosCode = $value;
        return $this;
    }
    
    /**
     * Get lastAccessionDate
     * 
     * @return string
     */
    public function getLastAccessionDate()
    {
        return $this->lastAccessionDate;
    }
    
    /**
     * Set lastAccessionDate
     * 
     * @param string $value lastAccessionDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setLastAccessionDate($value)
    {
        $this->lastAccessionDate = $value;
        return $this;
    }
    
    /**
     * Get firstAccessionDate
     * 
     * @return string
     */
    public function getFirstAccessionDate()
    {
        return $this->firstAccessionDate;
    }
    
    /**
     * Set firstAccessionDate
     * 
     * @param string $value firstAccessionDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setFirstAccessionDate($value)
    {
        $this->firstAccessionDate = $value;
        return $this;
    }
    
    /**
     * Get accountManagerCustomerNumber
     * 
     * @return string
     */
    public function getAccountManagerCustomerNumber()
    {
        return $this->accountManagerCustomerNumber;
    }
    
    /**
     * Set accountManagerCustomerNumber
     * 
     * @param string $value accountManagerCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setAccountManagerCustomerNumber($value)
    {
        $this->accountManagerCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get validityEndDate
     * 
     * @return string
     */
    public function getValidityEndDate()
    {
        return $this->validityEndDate;
    }
    
    /**
     * Set validityEndDate
     * 
     * @param string $value validityEndDate
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setValidityEndDate($value)
    {
        $this->validityEndDate = $value;
        return $this;
    }
    
    /**
     * Get customers
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }
    
    /**
     * Add element on customers collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer[] $value customers
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setCustomers($value)
    {
        $this->customers[] = $value;
        return $this;
    }
    
    /**
     * Get vouchers
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher[]
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }
    
    /**
     * Add element on vouchers collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Voucher[] $value vouchers
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setVouchers($value)
    {
        $this->vouchers[] = $value;
        return $this;
    }
    
    /**
     * Get unitsBalance
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance[]
     */
    public function getUnitsBalance()
    {
        return $this->unitsBalance;
    }
    
    /**
     * Add element on unitsBalance collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\UnitBalance[] $value unitsBalance
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setUnitsBalance($value)
    {
        $this->unitsBalance[] = $value;
        return $this;
    }
    
    /**
     * Get extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[]
     */
    public function getExtendedInformations()
    {
        return $this->extendedInformations;
    }
    
    /**
     * Add element on extendedInformations collection
     * 
     * @param \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\ExtendedInformation[] $value extendedInformations
     * 
     * @return \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Account
     */
    public function setExtendedInformations($value = null)
    {
        $this->extendedInformations[] = $value;
        return $this;
    }
}
