<?php
/**
 * Model for GetLoyaltyCustomerRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type;

use \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\GetLoyaltyCustomerRequest as GetLoyaltyCustomerRequestBase;

class GetLoyaltyCustomerRequest
    extends getLoyaltyCustomerRequestBase
{
}
