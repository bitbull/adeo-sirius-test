<?php
/**
 * Model for Customer
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type;

use \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\Customer as CustomerBase;

class Customer
    extends CustomerBase
{
}
