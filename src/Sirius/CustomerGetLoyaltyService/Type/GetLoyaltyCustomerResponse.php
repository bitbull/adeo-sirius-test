<?php
/**
 * Model for GetLoyaltyCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerGetLoyaltyService\Type;

use \Adeo\Sirius\CustomerGetLoyaltyService\Type\Base\GetLoyaltyCustomerResponse as GetLoyaltyCustomerResponseBase;

class GetLoyaltyCustomerResponse
    extends getLoyaltyCustomerResponseBase
{
}
