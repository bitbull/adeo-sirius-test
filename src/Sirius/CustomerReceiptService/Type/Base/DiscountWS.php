<?php
/**
 * Parent model for DiscountWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class DiscountWS
{
    
    /**
     * DiscountDesc
     * 
     * @var string
     */
    public $discountDesc = null;
    
    /**
     * Rank
     * 
     * @var int
     */
    public $rank;
    
    /**
     * TypeDiscount
     * 
     * @var int
     */
    public $typeDiscount;
    
    /**
     * Value
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public $value = null;
    
    /**
     * Get discountDesc
     * 
     * @return string
     */
    public function getDiscountDesc()
    {
        return $this->discountDesc;
    }
    
    /**
     * Set discountDesc
     * 
     * @param string $value discountDesc
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS
     */
    public function setDiscountDesc(string $value = null)
    {
        $this->discountDesc = $value;
        return $this;
    }
    
    /**
     * Get rank
     * 
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }
    
    /**
     * Set rank
     * 
     * @param int $value rank
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS
     */
    public function setRank($value)
    {
        $this->rank = $value;
        return $this;
    }
    
    /**
     * Get typeDiscount
     * 
     * @return int
     */
    public function getTypeDiscount()
    {
        return $this->typeDiscount;
    }
    
    /**
     * Set typeDiscount
     * 
     * @param int $value typeDiscount
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS
     */
    public function setTypeDiscount($value)
    {
        $this->typeDiscount = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value value
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS
     */
    public function setValue(\Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value = null)
    {
        $this->value = $value;
        return $this;
    }
}
