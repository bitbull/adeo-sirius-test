<?php
/**
 * Parent model for GetReceiptRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceiptRequest
{
    
    /**
     * ReceiptId
     * 
     * @var int
     */
    public $receiptId;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get receiptId
     * 
     * @return int
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }
    
    /**
     * Set receiptId
     * 
     * @param int $value receiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest
     */
    public function setReceiptId($value)
    {
        $this->receiptId = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
