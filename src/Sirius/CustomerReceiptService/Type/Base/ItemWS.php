<?php
/**
 * Parent model for ItemWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ItemWS
{
    
    /**
     * DepartmentID
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS
     */
    public $departmentID = null;
    
    /**
     * Description
     * 
     * @var string
     */
    public $description = null;
    
    /**
     * Reference
     * 
     * @var int
     */
    public $reference;
    
    /**
     * Get departmentID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS
     */
    public function getDepartmentID()
    {
        return $this->departmentID;
    }
    
    /**
     * Set departmentID
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS $value departmentID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS
     */
    public function setDepartmentID(\Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS $value = null)
    {
        $this->departmentID = $value;
        return $this;
    }
    
    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description
     * 
     * @param string $value description
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS
     */
    public function setDescription(string $value = null)
    {
        $this->description = $value;
        return $this;
    }
    
    /**
     * Get reference
     * 
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }
    
    /**
     * Set reference
     * 
     * @param int $value reference
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS
     */
    public function setReference($value)
    {
        $this->reference = $value;
        return $this;
    }
}
