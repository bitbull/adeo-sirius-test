<?php
/**
 * Parent model for GetReceiptByNumResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceiptByNumResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse
     */
    public $out = null;
    
    /**
     * Construct GetReceiptByNumResponse
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse $value out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumResponse
     */
    public function setOut(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptResponse $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
