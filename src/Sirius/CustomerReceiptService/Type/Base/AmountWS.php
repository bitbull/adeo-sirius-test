<?php
/**
 * Parent model for AmountWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class AmountWS
{
    
    /**
     * Amount
     * 
     * @var float
     */
    public $amount = null;
    
    /**
     * CodCur
     * 
     * @var string
     */
    public $codCur = null;
    
    /**
     * Get amount
     * 
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set amount
     * 
     * @param float $value amount
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function setAmount($value = null)
    {
        $this->amount = $value;
        return $this;
    }
    
    /**
     * Get codCur
     * 
     * @return string
     */
    public function getCodCur()
    {
        return $this->codCur;
    }
    
    /**
     * Set codCur
     * 
     * @param string $value codCur
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function setCodCur(string $value = null)
    {
        $this->codCur = $value;
        return $this;
    }
}
