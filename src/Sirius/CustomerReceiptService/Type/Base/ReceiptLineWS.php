<?php
/**
 * Parent model for ReceiptLineWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ReceiptLineWS
{
    
    /**
     * AmTTC
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public $amTTC = null;
    
    /**
     * CodeC1
     * 
     * @var int
     */
    public $codeC1 = null;
    
    /**
     * DateUpdate
     * 
     * @var string
     */
    public $dateUpdate = null;
    
    /**
     * Discountlst
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfDiscountWS
     */
    public $discountlst = null;
    
    /**
     * InitialSellReceiptId
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public $initialSellReceiptId = null;
    
    /**
     * ItemCde
     * 
     * @var int
     */
    public $itemCde;
    
    /**
     * ItemID
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS
     */
    public $itemID = null;
    
    /**
     * ItemPrice
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public $itemPrice = null;
    
    /**
     * ItemQty
     * 
     * @var float
     */
    public $itemQty = null;
    
    /**
     * LineNumber
     * 
     * @var int
     */
    public $lineNumber;
    
    /**
     * ReceiptId
     * 
     * @var int
     */
    public $receiptId;
    
    /**
     * TransactionID
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS
     */
    public $transactionID = null;
    
    /**
     * VatRate
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS
     */
    public $vatRate = null;
    
    /**
     * Get amTTC
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function getAmTTC()
    {
        return $this->amTTC;
    }
    
    /**
     * Set amTTC
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value amTTC
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setAmTTC(\Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value = null)
    {
        $this->amTTC = $value;
        return $this;
    }
    
    /**
     * Get codeC1
     * 
     * @return int
     */
    public function getCodeC1()
    {
        return $this->codeC1;
    }
    
    /**
     * Set codeC1
     * 
     * @param int $value codeC1
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setCodeC1($value = null)
    {
        $this->codeC1 = $value;
        return $this;
    }
    
    /**
     * Get dateUpdate
     * 
     * @return string
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
    
    /**
     * Set dateUpdate
     * 
     * @param string $value dateUpdate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setDateUpdate(string $value = null)
    {
        $this->dateUpdate = $value;
        return $this;
    }
    
    /**
     * Get discountlst
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfDiscountWS
     */
    public function getDiscountlst()
    {
        return $this->discountlst;
    }
    
    /**
     * Set discountlst
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfDiscountWS $value discountlst
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setDiscountlst(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfDiscountWS $value = null)
    {
        $this->discountlst = $value;
        return $this;
    }
    
    /**
     * Get initialSellReceiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function getInitialSellReceiptId()
    {
        return $this->initialSellReceiptId;
    }
    
    /**
     * Set initialSellReceiptId
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS $value initialSellReceiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setInitialSellReceiptId(\Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS $value = null)
    {
        $this->initialSellReceiptId = $value;
        return $this;
    }
    
    /**
     * Get itemCde
     * 
     * @return int
     */
    public function getItemCde()
    {
        return $this->itemCde;
    }
    
    /**
     * Set itemCde
     * 
     * @param int $value itemCde
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setItemCde($value)
    {
        $this->itemCde = $value;
        return $this;
    }
    
    /**
     * Get itemID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS
     */
    public function getItemID()
    {
        return $this->itemID;
    }
    
    /**
     * Set itemID
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS $value itemID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setItemID(\Adeo\Sirius\CustomerReceiptService\Type\Base\ItemWS $value = null)
    {
        $this->itemID = $value;
        return $this;
    }
    
    /**
     * Get itemPrice
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }
    
    /**
     * Set itemPrice
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value itemPrice
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setItemPrice(\Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value = null)
    {
        $this->itemPrice = $value;
        return $this;
    }
    
    /**
     * Get itemQty
     * 
     * @return float
     */
    public function getItemQty()
    {
        return $this->itemQty;
    }
    
    /**
     * Set itemQty
     * 
     * @param float $value itemQty
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setItemQty($value = null)
    {
        $this->itemQty = $value;
        return $this;
    }
    
    /**
     * Get lineNumber
     * 
     * @return int
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }
    
    /**
     * Set lineNumber
     * 
     * @param int $value lineNumber
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setLineNumber($value)
    {
        $this->lineNumber = $value;
        return $this;
    }
    
    /**
     * Get receiptId
     * 
     * @return int
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }
    
    /**
     * Set receiptId
     * 
     * @param int $value receiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setReceiptId($value)
    {
        $this->receiptId = $value;
        return $this;
    }
    
    /**
     * Get transactionID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS
     */
    public function getTransactionID()
    {
        return $this->transactionID;
    }
    
    /**
     * Set transactionID
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS $value transactionID
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setTransactionID(\Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS $value = null)
    {
        $this->transactionID = $value;
        return $this;
    }
    
    /**
     * Get vatRate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }
    
    /**
     * Set vatRate
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS $value vatRate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS
     */
    public function setVatRate(\Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS $value = null)
    {
        $this->vatRate = $value;
        return $this;
    }
}
