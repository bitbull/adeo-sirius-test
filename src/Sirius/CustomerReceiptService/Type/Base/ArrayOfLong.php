<?php
/**
 * Parent model for ArrayOfLong
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfLong
{
    
    /**
     * Long collection
     * 
     * @var int[]
     */
    public $long = array();
    
    /**
     * Get long
     * 
     * @return int[]
     */
    public function getLong()
    {
        return $this->long;
    }
    
    /**
     * Add element on long collection
     * 
     * @param int[] $value long
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public function setLong($value = null)
    {
        $this->long[] = $value;
        return $this;
    }
}
