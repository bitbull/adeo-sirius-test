<?php
/**
 * Parent model for SearchReceiptByMagRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptByMagRequest
{
    
    /**
     * IdxEnd
     * 
     * @var int
     */
    public $idxEnd;
    
    /**
     * IdxStart
     * 
     * @var int
     */
    public $idxStart;
    
    /**
     * MaxDate
     * 
     * @var string
     */
    public $maxDate = null;
    
    /**
     * MinDate
     * 
     * @var string
     */
    public $minDate = null;
    
    /**
     * NumMag
     * 
     * @var string
     */
    public $numMag = null;
    
    /**
     * NumReceipt
     * 
     * @var int
     */
    public $numReceipt;
    
    /**
     * RefItem
     * 
     * @var int
     */
    public $refItem;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get idxEnd
     * 
     * @return int
     */
    public function getIdxEnd()
    {
        return $this->idxEnd;
    }
    
    /**
     * Set idxEnd
     * 
     * @param int $value idxEnd
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setIdxEnd($value)
    {
        $this->idxEnd = $value;
        return $this;
    }
    
    /**
     * Get idxStart
     * 
     * @return int
     */
    public function getIdxStart()
    {
        return $this->idxStart;
    }
    
    /**
     * Set idxStart
     * 
     * @param int $value idxStart
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setIdxStart($value)
    {
        $this->idxStart = $value;
        return $this;
    }
    
    /**
     * Get maxDate
     * 
     * @return string
     */
    public function getMaxDate()
    {
        return $this->maxDate;
    }
    
    /**
     * Set maxDate
     * 
     * @param string $value maxDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setMaxDate(string $value = null)
    {
        $this->maxDate = $value;
        return $this;
    }
    
    /**
     * Get minDate
     * 
     * @return string
     */
    public function getMinDate()
    {
        return $this->minDate;
    }
    
    /**
     * Set minDate
     * 
     * @param string $value minDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setMinDate(string $value = null)
    {
        $this->minDate = $value;
        return $this;
    }
    
    /**
     * Get numMag
     * 
     * @return string
     */
    public function getNumMag()
    {
        return $this->numMag;
    }
    
    /**
     * Set numMag
     * 
     * @param string $value numMag
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setNumMag(string $value = null)
    {
        $this->numMag = $value;
        return $this;
    }
    
    /**
     * Get numReceipt
     * 
     * @return int
     */
    public function getNumReceipt()
    {
        return $this->numReceipt;
    }
    
    /**
     * Set numReceipt
     * 
     * @param int $value numReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setNumReceipt($value)
    {
        $this->numReceipt = $value;
        return $this;
    }
    
    /**
     * Get refItem
     * 
     * @return int
     */
    public function getRefItem()
    {
        return $this->refItem;
    }
    
    /**
     * Set refItem
     * 
     * @param int $value refItem
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setRefItem($value)
    {
        $this->refItem = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
