<?php
/**
 * Parent model for SearchReceiptByMag
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptByMag
{
    
    /**
     * In0
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public $in0 = null;
    
    /**
     * Construct SearchReceiptByMag
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest $in0 Value of in0
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest $in0 = null)
    {
        $this->in0 = $in0;
    }
    
    /**
     * Get in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest $value in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMag
     */
    public function setIn0(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagRequest $value = null)
    {
        $this->in0 = $value;
        return $this;
    }
}
