<?php
/**
 * Parent model for SearchDetailedReceiptHeaders
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchDetailedReceiptHeaders
{
    
    /**
     * In0
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public $in0 = null;
    
    /**
     * Construct SearchDetailedReceiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest $in0 Value of in0
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest $in0 = null)
    {
        $this->in0 = $in0;
    }
    
    /**
     * Get in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest $value in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaders
     */
    public function setIn0(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest $value = null)
    {
        $this->in0 = $value;
        return $this;
    }
}
