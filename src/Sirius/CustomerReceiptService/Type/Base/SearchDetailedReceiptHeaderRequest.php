<?php
/**
 * Parent model for SearchDetailedReceiptHeaderRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchDetailedReceiptHeaderRequest
{
    
    /**
     * IdxEnd
     * 
     * @var int
     */
    public $idxEnd;
    
    /**
     * IdxStart
     * 
     * @var int
     */
    public $idxStart;
    
    /**
     * MaxDate
     * 
     * @var string
     */
    public $maxDate = null;
    
    /**
     * MinDate
     * 
     * @var string
     */
    public $minDate = null;
    
    /**
     * NumRGRPs
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfString
     */
    public $numRGRPs;
    
    /**
     * RefCdes
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public $refCdes = null;
    
    /**
     * RefItems
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public $refItems = null;
    
    /**
     * SortCriteria
     * 
     * @var int
     */
    public $sortCriteria;
    
    /**
     * StoreNumber
     * 
     * @var string
     */
    public $storeNumber = null;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get idxEnd
     * 
     * @return int
     */
    public function getIdxEnd()
    {
        return $this->idxEnd;
    }
    
    /**
     * Set idxEnd
     * 
     * @param int $value idxEnd
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setIdxEnd($value)
    {
        $this->idxEnd = $value;
        return $this;
    }
    
    /**
     * Get idxStart
     * 
     * @return int
     */
    public function getIdxStart()
    {
        return $this->idxStart;
    }
    
    /**
     * Set idxStart
     * 
     * @param int $value idxStart
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setIdxStart($value)
    {
        $this->idxStart = $value;
        return $this;
    }
    
    /**
     * Get maxDate
     * 
     * @return string
     */
    public function getMaxDate()
    {
        return $this->maxDate;
    }
    
    /**
     * Set maxDate
     * 
     * @param string $value maxDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setMaxDate(string $value = null)
    {
        $this->maxDate = $value;
        return $this;
    }
    
    /**
     * Get minDate
     * 
     * @return string
     */
    public function getMinDate()
    {
        return $this->minDate;
    }
    
    /**
     * Set minDate
     * 
     * @param string $value minDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setMinDate(string $value = null)
    {
        $this->minDate = $value;
        return $this;
    }
    
    /**
     * Get numRGRPs
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfString
     */
    public function getNumRGRPs()
    {
        return $this->numRGRPs;
    }
    
    /**
     * Set numRGRPs
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfString $value numRGRPs
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setNumRGRPs(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfString $value)
    {
        $this->numRGRPs = $value;
        return $this;
    }
    
    /**
     * Get refCdes
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public function getRefCdes()
    {
        return $this->refCdes;
    }
    
    /**
     * Set refCdes
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value refCdes
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setRefCdes(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value = null)
    {
        $this->refCdes = $value;
        return $this;
    }
    
    /**
     * Get refItems
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public function getRefItems()
    {
        return $this->refItems;
    }
    
    /**
     * Set refItems
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value refItems
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setRefItems(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value = null)
    {
        $this->refItems = $value;
        return $this;
    }
    
    /**
     * Get sortCriteria
     * 
     * @return int
     */
    public function getSortCriteria()
    {
        return $this->sortCriteria;
    }
    
    /**
     * Set sortCriteria
     * 
     * @param int $value sortCriteria
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setSortCriteria($value)
    {
        $this->sortCriteria = $value;
        return $this;
    }
    
    /**
     * Get storeNumber
     * 
     * @return string
     */
    public function getStoreNumber()
    {
        return $this->storeNumber;
    }
    
    /**
     * Set storeNumber
     * 
     * @param string $value storeNumber
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setStoreNumber(string $value = null)
    {
        $this->storeNumber = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchDetailedReceiptHeaderRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
