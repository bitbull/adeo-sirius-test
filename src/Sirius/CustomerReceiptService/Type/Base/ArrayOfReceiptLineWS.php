<?php
/**
 * Parent model for ArrayOfReceiptLineWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfReceiptLineWS
{
    
    /**
     * ReceiptLineWS collection
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS[]
     */
    public $ReceiptLineWS = array();
    
    /**
     * Get ReceiptLineWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS[]
     */
    public function getReceiptLineWS()
    {
        return $this->ReceiptLineWS;
    }
    
    /**
     * Add element on ReceiptLineWS collection
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptLineWS[] $value ReceiptLineWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS
     */
    public function setReceiptLineWS($value = null)
    {
        $this->ReceiptLineWS[] = $value;
        return $this;
    }
}
