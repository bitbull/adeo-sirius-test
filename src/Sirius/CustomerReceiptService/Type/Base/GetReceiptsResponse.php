<?php
/**
 * Parent model for GetReceiptsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceiptsResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse
     */
    public $out = null;
    
    /**
     * Construct GetReceiptsResponse
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse $value out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse
     */
    public function setOut(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsResponse $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
