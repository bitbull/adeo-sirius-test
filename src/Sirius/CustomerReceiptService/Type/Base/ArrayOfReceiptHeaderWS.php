<?php
/**
 * Parent model for ArrayOfReceiptHeaderWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfReceiptHeaderWS
{
    
    /**
     * ReceiptHeaderWS collection
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS[]
     */
    public $ReceiptHeaderWS = array();
    
    /**
     * Get ReceiptHeaderWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS[]
     */
    public function getReceiptHeaderWS()
    {
        return $this->ReceiptHeaderWS;
    }
    
    /**
     * Add element on ReceiptHeaderWS collection
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS[] $value ReceiptHeaderWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptHeaderWS
     */
    public function setReceiptHeaderWS($value = null)
    {
        $this->ReceiptHeaderWS[] = $value;
        return $this;
    }
}
