<?php
/**
 * Parent model for ArrayOfDiscountWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfDiscountWS
{
    
    /**
     * DiscountWS collection
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS[]
     */
    public $DiscountWS = array();
    
    /**
     * Get DiscountWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS[]
     */
    public function getDiscountWS()
    {
        return $this->DiscountWS;
    }
    
    /**
     * Add element on DiscountWS collection
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\DiscountWS[] $value DiscountWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfDiscountWS
     */
    public function setDiscountWS($value = null)
    {
        $this->DiscountWS[] = $value;
        return $this;
    }
}
