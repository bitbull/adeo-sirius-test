<?php
/**
 * Parent model for ReceiptHeaderWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ReceiptHeaderWS
{
    
    /**
     * AmReceipt
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public $amReceipt = null;
    
    /**
     * BuyingChannelDesc
     * 
     * @var string
     */
    public $buyingChannelDesc = null;
    
    /**
     * CheckoutNum
     * 
     * @var int
     */
    public $checkoutNum;
    
    /**
     * CustomerRgrp
     * 
     * @var string
     */
    public $customerRgrp = null;
    
    /**
     * DateReceipt
     * 
     * @var string
     */
    public $dateReceipt = null;
    
    /**
     * DateUpdate
     * 
     * @var string
     */
    public $dateUpdate = null;
    
    /**
     * EntityId
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public $entityId = null;
    
    /**
     * LoyPointEarn
     * 
     * @var int
     */
    public $loyPointEarn = null;
    
    /**
     * PaymentTypes
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfPaymentTypeWS
     */
    public $paymentTypes = null;
    
    /**
     * ReceiptId
     * 
     * @var int
     */
    public $receiptId;
    
    /**
     * ReceiptLinelst
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS
     */
    public $receiptLinelst = null;
    
    /**
     * ReceiptNum
     * 
     * @var int
     */
    public $receiptNum;
    
    /**
     * ReturnReceiptIds
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptIdWS
     */
    public $returnReceiptIds = null;
    
    /**
     * Get amReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS
     */
    public function getAmReceipt()
    {
        return $this->amReceipt;
    }
    
    /**
     * Set amReceipt
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value amReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setAmReceipt(\Adeo\Sirius\CustomerReceiptService\Type\Base\AmountWS $value = null)
    {
        $this->amReceipt = $value;
        return $this;
    }
    
    /**
     * Get buyingChannelDesc
     * 
     * @return string
     */
    public function getBuyingChannelDesc()
    {
        return $this->buyingChannelDesc;
    }
    
    /**
     * Set buyingChannelDesc
     * 
     * @param string $value buyingChannelDesc
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setBuyingChannelDesc(string $value = null)
    {
        $this->buyingChannelDesc = $value;
        return $this;
    }
    
    /**
     * Get checkoutNum
     * 
     * @return int
     */
    public function getCheckoutNum()
    {
        return $this->checkoutNum;
    }
    
    /**
     * Set checkoutNum
     * 
     * @param int $value checkoutNum
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setCheckoutNum($value)
    {
        $this->checkoutNum = $value;
        return $this;
    }
    
    /**
     * Get customerRgrp
     * 
     * @return string
     */
    public function getCustomerRgrp()
    {
        return $this->customerRgrp;
    }
    
    /**
     * Set customerRgrp
     * 
     * @param string $value customerRgrp
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setCustomerRgrp(string $value = null)
    {
        $this->customerRgrp = $value;
        return $this;
    }
    
    /**
     * Get dateReceipt
     * 
     * @return string
     */
    public function getDateReceipt()
    {
        return $this->dateReceipt;
    }
    
    /**
     * Set dateReceipt
     * 
     * @param string $value dateReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setDateReceipt(string $value = null)
    {
        $this->dateReceipt = $value;
        return $this;
    }
    
    /**
     * Get dateUpdate
     * 
     * @return string
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
    
    /**
     * Set dateUpdate
     * 
     * @param string $value dateUpdate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setDateUpdate(string $value = null)
    {
        $this->dateUpdate = $value;
        return $this;
    }
    
    /**
     * Get entityId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public function getEntityId()
    {
        return $this->entityId;
    }
    
    /**
     * Set entityId
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS $value entityId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setEntityId(\Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS $value = null)
    {
        $this->entityId = $value;
        return $this;
    }
    
    /**
     * Get loyPointEarn
     * 
     * @return int
     */
    public function getLoyPointEarn()
    {
        return $this->loyPointEarn;
    }
    
    /**
     * Set loyPointEarn
     * 
     * @param int $value loyPointEarn
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setLoyPointEarn($value = null)
    {
        $this->loyPointEarn = $value;
        return $this;
    }
    
    /**
     * Get paymentTypes
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfPaymentTypeWS
     */
    public function getPaymentTypes()
    {
        return $this->paymentTypes;
    }
    
    /**
     * Set paymentTypes
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfPaymentTypeWS $value paymentTypes
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setPaymentTypes(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfPaymentTypeWS $value = null)
    {
        $this->paymentTypes = $value;
        return $this;
    }
    
    /**
     * Get receiptId
     * 
     * @return int
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }
    
    /**
     * Set receiptId
     * 
     * @param int $value receiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setReceiptId($value)
    {
        $this->receiptId = $value;
        return $this;
    }
    
    /**
     * Get receiptLinelst
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS
     */
    public function getReceiptLinelst()
    {
        return $this->receiptLinelst;
    }
    
    /**
     * Set receiptLinelst
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS $value receiptLinelst
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setReceiptLinelst(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS $value = null)
    {
        $this->receiptLinelst = $value;
        return $this;
    }
    
    /**
     * Get receiptNum
     * 
     * @return int
     */
    public function getReceiptNum()
    {
        return $this->receiptNum;
    }
    
    /**
     * Set receiptNum
     * 
     * @param int $value receiptNum
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setReceiptNum($value)
    {
        $this->receiptNum = $value;
        return $this;
    }
    
    /**
     * Get returnReceiptIds
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptIdWS
     */
    public function getReturnReceiptIds()
    {
        return $this->returnReceiptIds;
    }
    
    /**
     * Set returnReceiptIds
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptIdWS $value returnReceiptIds
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptHeaderWS
     */
    public function setReturnReceiptIds(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptIdWS $value = null)
    {
        $this->returnReceiptIds = $value;
        return $this;
    }
}
