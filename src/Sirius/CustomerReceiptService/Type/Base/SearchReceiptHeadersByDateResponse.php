<?php
/**
 * Parent model for SearchReceiptHeadersByDateResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptHeadersByDateResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public $out = null;
    
    /**
     * Construct SearchReceiptHeadersByDateResponse
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse $value out
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeadersByDateResponse
     */
    public function setOut(\Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
