<?php
/**
 * Parent model for GetReceiptByNumRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceiptByNumRequest
{
    
    /**
     * NumBu
     * 
     * @var int
     */
    public $numBu = null;
    
    /**
     * NumReceipt
     * 
     * @var int
     */
    public $numReceipt = null;
    
    /**
     * NumTill
     * 
     * @var int
     */
    public $numTill = null;
    
    /**
     * StoreNumber
     * 
     * @var string
     */
    public $storeNumber = null;
    
    /**
     * TicketDate
     * 
     * @var string
     */
    public $ticketDate = null;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get numBu
     * 
     * @return int
     */
    public function getNumBu()
    {
        return $this->numBu;
    }
    
    /**
     * Set numBu
     * 
     * @param int $value numBu
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setNumBu($value = null)
    {
        $this->numBu = $value;
        return $this;
    }
    
    /**
     * Get numReceipt
     * 
     * @return int
     */
    public function getNumReceipt()
    {
        return $this->numReceipt;
    }
    
    /**
     * Set numReceipt
     * 
     * @param int $value numReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setNumReceipt($value = null)
    {
        $this->numReceipt = $value;
        return $this;
    }
    
    /**
     * Get numTill
     * 
     * @return int
     */
    public function getNumTill()
    {
        return $this->numTill;
    }
    
    /**
     * Set numTill
     * 
     * @param int $value numTill
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setNumTill($value = null)
    {
        $this->numTill = $value;
        return $this;
    }
    
    /**
     * Get storeNumber
     * 
     * @return string
     */
    public function getStoreNumber()
    {
        return $this->storeNumber;
    }
    
    /**
     * Set storeNumber
     * 
     * @param string $value storeNumber
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setStoreNumber(string $value = null)
    {
        $this->storeNumber = $value;
        return $this;
    }
    
    /**
     * Get ticketDate
     * 
     * @return string
     */
    public function getTicketDate()
    {
        return $this->ticketDate;
    }
    
    /**
     * Set ticketDate
     * 
     * @param string $value ticketDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setTicketDate(string $value = null)
    {
        $this->ticketDate = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
