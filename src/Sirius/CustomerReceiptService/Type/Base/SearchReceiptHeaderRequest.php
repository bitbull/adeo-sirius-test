<?php
/**
 * Parent model for SearchReceiptHeaderRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptHeaderRequest
{
    
    /**
     * IdxEnd
     * 
     * @var int
     */
    public $idxEnd;
    
    /**
     * IdxStart
     * 
     * @var int
     */
    public $idxStart;
    
    /**
     * NumRGRP
     * 
     * @var string
     */
    public $numRGRP = null;
    
    /**
     * NumRgrpCoTitulaire
     * 
     * @var string
     */
    public $numRgrpCoTitulaire = null;
    
    /**
     * RefCde
     * 
     * @var int
     */
    public $refCde;
    
    /**
     * RefItem
     * 
     * @var int
     */
    public $refItem;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get idxEnd
     * 
     * @return int
     */
    public function getIdxEnd()
    {
        return $this->idxEnd;
    }
    
    /**
     * Set idxEnd
     * 
     * @param int $value idxEnd
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setIdxEnd($value)
    {
        $this->idxEnd = $value;
        return $this;
    }
    
    /**
     * Get idxStart
     * 
     * @return int
     */
    public function getIdxStart()
    {
        return $this->idxStart;
    }
    
    /**
     * Set idxStart
     * 
     * @param int $value idxStart
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setIdxStart($value)
    {
        $this->idxStart = $value;
        return $this;
    }
    
    /**
     * Get numRGRP
     * 
     * @return string
     */
    public function getNumRGRP()
    {
        return $this->numRGRP;
    }
    
    /**
     * Set numRGRP
     * 
     * @param string $value numRGRP
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setNumRGRP(string $value = null)
    {
        $this->numRGRP = $value;
        return $this;
    }
    
    /**
     * Get numRgrpCoTitulaire
     * 
     * @return string
     */
    public function getNumRgrpCoTitulaire()
    {
        return $this->numRgrpCoTitulaire;
    }
    
    /**
     * Set numRgrpCoTitulaire
     * 
     * @param string $value numRgrpCoTitulaire
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setNumRgrpCoTitulaire(string $value = null)
    {
        $this->numRgrpCoTitulaire = $value;
        return $this;
    }
    
    /**
     * Get refCde
     * 
     * @return int
     */
    public function getRefCde()
    {
        return $this->refCde;
    }
    
    /**
     * Set refCde
     * 
     * @param int $value refCde
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setRefCde($value)
    {
        $this->refCde = $value;
        return $this;
    }
    
    /**
     * Get refItem
     * 
     * @return int
     */
    public function getRefItem()
    {
        return $this->refItem;
    }
    
    /**
     * Set refItem
     * 
     * @param int $value refItem
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setRefItem($value)
    {
        $this->refItem = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
