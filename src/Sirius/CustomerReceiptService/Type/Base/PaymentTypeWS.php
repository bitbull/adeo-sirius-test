<?php
/**
 * Parent model for PaymentTypeWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class PaymentTypeWS
{
    
    /**
     * LongLabel
     * 
     * @var string
     */
    public $longLabel = null;
    
    /**
     * ShortLabel
     * 
     * @var string
     */
    public $shortLabel = null;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode = null;
    
    /**
     * Get longLabel
     * 
     * @return string
     */
    public function getLongLabel()
    {
        return $this->longLabel;
    }
    
    /**
     * Set longLabel
     * 
     * @param string $value longLabel
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS
     */
    public function setLongLabel(string $value = null)
    {
        $this->longLabel = $value;
        return $this;
    }
    
    /**
     * Get shortLabel
     * 
     * @return string
     */
    public function getShortLabel()
    {
        return $this->shortLabel;
    }
    
    /**
     * Set shortLabel
     * 
     * @param string $value shortLabel
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS
     */
    public function setShortLabel(string $value = null)
    {
        $this->shortLabel = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS
     */
    public function setTypeCode($value = null)
    {
        $this->typeCode = $value;
        return $this;
    }
}
