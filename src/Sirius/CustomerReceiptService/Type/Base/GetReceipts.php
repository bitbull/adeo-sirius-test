<?php
/**
 * Parent model for GetReceipts
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceipts
{
    
    /**
     * In0
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest
     */
    public $in0 = null;
    
    /**
     * Construct GetReceipts
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest $in0 Value of in0
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest $in0 = null)
    {
        $this->in0 = $in0;
    }
    
    /**
     * Get in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest $value in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceipts
     */
    public function setIn0(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest $value = null)
    {
        $this->in0 = $value;
        return $this;
    }
}
