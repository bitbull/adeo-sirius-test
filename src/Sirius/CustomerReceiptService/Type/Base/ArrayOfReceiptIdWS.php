<?php
/**
 * Parent model for ArrayOfReceiptIdWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfReceiptIdWS
{
    
    /**
     * ReceiptIdWS collection
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS[]
     */
    public $ReceiptIdWS = array();
    
    /**
     * Get ReceiptIdWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS[]
     */
    public function getReceiptIdWS()
    {
        return $this->ReceiptIdWS;
    }
    
    /**
     * Add element on ReceiptIdWS collection
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS[] $value ReceiptIdWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptIdWS
     */
    public function setReceiptIdWS($value = null)
    {
        $this->ReceiptIdWS[] = $value;
        return $this;
    }
}
