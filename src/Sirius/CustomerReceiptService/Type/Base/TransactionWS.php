<?php
/**
 * Parent model for TransactionWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class TransactionWS
{
    
    /**
     * Description
     * 
     * @var string
     */
    public $description = null;
    
    /**
     * TypeTransaction
     * 
     * @var int
     */
    public $typeTransaction;
    
    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description
     * 
     * @param string $value description
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS
     */
    public function setDescription(string $value = null)
    {
        $this->description = $value;
        return $this;
    }
    
    /**
     * Get typeTransaction
     * 
     * @return int
     */
    public function getTypeTransaction()
    {
        return $this->typeTransaction;
    }
    
    /**
     * Set typeTransaction
     * 
     * @param int $value typeTransaction
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\TransactionWS
     */
    public function setTypeTransaction($value)
    {
        $this->typeTransaction = $value;
        return $this;
    }
}
