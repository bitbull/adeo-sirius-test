<?php
/**
 * Parent model for SearchReceiptHeaderByDateRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptHeaderByDateRequest
{
    
    /**
     * IdxEnd
     * 
     * @var int
     */
    public $idxEnd;
    
    /**
     * IdxStart
     * 
     * @var int
     */
    public $idxStart;
    
    /**
     * MaxDate
     * 
     * @var string
     */
    public $maxDate = null;
    
    /**
     * MinDate
     * 
     * @var string
     */
    public $minDate = null;
    
    /**
     * NumRGRP
     * 
     * @var string
     */
    public $numRGRP = null;
    
    /**
     * RefItem
     * 
     * @var int
     */
    public $refItem;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get idxEnd
     * 
     * @return int
     */
    public function getIdxEnd()
    {
        return $this->idxEnd;
    }
    
    /**
     * Set idxEnd
     * 
     * @param int $value idxEnd
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setIdxEnd($value)
    {
        $this->idxEnd = $value;
        return $this;
    }
    
    /**
     * Get idxStart
     * 
     * @return int
     */
    public function getIdxStart()
    {
        return $this->idxStart;
    }
    
    /**
     * Set idxStart
     * 
     * @param int $value idxStart
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setIdxStart($value)
    {
        $this->idxStart = $value;
        return $this;
    }
    
    /**
     * Get maxDate
     * 
     * @return string
     */
    public function getMaxDate()
    {
        return $this->maxDate;
    }
    
    /**
     * Set maxDate
     * 
     * @param string $value maxDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setMaxDate(string $value = null)
    {
        $this->maxDate = $value;
        return $this;
    }
    
    /**
     * Get minDate
     * 
     * @return string
     */
    public function getMinDate()
    {
        return $this->minDate;
    }
    
    /**
     * Set minDate
     * 
     * @param string $value minDate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setMinDate(string $value = null)
    {
        $this->minDate = $value;
        return $this;
    }
    
    /**
     * Get numRGRP
     * 
     * @return string
     */
    public function getNumRGRP()
    {
        return $this->numRGRP;
    }
    
    /**
     * Set numRGRP
     * 
     * @param string $value numRGRP
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setNumRGRP(string $value = null)
    {
        $this->numRGRP = $value;
        return $this;
    }
    
    /**
     * Get refItem
     * 
     * @return int
     */
    public function getRefItem()
    {
        return $this->refItem;
    }
    
    /**
     * Set refItem
     * 
     * @param int $value refItem
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setRefItem($value)
    {
        $this->refItem = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderByDateRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
