<?php
/**
 * Parent model for ArrayOfString
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfString
{
    
    /**
     * String collection
     * 
     * @var string[]
     */
    public $string = array();
    
    /**
     * Get string
     * 
     * @return string[]
     */
    public function getString()
    {
        return $this->string;
    }
    
    /**
     * Add element on string collection
     * 
     * @param string[] $value string
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfString
     */
    public function setString($value = null)
    {
        $this->string[] = $value;
        return $this;
    }
}
