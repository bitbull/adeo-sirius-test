<?php
/**
 * Parent model for VATRateWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class VATRateWS
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code = null;
    
    /**
     * Rate
     * 
     * @var float
     */
    public $rate = null;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS
     */
    public function setCode($value = null)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get rate
     * 
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }
    
    /**
     * Set rate
     * 
     * @param float $value rate
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\VATRateWS
     */
    public function setRate($value = null)
    {
        $this->rate = $value;
        return $this;
    }
}
