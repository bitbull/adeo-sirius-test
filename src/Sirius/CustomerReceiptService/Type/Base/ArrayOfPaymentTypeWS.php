<?php
/**
 * Parent model for ArrayOfPaymentTypeWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ArrayOfPaymentTypeWS
{
    
    /**
     * PaymentTypeWS collection
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS[]
     */
    public $PaymentTypeWS = array();
    
    /**
     * Get PaymentTypeWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS[]
     */
    public function getPaymentTypeWS()
    {
        return $this->PaymentTypeWS;
    }
    
    /**
     * Add element on PaymentTypeWS collection
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\PaymentTypeWS[] $value PaymentTypeWS
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfPaymentTypeWS
     */
    public function setPaymentTypeWS($value = null)
    {
        $this->PaymentTypeWS[] = $value;
        return $this;
    }
}
