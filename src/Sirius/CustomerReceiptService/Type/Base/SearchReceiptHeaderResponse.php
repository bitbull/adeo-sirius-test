<?php
/**
 * Parent model for SearchReceiptHeaderResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SearchReceiptHeaderResponse
{
    
    /**
     * ErrorMsg
     * 
     * @var string
     */
    public $errorMsg = null;
    
    /**
     * ReceiptHeaders
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptHeaderWS
     */
    public $receiptHeaders = null;
    
    /**
     * RecordCount
     * 
     * @var int
     */
    public $recordCount = null;
    
    /**
     * ResponseCode
     * 
     * @var int
     */
    public $responseCode;
    
    /**
     * StackTrace
     * 
     * @var string
     */
    public $stackTrace = null;
    
    /**
     * Get errorMsg
     * 
     * @return string
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }
    
    /**
     * Set errorMsg
     * 
     * @param string $value errorMsg
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function setErrorMsg(string $value = null)
    {
        $this->errorMsg = $value;
        return $this;
    }
    
    /**
     * Get receiptHeaders
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptHeaderWS
     */
    public function getReceiptHeaders()
    {
        return $this->receiptHeaders;
    }
    
    /**
     * Set receiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptHeaderWS $value receiptHeaders
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function setReceiptHeaders(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptHeaderWS $value = null)
    {
        $this->receiptHeaders = $value;
        return $this;
    }
    
    /**
     * Get recordCount
     * 
     * @return int
     */
    public function getRecordCount()
    {
        return $this->recordCount;
    }
    
    /**
     * Set recordCount
     * 
     * @param int $value recordCount
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function setRecordCount($value = null)
    {
        $this->recordCount = $value;
        return $this;
    }
    
    /**
     * Get responseCode
     * 
     * @return int
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }
    
    /**
     * Set responseCode
     * 
     * @param int $value responseCode
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function setResponseCode($value)
    {
        $this->responseCode = $value;
        return $this;
    }
    
    /**
     * Get stackTrace
     * 
     * @return string
     */
    public function getStackTrace()
    {
        return $this->stackTrace;
    }
    
    /**
     * Set stackTrace
     * 
     * @param string $value stackTrace
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeaderResponse
     */
    public function setStackTrace(string $value = null)
    {
        $this->stackTrace = $value;
        return $this;
    }
}
