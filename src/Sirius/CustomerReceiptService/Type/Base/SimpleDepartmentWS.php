<?php
/**
 * Parent model for SimpleDepartmentWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class SimpleDepartmentWS
{
    
    /**
     * Description
     * 
     * @var string
     */
    public $description = null;
    
    /**
     * NumDepartment
     * 
     * @var int
     */
    public $numDepartment;
    
    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description
     * 
     * @param string $value description
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS
     */
    public function setDescription(string $value = null)
    {
        $this->description = $value;
        return $this;
    }
    
    /**
     * Get numDepartment
     * 
     * @return int
     */
    public function getNumDepartment()
    {
        return $this->numDepartment;
    }
    
    /**
     * Set numDepartment
     * 
     * @param int $value numDepartment
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\SimpleDepartmentWS
     */
    public function setNumDepartment($value)
    {
        $this->numDepartment = $value;
        return $this;
    }
}
