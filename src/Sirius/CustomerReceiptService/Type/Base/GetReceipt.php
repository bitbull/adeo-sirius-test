<?php
/**
 * Parent model for GetReceipt
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceipt
{
    
    /**
     * In0
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest
     */
    public $in0 = null;
    
    /**
     * Construct GetReceipt
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest $in0 Value of in0
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest $in0 = null)
    {
        $this->in0 = $in0;
    }
    
    /**
     * Get in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest $value in0
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceipt
     */
    public function setIn0(\Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptRequest $value = null)
    {
        $this->in0 = $value;
        return $this;
    }
}
