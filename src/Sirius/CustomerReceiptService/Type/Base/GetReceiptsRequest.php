<?php
/**
 * Parent model for GetReceiptsRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class GetReceiptsRequest
{
    
    /**
     * ReceiptIds
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public $receiptIds = null;
    
    /**
     * Version
     * 
     * @var string
     */
    public $version = null;
    
    /**
     * Get receiptIds
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong
     */
    public function getReceiptIds()
    {
        return $this->receiptIds;
    }
    
    /**
     * Set receiptIds
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value receiptIds
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest
     */
    public function setReceiptIds(\Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfLong $value = null)
    {
        $this->receiptIds = $value;
        return $this;
    }
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptsRequest
     */
    public function setVersion(string $value = null)
    {
        $this->version = $value;
        return $this;
    }
}
