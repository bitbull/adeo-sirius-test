<?php
/**
 * Parent model for EntityWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class EntityWS
{
    
    /**
     * EntityLabel
     * 
     * @var string
     */
    public $entityLabel = null;
    
    /**
     * EntityNum
     * 
     * @var int
     */
    public $entityNum;
    
    /**
     * EntityType
     * 
     * @var int
     */
    public $entityType;
    
    /**
     * Get entityLabel
     * 
     * @return string
     */
    public function getEntityLabel()
    {
        return $this->entityLabel;
    }
    
    /**
     * Set entityLabel
     * 
     * @param string $value entityLabel
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public function setEntityLabel(string $value = null)
    {
        $this->entityLabel = $value;
        return $this;
    }
    
    /**
     * Get entityNum
     * 
     * @return int
     */
    public function getEntityNum()
    {
        return $this->entityNum;
    }
    
    /**
     * Set entityNum
     * 
     * @param int $value entityNum
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public function setEntityNum($value)
    {
        $this->entityNum = $value;
        return $this;
    }
    
    /**
     * Get entityType
     * 
     * @return int
     */
    public function getEntityType()
    {
        return $this->entityType;
    }
    
    /**
     * Set entityType
     * 
     * @param int $value entityType
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public function setEntityType($value)
    {
        $this->entityType = $value;
        return $this;
    }
}
