<?php
/**
 * Parent model for ReceiptIdWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type\Base;

abstract class ReceiptIdWS
{
    
    /**
     * CheckoutNumber
     * 
     * @var int
     */
    public $checkoutNumber;
    
    /**
     * DateReceipt
     * 
     * @var string
     */
    public $dateReceipt = null;
    
    /**
     * EntityId
     * 
     * @var \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public $entityId = null;
    
    /**
     * ReceiptId
     * 
     * @var int
     */
    public $receiptId;
    
    /**
     * ReceiptNumber
     * 
     * @var int
     */
    public $receiptNumber;
    
    /**
     * Get checkoutNumber
     * 
     * @return int
     */
    public function getCheckoutNumber()
    {
        return $this->checkoutNumber;
    }
    
    /**
     * Set checkoutNumber
     * 
     * @param int $value checkoutNumber
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function setCheckoutNumber($value)
    {
        $this->checkoutNumber = $value;
        return $this;
    }
    
    /**
     * Get dateReceipt
     * 
     * @return string
     */
    public function getDateReceipt()
    {
        return $this->dateReceipt;
    }
    
    /**
     * Set dateReceipt
     * 
     * @param string $value dateReceipt
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function setDateReceipt(string $value = null)
    {
        $this->dateReceipt = $value;
        return $this;
    }
    
    /**
     * Get entityId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS
     */
    public function getEntityId()
    {
        return $this->entityId;
    }
    
    /**
     * Set entityId
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS $value entityId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function setEntityId(\Adeo\Sirius\CustomerReceiptService\Type\Base\EntityWS $value = null)
    {
        $this->entityId = $value;
        return $this;
    }
    
    /**
     * Get receiptId
     * 
     * @return int
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }
    
    /**
     * Set receiptId
     * 
     * @param int $value receiptId
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function setReceiptId($value)
    {
        $this->receiptId = $value;
        return $this;
    }
    
    /**
     * Get receiptNumber
     * 
     * @return int
     */
    public function getReceiptNumber()
    {
        return $this->receiptNumber;
    }
    
    /**
     * Set receiptNumber
     * 
     * @param int $value receiptNumber
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\Base\ReceiptIdWS
     */
    public function setReceiptNumber($value)
    {
        $this->receiptNumber = $value;
        return $this;
    }
}
