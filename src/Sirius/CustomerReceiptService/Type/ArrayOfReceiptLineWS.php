<?php
/**
 * Model for ArrayOfReceiptLineWS
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type;

use \Adeo\Sirius\CustomerReceiptService\Type\Base\ArrayOfReceiptLineWS as ArrayOfReceiptLineWSBase;

class ArrayOfReceiptLineWS
    extends ArrayOfReceiptLineWSBase
{
}
