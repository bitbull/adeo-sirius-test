<?php
/**
 * Model for GetReceiptByNumResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type;

use \Adeo\Sirius\CustomerReceiptService\Type\Base\GetReceiptByNumResponse as GetReceiptByNumResponseBase;

class GetReceiptByNumResponse
    extends getReceiptByNumResponseBase
{
}
