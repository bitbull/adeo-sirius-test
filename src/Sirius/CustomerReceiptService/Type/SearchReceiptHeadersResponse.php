<?php
/**
 * Model for SearchReceiptHeadersResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type;

use \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptHeadersResponse as SearchReceiptHeadersResponseBase;

class SearchReceiptHeadersResponse
    extends searchReceiptHeadersResponseBase
{
}
