<?php
/**
 * Model for SearchReceiptByMagResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerReceiptService\Type;

use \Adeo\Sirius\CustomerReceiptService\Type\Base\SearchReceiptByMagResponse as SearchReceiptByMagResponseBase;

class SearchReceiptByMagResponse
    extends searchReceiptByMagResponseBase
{
}
