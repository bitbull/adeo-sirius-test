<?php
/**
 * Model for CustomerPreExistingExternalIdentifierCriterionDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type;

use \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO as CustomerPreExistingExternalIdentifierCriterionDTOBase;

class CustomerPreExistingExternalIdentifierCriterionDTO
    extends customerPreExistingExternalIdentifierCriterionDTOBase
{
}
