<?php
/**
 * Parent model for CustomerPreExistingExternalIdentifierCriterionDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingExternalIdentifierCriterionDTO
{
    
    /**
     * Type
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierTypeDTO
     */
    public $type;
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get type
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierTypeDTO
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierTypeDTO $value type
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
