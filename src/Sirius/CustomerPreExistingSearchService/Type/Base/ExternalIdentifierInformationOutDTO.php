<?php
/**
 * Parent model for ExternalIdentifierInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class ExternalIdentifierInformationOutDTO
{
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Date
     * 
     * @var string
     */
    public $date;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ExternalIdentifierInformationOutDTO
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get date
     * 
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set date
     * 
     * @param string $value date
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ExternalIdentifierInformationOutDTO
     */
    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ExternalIdentifierInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ExternalIdentifierInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ExternalIdentifierInformationOutDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
