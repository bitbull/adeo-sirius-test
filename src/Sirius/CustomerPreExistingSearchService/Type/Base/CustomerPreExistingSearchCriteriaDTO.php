<?php
/**
 * Parent model for CustomerPreExistingSearchCriteriaDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingSearchCriteriaDTO
{
    
    /**
     * ApplicationCode
     * 
     * @var string
     */
    public $applicationCode;
    
    /**
     * LanguageCode
     * 
     * @var string
     */
    public $languageCode;
    
    /**
     * MinRequiredRelevance
     * 
     * @var float
     */
    public $minRequiredRelevance;
    
    /**
     * BuNumber
     * 
     * @var int
     */
    public $buNumber;
    
    /**
     * CustomerStatus collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingStatusDTO[]
     */
    public $customerStatus = array();
    
    /**
     * CustomerType collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingTypeDTO[]
     */
    public $customerType = array();
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Address
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public $address;
    
    /**
     * MainCriteriaSearchList collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO[]
     */
    public $mainCriteriaSearchList = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingClassificationCriterionDTO[]
     */
    public $classifications = array();
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get applicationCode
     * 
     * @return string
     */
    public function getApplicationCode()
    {
        return $this->applicationCode;
    }
    
    /**
     * Set applicationCode
     * 
     * @param string $value applicationCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setApplicationCode($value)
    {
        $this->applicationCode = $value;
        return $this;
    }
    
    /**
     * Get languageCode
     * 
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }
    
    /**
     * Set languageCode
     * 
     * @param string $value languageCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setLanguageCode($value)
    {
        $this->languageCode = $value;
        return $this;
    }
    
    /**
     * Get minRequiredRelevance
     * 
     * @return float
     */
    public function getMinRequiredRelevance()
    {
        return $this->minRequiredRelevance;
    }
    
    /**
     * Set minRequiredRelevance
     * 
     * @param float $value minRequiredRelevance
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setMinRequiredRelevance($value)
    {
        $this->minRequiredRelevance = $value;
        return $this;
    }
    
    /**
     * Get buNumber
     * 
     * @return int
     */
    public function getBuNumber()
    {
        return $this->buNumber;
    }
    
    /**
     * Set buNumber
     * 
     * @param int $value buNumber
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setBuNumber($value)
    {
        $this->buNumber = $value;
        return $this;
    }
    
    /**
     * Get customerStatus
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingStatusDTO[]
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }
    
    /**
     * Add element on customerStatus collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingStatusDTO[] $value customerStatus
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setCustomerStatus($value = null)
    {
        $this->customerStatus[] = $value;
        return $this;
    }
    
    /**
     * Get customerType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingTypeDTO[]
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }
    
    /**
     * Add element on customerType collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingTypeDTO[] $value customerType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setCustomerType($value = null)
    {
        $this->customerType[] = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get address
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set address
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO $value address
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setAddress(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO $value)
    {
        $this->address = $value;
        return $this;
    }
    
    /**
     * Get mainCriteriaSearchList
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO[]
     */
    public function getMainCriteriaSearchList()
    {
        return $this->mainCriteriaSearchList;
    }
    
    /**
     * Add element on mainCriteriaSearchList collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO[] $value mainCriteriaSearchList
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setMainCriteriaSearchList($value = null)
    {
        $this->mainCriteriaSearchList[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingClassificationCriterionDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingClassificationCriterionDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdentifierCriterionDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
