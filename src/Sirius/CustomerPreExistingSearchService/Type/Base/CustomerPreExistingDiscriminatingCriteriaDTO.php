<?php
/**
 * Parent model for CustomerPreExistingDiscriminatingCriteriaDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingDiscriminatingCriteriaDTO
{
    
    /**
     * DiscriminatingCriteria
     * 
     * @var string
     */
    public $discriminatingCriteria;
    
    /**
     * ExternalIdTypeOrCommunicationType
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdTypeOrCommunicationTypeDTO
     */
    public $externalIdTypeOrCommunicationType;
    
    /**
     * DiscriminatingCriteriaType
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaTypeDTO
     */
    public $discriminatingCriteriaType;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get discriminatingCriteria
     * 
     * @return string
     */
    public function getDiscriminatingCriteria()
    {
        return $this->discriminatingCriteria;
    }
    
    /**
     * Set discriminatingCriteria
     * 
     * @param string $value discriminatingCriteria
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO
     */
    public function setDiscriminatingCriteria($value)
    {
        $this->discriminatingCriteria = $value;
        return $this;
    }
    
    /**
     * Get externalIdTypeOrCommunicationType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdTypeOrCommunicationTypeDTO
     */
    public function getExternalIdTypeOrCommunicationType()
    {
        return $this->externalIdTypeOrCommunicationType;
    }
    
    /**
     * Set externalIdTypeOrCommunicationType
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdTypeOrCommunicationTypeDTO $value externalIdTypeOrCommunicationType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO
     */
    public function setExternalIdTypeOrCommunicationType(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingExternalIdTypeOrCommunicationTypeDTO $value)
    {
        $this->externalIdTypeOrCommunicationType = $value;
        return $this;
    }
    
    /**
     * Get discriminatingCriteriaType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaTypeDTO
     */
    public function getDiscriminatingCriteriaType()
    {
        return $this->discriminatingCriteriaType;
    }
    
    /**
     * Set discriminatingCriteriaType
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaTypeDTO $value discriminatingCriteriaType
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO
     */
    public function setDiscriminatingCriteriaType(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaTypeDTO $value)
    {
        $this->discriminatingCriteriaType = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingDiscriminatingCriteriaDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
