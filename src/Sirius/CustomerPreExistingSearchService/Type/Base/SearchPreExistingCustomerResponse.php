<?php
/**
 * Parent model for SearchPreExistingCustomerResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class SearchPreExistingCustomerResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SearchPreExistingCustomerResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
