<?php
/**
 * Parent model for CustomerPreExistingAddressCriteriaDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingAddressCriteriaDTO
{
    
    /**
     * Country
     * 
     * @var string
     */
    public $country;
    
    /**
     * PostalCode
     * 
     * @var string
     */
    public $postalCode;
    
    /**
     * City
     * 
     * @var string
     */
    public $city;
    
    /**
     * Province
     * 
     * @var string
     */
    public $province;
    
    /**
     * AddressLine
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public $addressLine;
    
    /**
     * AddressExternalCodes collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressExternalCodeCriterionDTO[]
     */
    public $addressExternalCodes = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get country
     * 
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set country
     * 
     * @param string $value country
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setCountry($value)
    {
        $this->country = $value;
        return $this;
    }
    
    /**
     * Get postalCode
     * 
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * Set postalCode
     * 
     * @param string $value postalCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setPostalCode($value)
    {
        $this->postalCode = $value;
        return $this;
    }
    
    /**
     * Get city
     * 
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Set city
     * 
     * @param string $value city
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setCity($value)
    {
        $this->city = $value;
        return $this;
    }
    
    /**
     * Get province
     * 
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    /**
     * Set province
     * 
     * @param string $value province
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setProvince($value)
    {
        $this->province = $value;
        return $this;
    }
    
    /**
     * Get addressLine
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function getAddressLine()
    {
        return $this->addressLine;
    }
    
    /**
     * Set addressLine
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO $value addressLine
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setAddressLine(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO $value)
    {
        $this->addressLine = $value;
        return $this;
    }
    
    /**
     * Get addressExternalCodes
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressExternalCodeCriterionDTO[]
     */
    public function getAddressExternalCodes()
    {
        return $this->addressExternalCodes;
    }
    
    /**
     * Add element on addressExternalCodes collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressExternalCodeCriterionDTO[] $value addressExternalCodes
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setAddressExternalCodes($value = null)
    {
        $this->addressExternalCodes[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressCriteriaDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
