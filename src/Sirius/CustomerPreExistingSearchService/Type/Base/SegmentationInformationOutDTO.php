<?php
/**
 * Parent model for SegmentationInformationOutDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class SegmentationInformationOutDTO
{
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * ValueCode
     * 
     * @var int
     */
    public $valueCode;
    
    /**
     * ValueLabel
     * 
     * @var string
     */
    public $valueLabel;
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get valueCode
     * 
     * @return int
     */
    public function getValueCode()
    {
        return $this->valueCode;
    }
    
    /**
     * Set valueCode
     * 
     * @param int $value valueCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO
     */
    public function setValueCode($value)
    {
        $this->valueCode = $value;
        return $this;
    }
    
    /**
     * Get valueLabel
     * 
     * @return string
     */
    public function getValueLabel()
    {
        return $this->valueLabel;
    }
    
    /**
     * Set valueLabel
     * 
     * @param string $value valueLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO
     */
    public function setValueLabel($value)
    {
        $this->valueLabel = $value;
        return $this;
    }
}
