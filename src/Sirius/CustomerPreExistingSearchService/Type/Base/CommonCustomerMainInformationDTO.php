<?php
/**
 * Parent model for CommonCustomerMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CommonCustomerMainInformationDTO
{
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $classifications = array();
    
    /**
     * Communications collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO[]
     */
    public $communications = array();
    
    /**
     * CompanyName
     * 
     * @var string
     */
    public $companyName;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * CustomerTypeCode
     * 
     * @var int
     */
    public $customerTypeCode;
    
    /**
     * CustomerTypeLabel
     * 
     * @var string
     */
    public $customerTypeLabel;
    
    /**
     * Deletable
     * 
     * @var int
     */
    public $deletable;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * HouseholdNumber
     * 
     * @var int
     */
    public $householdNumber;
    
    /**
     * LegalEntityCustomerNumber
     * 
     * @var int
     */
    public $legalEntityCustomerNumber;
    
    /**
     * MasterNumber
     * 
     * @var int
     */
    public $masterNumber;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * Segmentations collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO[]
     */
    public $segmentations = array();
    
    /**
     * TitleCode
     * 
     * @var int
     */
    public $titleCode;
    
    /**
     * TitleLabel
     * 
     * @var string
     */
    public $titleLabel;
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\ClassificationInformationOutDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO[]
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Add element on communications collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO[] $value communications
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setCommunications($value = null)
    {
        $this->communications[] = $value;
        return $this;
    }
    
    /**
     * Get companyName
     * 
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    
    /**
     * Set companyName
     * 
     * @param string $value companyName
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setCompanyName($value)
    {
        $this->companyName = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get customerTypeCode
     * 
     * @return int
     */
    public function getCustomerTypeCode()
    {
        return $this->customerTypeCode;
    }
    
    /**
     * Set customerTypeCode
     * 
     * @param int $value customerTypeCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setCustomerTypeCode($value)
    {
        $this->customerTypeCode = $value;
        return $this;
    }
    
    /**
     * Get customerTypeLabel
     * 
     * @return string
     */
    public function getCustomerTypeLabel()
    {
        return $this->customerTypeLabel;
    }
    
    /**
     * Set customerTypeLabel
     * 
     * @param string $value customerTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setCustomerTypeLabel($value)
    {
        $this->customerTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get deletable
     * 
     * @return int
     */
    public function getDeletable()
    {
        return $this->deletable;
    }
    
    /**
     * Set deletable
     * 
     * @param int $value deletable
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setDeletable($value)
    {
        $this->deletable = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get householdNumber
     * 
     * @return int
     */
    public function getHouseholdNumber()
    {
        return $this->householdNumber;
    }
    
    /**
     * Set householdNumber
     * 
     * @param int $value householdNumber
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setHouseholdNumber($value)
    {
        $this->householdNumber = $value;
        return $this;
    }
    
    /**
     * Get legalEntityCustomerNumber
     * 
     * @return int
     */
    public function getLegalEntityCustomerNumber()
    {
        return $this->legalEntityCustomerNumber;
    }
    
    /**
     * Set legalEntityCustomerNumber
     * 
     * @param int $value legalEntityCustomerNumber
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setLegalEntityCustomerNumber($value)
    {
        $this->legalEntityCustomerNumber = $value;
        return $this;
    }
    
    /**
     * Get masterNumber
     * 
     * @return int
     */
    public function getMasterNumber()
    {
        return $this->masterNumber;
    }
    
    /**
     * Set masterNumber
     * 
     * @param int $value masterNumber
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setMasterNumber($value)
    {
        $this->masterNumber = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get segmentations
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO[]
     */
    public function getSegmentations()
    {
        return $this->segmentations;
    }
    
    /**
     * Add element on segmentations collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SegmentationInformationOutDTO[] $value segmentations
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setSegmentations($value = null)
    {
        $this->segmentations[] = $value;
        return $this;
    }
    
    /**
     * Get titleCode
     * 
     * @return int
     */
    public function getTitleCode()
    {
        return $this->titleCode;
    }
    
    /**
     * Set titleCode
     * 
     * @param int $value titleCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setTitleCode($value)
    {
        $this->titleCode = $value;
        return $this;
    }
    
    /**
     * Get titleLabel
     * 
     * @return string
     */
    public function getTitleLabel()
    {
        return $this->titleLabel;
    }
    
    /**
     * Set titleLabel
     * 
     * @param string $value titleLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommonCustomerMainInformationDTO
     */
    public function setTitleLabel($value)
    {
        $this->titleLabel = $value;
        return $this;
    }
}
