<?php
/**
 * Parent model for CustomerPreExistingCustomerListDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingCustomerListDTO
{
    
    /**
     * Customers collection
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO[]
     */
    public $customers = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customers
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }
    
    /**
     * Add element on customers collection
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO[] $value customers
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO
     */
    public function setCustomers($value = null)
    {
        $this->customers[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
