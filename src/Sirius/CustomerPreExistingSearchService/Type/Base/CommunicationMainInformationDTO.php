<?php
/**
 * Parent model for CommunicationMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CommunicationMainInformationDTO
{
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * Goneaway
     * 
     * @var boolean
     */
    public $goneaway;
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * MainCommunication
     * 
     * @var boolean
     */
    public $mainCommunication;
    
    /**
     * TypeCode
     * 
     * @var int
     */
    public $typeCode;
    
    /**
     * TypeLabel
     * 
     * @var string
     */
    public $typeLabel;
    
    /**
     * Value
     * 
     * @var string
     */
    public $value;
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get goneaway
     * 
     * @return boolean
     */
    public function getGoneaway()
    {
        return $this->goneaway;
    }
    
    /**
     * Set goneaway
     * 
     * @param boolean $value goneaway
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setGoneaway($value)
    {
        $this->goneaway = $value;
        return $this;
    }
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get mainCommunication
     * 
     * @return boolean
     */
    public function getMainCommunication()
    {
        return $this->mainCommunication;
    }
    
    /**
     * Set mainCommunication
     * 
     * @param boolean $value mainCommunication
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setMainCommunication($value)
    {
        $this->mainCommunication = $value;
        return $this;
    }
    
    /**
     * Get typeCode
     * 
     * @return int
     */
    public function getTypeCode()
    {
        return $this->typeCode;
    }
    
    /**
     * Set typeCode
     * 
     * @param int $value typeCode
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setTypeCode($value)
    {
        $this->typeCode = $value;
        return $this;
    }
    
    /**
     * Get typeLabel
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->typeLabel;
    }
    
    /**
     * Set typeLabel
     * 
     * @param string $value typeLabel
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setTypeLabel($value)
    {
        $this->typeLabel = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param string $value value
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CommunicationMainInformationDTO
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
