<?php
/**
 * Parent model for CustomerPreExistingSearchResponseDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingSearchResponseDTO
{
    
    /**
     * CustomerListResult
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO
     */
    public $customerListResult;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customerListResult
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO
     */
    public function getCustomerListResult()
    {
        return $this->customerListResult;
    }
    
    /**
     * Set customerListResult
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO $value customerListResult
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO
     */
    public function setCustomerListResult(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingCustomerListDTO $value)
    {
        $this->customerListResult = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchResponseDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
