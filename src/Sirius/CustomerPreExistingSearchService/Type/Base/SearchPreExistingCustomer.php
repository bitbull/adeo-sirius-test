<?php
/**
 * Parent model for SearchPreExistingCustomer
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class SearchPreExistingCustomer
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\SearchPreExistingCustomer
     */
    public function setArg0(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingSearchCriteriaDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
