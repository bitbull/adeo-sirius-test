<?php
/**
 * Parent model for CustomerPreExistingAddressLineCriterionDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingAddressLineCriterionDTO
{
    
    /**
     * LineOne
     * 
     * @var string
     */
    public $lineOne;
    
    /**
     * LineTwo
     * 
     * @var string
     */
    public $lineTwo;
    
    /**
     * LineThree
     * 
     * @var string
     */
    public $lineThree;
    
    /**
     * LineFour
     * 
     * @var string
     */
    public $lineFour;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get lineOne
     * 
     * @return string
     */
    public function getLineOne()
    {
        return $this->lineOne;
    }
    
    /**
     * Set lineOne
     * 
     * @param string $value lineOne
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function setLineOne($value)
    {
        $this->lineOne = $value;
        return $this;
    }
    
    /**
     * Get lineTwo
     * 
     * @return string
     */
    public function getLineTwo()
    {
        return $this->lineTwo;
    }
    
    /**
     * Set lineTwo
     * 
     * @param string $value lineTwo
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function setLineTwo($value)
    {
        $this->lineTwo = $value;
        return $this;
    }
    
    /**
     * Get lineThree
     * 
     * @return string
     */
    public function getLineThree()
    {
        return $this->lineThree;
    }
    
    /**
     * Set lineThree
     * 
     * @param string $value lineThree
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function setLineThree($value)
    {
        $this->lineThree = $value;
        return $this;
    }
    
    /**
     * Get lineFour
     * 
     * @return string
     */
    public function getLineFour()
    {
        return $this->lineFour;
    }
    
    /**
     * Set lineFour
     * 
     * @param string $value lineFour
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function setLineFour($value)
    {
        $this->lineFour = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressLineCriterionDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
