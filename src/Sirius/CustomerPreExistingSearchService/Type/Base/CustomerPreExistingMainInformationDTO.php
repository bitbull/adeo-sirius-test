<?php
/**
 * Parent model for CustomerPreExistingMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type\Base;

abstract class CustomerPreExistingMainInformationDTO
{
    
    /**
     * MainInformation
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerMainInformationDTO
     */
    public $mainInformation;
    
    /**
     * PerfectMatch
     * 
     * @var boolean
     */
    public $perfectMatch;
    
    /**
     * Relevance
     * 
     * @var \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\Float
     */
    public $relevance;
    
    /**
     * Get mainInformation
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerMainInformationDTO
     */
    public function getMainInformation()
    {
        return $this->mainInformation;
    }
    
    /**
     * Set mainInformation
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerMainInformationDTO $value mainInformation
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO
     */
    public function setMainInformation(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerMainInformationDTO $value)
    {
        $this->mainInformation = $value;
        return $this;
    }
    
    /**
     * Get perfectMatch
     * 
     * @return boolean
     */
    public function getPerfectMatch()
    {
        return $this->perfectMatch;
    }
    
    /**
     * Set perfectMatch
     * 
     * @param boolean $value perfectMatch
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO
     */
    public function setPerfectMatch($value)
    {
        $this->perfectMatch = $value;
        return $this;
    }
    
    /**
     * Get relevance
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\Float
     */
    public function getRelevance()
    {
        return $this->relevance;
    }
    
    /**
     * Set relevance
     * 
     * @param \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\Float $value relevance
     * 
     * @return \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingMainInformationDTO
     */
    public function setRelevance(\Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\Float $value)
    {
        $this->relevance = $value;
        return $this;
    }
}
