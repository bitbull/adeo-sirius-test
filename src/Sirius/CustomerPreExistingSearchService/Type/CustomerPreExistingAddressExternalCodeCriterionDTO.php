<?php
/**
 * Model for CustomerPreExistingAddressExternalCodeCriterionDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerPreExistingSearchService\Type;

use \Adeo\Sirius\CustomerPreExistingSearchService\Type\Base\CustomerPreExistingAddressExternalCodeCriterionDTO as CustomerPreExistingAddressExternalCodeCriterionDTOBase;

class CustomerPreExistingAddressExternalCodeCriterionDTO
    extends customerPreExistingAddressExternalCodeCriterionDTOBase
{
}
