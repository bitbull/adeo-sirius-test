<?php
/**
 * Proxy for CustomerQualityService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerQualityService as CustomerQualityServiceBase;
use \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneaway;
use \Adeo\Sirius\CustomerQualityService\Type\GetHistory;
use \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomer;
use \Adeo\Sirius\CustomerQualityService\Type\DelCustomer;

class CustomerQualityService
    extends CustomerQualityServiceBase
{
    
    /**
     * Method updAddressGoneaway
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneaway $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\UpdAddressGoneawayResponse
     */
    public function updAddressGoneaway(UpdAddressGoneaway $parameters)
    {
        $response = parent::_updAddressGoneaway($parameters);
        return $response;
    }
    
    /**
     * Method getHistory
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\GetHistory $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\GetHistoryResponse
     */
    public function getHistory(GetHistory $parameters)
    {
        $response = parent::_getHistory($parameters);
        return $response;
    }
    
    /**
     * Method reactivateCustomer
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\ReactivateCustomerResponse
     */
    public function reactivateCustomer(ReactivateCustomer $parameters)
    {
        $response = parent::_reactivateCustomer($parameters);
        return $response;
    }
    
    /**
     * Method delCustomer
     * 
     * @param \Adeo\Sirius\CustomerQualityService\Type\DelCustomer $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerQualityService\Type\DelCustomerResponse
     */
    public function delCustomer(DelCustomer $parameters)
    {
        $response = parent::_delCustomer($parameters);
        return $response;
    }
}
