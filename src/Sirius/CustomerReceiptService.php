<?php
/**
 * Proxy for CustomerReceiptService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerReceiptService as CustomerReceiptServiceBase;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaders;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceipts;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceipt;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMag;
use \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNum;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDate;
use \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaders;

class CustomerReceiptService
    extends CustomerReceiptServiceBase
{
    
    /**
     * Method searchDetailedReceiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeaders $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchDetailedReceiptHeadersResponse
     */
    public function searchDetailedReceiptHeaders(SearchDetailedReceiptHeaders $parameters)
    {
        $response = parent::_searchDetailedReceiptHeaders($parameters);
        return $response;
    }
    
    /**
     * Method getReceipts
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceipts $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptsResponse
     */
    public function getReceipts(GetReceipts $parameters)
    {
        $response = parent::_getReceipts($parameters);
        return $response;
    }
    
    /**
     * Method getReceipt
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceipt $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptResponse
     */
    public function getReceipt(GetReceipt $parameters)
    {
        $response = parent::_getReceipt($parameters);
        return $response;
    }
    
    /**
     * Method searchReceiptByMag
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMag $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptByMagResponse
     */
    public function searchReceiptByMag(SearchReceiptByMag $parameters)
    {
        $response = parent::_searchReceiptByMag($parameters);
        return $response;
    }
    
    /**
     * Method getReceiptByNum
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNum $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\GetReceiptByNumResponse
     */
    public function getReceiptByNum(GetReceiptByNum $parameters)
    {
        $response = parent::_getReceiptByNum($parameters);
        return $response;
    }
    
    /**
     * Method searchReceiptHeadersByDate
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDate $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersByDateResponse
     */
    public function searchReceiptHeadersByDate(SearchReceiptHeadersByDate $parameters)
    {
        $response = parent::_searchReceiptHeadersByDate($parameters);
        return $response;
    }
    
    /**
     * Method searchReceiptHeaders
     * 
     * @param \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeaders $parameters Value of parameters
     * 
     * @return \Adeo\Sirius\CustomerReceiptService\Type\SearchReceiptHeadersResponse
     */
    public function searchReceiptHeaders(SearchReceiptHeaders $parameters)
    {
        $response = parent::_searchReceiptHeaders($parameters);
        return $response;
    }
}
