<?php
/**
 * Model for QuotationRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type;

use \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest as QuotationRequestBase;

class QuotationRequest
    extends QuotationRequestBase
{
}
