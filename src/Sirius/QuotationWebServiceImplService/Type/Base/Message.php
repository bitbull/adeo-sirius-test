<?php
/**
 * Parent model for Message
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Message
{
    
    /**
     * Code
     * 
     * @var string
     */
    public $code;
    
    /**
     * Channel
     * 
     * @var string
     */
    public $channel;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * Get code
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param string $value code
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get channel
     * 
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }
    
    /**
     * Set channel
     * 
     * @param string $value channel
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message
     */
    public function setChannel($value)
    {
        $this->channel = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
}
