<?php
/**
 * Parent model for TechnicalContext
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class TechnicalContext
{
    
    /**
     * Application
     * 
     * @var string
     */
    public $application;
    
    /**
     * User
     * 
     * @var string
     */
    public $user;
    
    /**
     * Language
     * 
     * @var string
     */
    public $language;
    
    /**
     * Get application
     * 
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set application
     * 
     * @param string $value application
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public function setApplication($value)
    {
        $this->application = $value;
        return $this;
    }
    
    /**
     * Get user
     * 
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set user
     * 
     * @param string $value user
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public function setUser($value)
    {
        $this->user = $value;
        return $this;
    }
    
    /**
     * Get language
     * 
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * Set language
     * 
     * @param string $value language
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }
}
