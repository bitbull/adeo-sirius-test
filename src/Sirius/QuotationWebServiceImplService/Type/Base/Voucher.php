<?php
/**
 * Parent model for Voucher
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Voucher
{
    
    /**
     * OrderNumber
     * 
     * @var float
     */
    public $orderNumber;
    
    /**
     * RightLabel
     * 
     * @var string
     */
    public $rightLabel;
    
    /**
     * RightCode
     * 
     * @var string
     */
    public $rightCode;
    
    /**
     * RightCliCode
     * 
     * @var string
     */
    public $rightCliCode;
    
    /**
     * Active
     * 
     * @var boolean
     */
    public $active;
    
    /**
     * ActivationDate
     * 
     * @var string
     */
    public $activationDate;
    
    /**
     * ValidityEndDate
     * 
     * @var string
     */
    public $validityEndDate;
    
    /**
     * EndUseDate
     * 
     * @var string
     */
    public $endUseDate;
    
    /**
     * UsageDuration
     * 
     * @var string
     */
    public $usageDuration;
    
    /**
     * BurnValue
     * 
     * @var int
     */
    public $burnValue;
    
    /**
     * RewardType
     * 
     * @var integer
     */
    public $rewardType;
    
    /**
     * Value
     * 
     * @var float
     */
    public $value;
    
    /**
     * GapCode
     * 
     * @var int
     */
    public $gapCode;
    
    /**
     * Get orderNumber
     * 
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
    
    /**
     * Set orderNumber
     * 
     * @param float $value orderNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setOrderNumber($value)
    {
        $this->orderNumber = $value;
        return $this;
    }
    
    /**
     * Get rightLabel
     * 
     * @return string
     */
    public function getRightLabel()
    {
        return $this->rightLabel;
    }
    
    /**
     * Set rightLabel
     * 
     * @param string $value rightLabel
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setRightLabel($value)
    {
        $this->rightLabel = $value;
        return $this;
    }
    
    /**
     * Get rightCode
     * 
     * @return string
     */
    public function getRightCode()
    {
        return $this->rightCode;
    }
    
    /**
     * Set rightCode
     * 
     * @param string $value rightCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setRightCode($value)
    {
        $this->rightCode = $value;
        return $this;
    }
    
    /**
     * Get rightCliCode
     * 
     * @return string
     */
    public function getRightCliCode()
    {
        return $this->rightCliCode;
    }
    
    /**
     * Set rightCliCode
     * 
     * @param string $value rightCliCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setRightCliCode($value)
    {
        $this->rightCliCode = $value;
        return $this;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set active
     * 
     * @param boolean $value active
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }
    
    /**
     * Get activationDate
     * 
     * @return string
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }
    
    /**
     * Set activationDate
     * 
     * @param string $value activationDate
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setActivationDate($value)
    {
        $this->activationDate = $value;
        return $this;
    }
    
    /**
     * Get validityEndDate
     * 
     * @return string
     */
    public function getValidityEndDate()
    {
        return $this->validityEndDate;
    }
    
    /**
     * Set validityEndDate
     * 
     * @param string $value validityEndDate
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setValidityEndDate($value)
    {
        $this->validityEndDate = $value;
        return $this;
    }
    
    /**
     * Get endUseDate
     * 
     * @return string
     */
    public function getEndUseDate()
    {
        return $this->endUseDate;
    }
    
    /**
     * Set endUseDate
     * 
     * @param string $value endUseDate
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setEndUseDate($value)
    {
        $this->endUseDate = $value;
        return $this;
    }
    
    /**
     * Get usageDuration
     * 
     * @return string
     */
    public function getUsageDuration()
    {
        return $this->usageDuration;
    }
    
    /**
     * Set usageDuration
     * 
     * @param string $value usageDuration
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setUsageDuration($value)
    {
        $this->usageDuration = $value;
        return $this;
    }
    
    /**
     * Get burnValue
     * 
     * @return int
     */
    public function getBurnValue()
    {
        return $this->burnValue;
    }
    
    /**
     * Set burnValue
     * 
     * @param int $value burnValue
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setBurnValue($value)
    {
        $this->burnValue = $value;
        return $this;
    }
    
    /**
     * Get rewardType
     * 
     * @return integer
     */
    public function getRewardType()
    {
        return $this->rewardType;
    }
    
    /**
     * Set rewardType
     * 
     * @param integer $value rewardType
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setRewardType($value)
    {
        $this->rewardType = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param float $value value
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get gapCode
     * 
     * @return int
     */
    public function getGapCode()
    {
        return $this->gapCode;
    }
    
    /**
     * Set gapCode
     * 
     * @param int $value gapCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher
     */
    public function setGapCode($value)
    {
        $this->gapCode = $value;
        return $this;
    }
}
