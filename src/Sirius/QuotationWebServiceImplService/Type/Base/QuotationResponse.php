<?php
/**
 * Parent model for QuotationResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class QuotationResponse
{
    
    /**
     * Version
     * 
     * @var string
     */
    public $version;
    
    /**
     * TransactionId
     * 
     * @var string
     */
    public $transactionId;
    
    /**
     * TechnicalContext
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public $technicalContext;
    
    /**
     * BusinessContext
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public $businessContext;
    
    /**
     * Error
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Error
     */
    public $error;
    
    /**
     * QuotationNumber
     * 
     * @var float
     */
    public $quotationNumber;
    
    /**
     * LoyaltyStatus
     * 
     * @var integer
     */
    public $loyaltyStatus;
    
    /**
     * PercentageDiscountableAmount
     * 
     * @var float
     */
    public $percentageDiscountableAmount;
    
    /**
     * UsableVoucher collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[]
     */
    public $usableVoucher = array();
    
    /**
     * UsedVoucher collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[]
     */
    public $usedVoucher = array();
    
    /**
     * AddedPayment collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[]
     */
    public $addedPayment = array();
    
    /**
     * AddedSale collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public $addedSale = array();
    
    /**
     * FinalSale collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public $finalSale = array();
    
    /**
     * FinalOrder collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[]
     */
    public $finalOrder = array();
    
    /**
     * DiscountsSummary collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary[]
     */
    public $discountsSummary = array();
    
    /**
     * Message collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message[]
     */
    public $message = array();
    
    /**
     * Get version
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Set version
     * 
     * @param string $value version
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setVersion($value)
    {
        $this->version = $value;
        return $this;
    }
    
    /**
     * Get transactionId
     * 
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    
    /**
     * Set transactionId
     * 
     * @param string $value transactionId
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setTransactionId($value)
    {
        $this->transactionId = $value;
        return $this;
    }
    
    /**
     * Get technicalContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public function getTechnicalContext()
    {
        return $this->technicalContext;
    }
    
    /**
     * Set technicalContext
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext $value technicalContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setTechnicalContext(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext $value)
    {
        $this->technicalContext = $value;
        return $this;
    }
    
    /**
     * Get businessContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function getBusinessContext()
    {
        return $this->businessContext;
    }
    
    /**
     * Set businessContext
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext $value businessContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setBusinessContext(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext $value)
    {
        $this->businessContext = $value;
        return $this;
    }
    
    /**
     * Get error
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Error
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * Set error
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Error $value error
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setError(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Error $value)
    {
        $this->error = $value;
        return $this;
    }
    
    /**
     * Get quotationNumber
     * 
     * @return float
     */
    public function getQuotationNumber()
    {
        return $this->quotationNumber;
    }
    
    /**
     * Set quotationNumber
     * 
     * @param float $value quotationNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setQuotationNumber($value)
    {
        $this->quotationNumber = $value;
        return $this;
    }
    
    /**
     * Get loyaltyStatus
     * 
     * @return integer
     */
    public function getLoyaltyStatus()
    {
        return $this->loyaltyStatus;
    }
    
    /**
     * Set loyaltyStatus
     * 
     * @param integer $value loyaltyStatus
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setLoyaltyStatus($value)
    {
        $this->loyaltyStatus = $value;
        return $this;
    }
    
    /**
     * Get percentageDiscountableAmount
     * 
     * @return float
     */
    public function getPercentageDiscountableAmount()
    {
        return $this->percentageDiscountableAmount;
    }
    
    /**
     * Set percentageDiscountableAmount
     * 
     * @param float $value percentageDiscountableAmount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setPercentageDiscountableAmount($value)
    {
        $this->percentageDiscountableAmount = $value;
        return $this;
    }
    
    /**
     * Get usableVoucher
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[]
     */
    public function getUsableVoucher()
    {
        return $this->usableVoucher;
    }
    
    /**
     * Add element on usableVoucher collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[] $value usableVoucher
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setUsableVoucher($value = null)
    {
        $this->usableVoucher[] = $value;
        return $this;
    }
    
    /**
     * Get usedVoucher
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[]
     */
    public function getUsedVoucher()
    {
        return $this->usedVoucher;
    }
    
    /**
     * Add element on usedVoucher collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Voucher[] $value usedVoucher
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setUsedVoucher($value = null)
    {
        $this->usedVoucher[] = $value;
        return $this;
    }
    
    /**
     * Get addedPayment
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[]
     */
    public function getAddedPayment()
    {
        return $this->addedPayment;
    }
    
    /**
     * Add element on addedPayment collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[] $value addedPayment
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setAddedPayment($value = null)
    {
        $this->addedPayment[] = $value;
        return $this;
    }
    
    /**
     * Get addedSale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public function getAddedSale()
    {
        return $this->addedSale;
    }
    
    /**
     * Add element on addedSale collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[] $value addedSale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setAddedSale($value = null)
    {
        $this->addedSale[] = $value;
        return $this;
    }
    
    /**
     * Get finalSale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public function getFinalSale()
    {
        return $this->finalSale;
    }
    
    /**
     * Add element on finalSale collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[] $value finalSale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setFinalSale($value = null)
    {
        $this->finalSale[] = $value;
        return $this;
    }
    
    /**
     * Get finalOrder
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[]
     */
    public function getFinalOrder()
    {
        return $this->finalOrder;
    }
    
    /**
     * Add element on finalOrder collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[] $value finalOrder
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setFinalOrder($value = null)
    {
        $this->finalOrder[] = $value;
        return $this;
    }
    
    /**
     * Get discountsSummary
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary[]
     */
    public function getDiscountsSummary()
    {
        return $this->discountsSummary;
    }
    
    /**
     * Add element on discountsSummary collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary[] $value discountsSummary
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setDiscountsSummary($value = null)
    {
        $this->discountsSummary[] = $value;
        return $this;
    }
    
    /**
     * Get message
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message[]
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Add element on message collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Message[] $value message
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationResponse
     */
    public function setMessage($value = null)
    {
        $this->message[] = $value;
        return $this;
    }
}
