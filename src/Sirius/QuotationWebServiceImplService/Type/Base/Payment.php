<?php
/**
 * Parent model for Payment
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Payment
{
    
    /**
     * Type
     * 
     * @var int
     */
    public $type;
    
    /**
     * Amount
     * 
     * @var float
     */
    public $amount;
    
    /**
     * OrderNumber
     * 
     * @var float
     */
    public $orderNumber;
    
    /**
     * Get type
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param int $value type
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get amount
     * 
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set amount
     * 
     * @param float $value amount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment
     */
    public function setAmount($value)
    {
        $this->amount = $value;
        return $this;
    }
    
    /**
     * Get orderNumber
     * 
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
    
    /**
     * Set orderNumber
     * 
     * @param float $value orderNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment
     */
    public function setOrderNumber($value)
    {
        $this->orderNumber = $value;
        return $this;
    }
}
