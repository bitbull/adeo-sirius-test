<?php
/**
 * Parent model for QuotationRequest
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class QuotationRequest
{
    
    /**
     * TransactionId
     * 
     * @var string
     */
    public $transactionId;
    
    /**
     * TechnicalContext
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public $technicalContext;
    
    /**
     * BusinessContext
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public $businessContext;
    
    /**
     * CustomerNumber
     * 
     * @var float
     */
    public $customerNumber;
    
    /**
     * Order collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader[]
     */
    public $order = array();
    
    /**
     * ManualOrder collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[]
     */
    public $manualOrder = array();
    
    /**
     * Sale collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public $sale = array();
    
    /**
     * Payment collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[]
     */
    public $payment = array();
    
    /**
     * VoucherCode collection
     * 
     * @var string[]
     */
    public $voucherCode = array();
    
    /**
     * Commit
     * 
     * @var boolean
     */
    public $commit;
    
    /**
     * Get transactionId
     * 
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    
    /**
     * Set transactionId
     * 
     * @param string $value transactionId
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setTransactionId($value)
    {
        $this->transactionId = $value;
        return $this;
    }
    
    /**
     * Get technicalContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext
     */
    public function getTechnicalContext()
    {
        return $this->technicalContext;
    }
    
    /**
     * Set technicalContext
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext $value technicalContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setTechnicalContext(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\TechnicalContext $value)
    {
        $this->technicalContext = $value;
        return $this;
    }
    
    /**
     * Get businessContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function getBusinessContext()
    {
        return $this->businessContext;
    }
    
    /**
     * Set businessContext
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext $value businessContext
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setBusinessContext(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext $value)
    {
        $this->businessContext = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return float
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param float $value customerNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get order
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader[]
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Add element on order collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader[] $value order
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setOrder($value = null)
    {
        $this->order[] = $value;
        return $this;
    }
    
    /**
     * Get manualOrder
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[]
     */
    public function getManualOrder()
    {
        return $this->manualOrder;
    }
    
    /**
     * Add element on manualOrder collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order[] $value manualOrder
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setManualOrder($value = null)
    {
        $this->manualOrder[] = $value;
        return $this;
    }
    
    /**
     * Get sale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public function getSale()
    {
        return $this->sale;
    }
    
    /**
     * Add element on sale collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[] $value sale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setSale($value = null)
    {
        $this->sale[] = $value;
        return $this;
    }
    
    /**
     * Get payment
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[]
     */
    public function getPayment()
    {
        return $this->payment;
    }
    
    /**
     * Add element on payment collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Payment[] $value payment
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setPayment($value = null)
    {
        $this->payment[] = $value;
        return $this;
    }
    
    /**
     * Get voucherCode
     * 
     * @return string[]
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }
    
    /**
     * Add element on voucherCode collection
     * 
     * @param string[] $value voucherCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setVoucherCode($value = null)
    {
        $this->voucherCode[] = $value;
        return $this;
    }
    
    /**
     * Get commit
     * 
     * @return boolean
     */
    public function getCommit()
    {
        return $this->commit;
    }
    
    /**
     * Set commit
     * 
     * @param boolean $value commit
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\QuotationRequest
     */
    public function setCommit($value)
    {
        $this->commit = $value;
        return $this;
    }
}
