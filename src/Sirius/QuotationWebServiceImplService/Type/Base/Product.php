<?php
/**
 * Parent model for Product
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Product
{
    
    /**
     * LineNumber
     * 
     * @var int
     */
    public $lineNumber;
    
    /**
     * LineType
     * 
     * @var integer
     */
    public $lineType;
    
    /**
     * ProductCode
     * 
     * @var int
     */
    public $productCode;
    
    /**
     * Quantity
     * 
     * @var float
     */
    public $quantity;
    
    /**
     * UnitaryTaxIncludedAmount
     * 
     * @var float
     */
    public $unitaryTaxIncludedAmount;
    
    /**
     * Discount collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[]
     */
    public $discount = array();
    
    /**
     * Get lineNumber
     * 
     * @return int
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }
    
    /**
     * Set lineNumber
     * 
     * @param int $value lineNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setLineNumber($value)
    {
        $this->lineNumber = $value;
        return $this;
    }
    
    /**
     * Get lineType
     * 
     * @return integer
     */
    public function getLineType()
    {
        return $this->lineType;
    }
    
    /**
     * Set lineType
     * 
     * @param integer $value lineType
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setLineType($value)
    {
        $this->lineType = $value;
        return $this;
    }
    
    /**
     * Get productCode
     * 
     * @return int
     */
    public function getProductCode()
    {
        return $this->productCode;
    }
    
    /**
     * Set productCode
     * 
     * @param int $value productCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setProductCode($value)
    {
        $this->productCode = $value;
        return $this;
    }
    
    /**
     * Get quantity
     * 
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Set quantity
     * 
     * @param float $value quantity
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setQuantity($value)
    {
        $this->quantity = $value;
        return $this;
    }
    
    /**
     * Get unitaryTaxIncludedAmount
     * 
     * @return float
     */
    public function getUnitaryTaxIncludedAmount()
    {
        return $this->unitaryTaxIncludedAmount;
    }
    
    /**
     * Set unitaryTaxIncludedAmount
     * 
     * @param float $value unitaryTaxIncludedAmount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setUnitaryTaxIncludedAmount($value)
    {
        $this->unitaryTaxIncludedAmount = $value;
        return $this;
    }
    
    /**
     * Get discount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[]
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * Add element on discount collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[] $value discount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product
     */
    public function setDiscount($value = null)
    {
        $this->discount[] = $value;
        return $this;
    }
}
