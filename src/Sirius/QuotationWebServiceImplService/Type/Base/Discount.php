<?php
/**
 * Parent model for Discount
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Discount
{
    
    /**
     * Type
     * 
     * @var integer
     */
    public $type;
    
    /**
     * GapCode
     * 
     * @var int
     */
    public $gapCode;
    
    /**
     * Amount
     * 
     * @var float
     */
    public $amount;
    
    /**
     * Percent
     * 
     * @var float
     */
    public $percent;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * Label
     * 
     * @var string
     */
    public $label;
    
    /**
     * Source
     * 
     * @var int
     */
    public $source;
    
    /**
     * Get type
     * 
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type
     * 
     * @param integer $value type
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }
    
    /**
     * Get gapCode
     * 
     * @return int
     */
    public function getGapCode()
    {
        return $this->gapCode;
    }
    
    /**
     * Set gapCode
     * 
     * @param int $value gapCode
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setGapCode($value)
    {
        $this->gapCode = $value;
        return $this;
    }
    
    /**
     * Get amount
     * 
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set amount
     * 
     * @param float $value amount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setAmount($value)
    {
        $this->amount = $value;
        return $this;
    }
    
    /**
     * Get percent
     * 
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }
    
    /**
     * Set percent
     * 
     * @param float $value percent
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setPercent($value)
    {
        $this->percent = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get label
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set label
     * 
     * @param string $value label
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setLabel($value)
    {
        $this->label = $value;
        return $this;
    }
    
    /**
     * Get source
     * 
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set source
     * 
     * @param int $value source
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount
     */
    public function setSource($value)
    {
        $this->source = $value;
        return $this;
    }
}
