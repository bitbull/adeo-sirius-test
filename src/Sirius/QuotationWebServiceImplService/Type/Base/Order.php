<?php
/**
 * Parent model for Order
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class Order
{
    
    /**
     * Header
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public $header;
    
    /**
     * Detail collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public $detail = array();
    
    /**
     * Get header
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public function getHeader()
    {
        return $this->header;
    }
    
    /**
     * Set header
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader $value header
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order
     */
    public function setHeader(\Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader $value)
    {
        $this->header = $value;
        return $this;
    }
    
    /**
     * Get detail
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[]
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Add element on detail collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product[] $value detail
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Order
     */
    public function setDetail($value)
    {
        $this->detail[] = $value;
        return $this;
    }
}
