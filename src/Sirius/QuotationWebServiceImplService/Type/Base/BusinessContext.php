<?php
/**
 * Parent model for BusinessContext
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class BusinessContext
{
    
    /**
     * BusinessUnit
     * 
     * @var int
     */
    public $businessUnit;
    
    /**
     * PointOfSale
     * 
     * @var int
     */
    public $pointOfSale;
    
    /**
     * Channel
     * 
     * @var string
     */
    public $channel;
    
    /**
     * CashDesk
     * 
     * @var int
     */
    public $cashDesk;
    
    /**
     * Cashier
     * 
     * @var int
     */
    public $cashier;
    
    /**
     * Get businessUnit
     * 
     * @return int
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }
    
    /**
     * Set businessUnit
     * 
     * @param int $value businessUnit
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function setBusinessUnit($value)
    {
        $this->businessUnit = $value;
        return $this;
    }
    
    /**
     * Get pointOfSale
     * 
     * @return int
     */
    public function getPointOfSale()
    {
        return $this->pointOfSale;
    }
    
    /**
     * Set pointOfSale
     * 
     * @param int $value pointOfSale
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function setPointOfSale($value)
    {
        $this->pointOfSale = $value;
        return $this;
    }
    
    /**
     * Get channel
     * 
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }
    
    /**
     * Set channel
     * 
     * @param string $value channel
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function setChannel($value)
    {
        $this->channel = $value;
        return $this;
    }
    
    /**
     * Get cashDesk
     * 
     * @return int
     */
    public function getCashDesk()
    {
        return $this->cashDesk;
    }
    
    /**
     * Set cashDesk
     * 
     * @param int $value cashDesk
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function setCashDesk($value)
    {
        $this->cashDesk = $value;
        return $this;
    }
    
    /**
     * Get cashier
     * 
     * @return int
     */
    public function getCashier()
    {
        return $this->cashier;
    }
    
    /**
     * Set cashier
     * 
     * @param int $value cashier
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\BusinessContext
     */
    public function setCashier($value)
    {
        $this->cashier = $value;
        return $this;
    }
}
