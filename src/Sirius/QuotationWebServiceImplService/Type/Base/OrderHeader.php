<?php
/**
 * Parent model for OrderHeader
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class OrderHeader
{
    
    /**
     * Number
     * 
     * @var float
     */
    public $number;
    
    /**
     * QuotationNumber
     * 
     * @var float
     */
    public $quotationNumber;
    
    /**
     * DepositType
     * 
     * @var integer
     */
    public $depositType;
    
    /**
     * DepositValue
     * 
     * @var float
     */
    public $depositValue;
    
    /**
     * Get number
     * 
     * @return float
     */
    public function getNumber()
    {
        return $this->number;
    }
    
    /**
     * Set number
     * 
     * @param float $value number
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public function setNumber($value)
    {
        $this->number = $value;
        return $this;
    }
    
    /**
     * Get quotationNumber
     * 
     * @return float
     */
    public function getQuotationNumber()
    {
        return $this->quotationNumber;
    }
    
    /**
     * Set quotationNumber
     * 
     * @param float $value quotationNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public function setQuotationNumber($value)
    {
        $this->quotationNumber = $value;
        return $this;
    }
    
    /**
     * Get depositType
     * 
     * @return integer
     */
    public function getDepositType()
    {
        return $this->depositType;
    }
    
    /**
     * Set depositType
     * 
     * @param integer $value depositType
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public function setDepositType($value)
    {
        $this->depositType = $value;
        return $this;
    }
    
    /**
     * Get depositValue
     * 
     * @return float
     */
    public function getDepositValue()
    {
        return $this->depositValue;
    }
    
    /**
     * Set depositValue
     * 
     * @param float $value depositValue
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\OrderHeader
     */
    public function setDepositValue($value)
    {
        $this->depositValue = $value;
        return $this;
    }
}
