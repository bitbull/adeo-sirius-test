<?php
/**
 * Parent model for DiscountsSummary
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type\Base;

abstract class DiscountsSummary
{
    
    /**
     * TransactionType
     * 
     * @var integer
     */
    public $transactionType;
    
    /**
     * OrderNumber
     * 
     * @var float
     */
    public $orderNumber;
    
    /**
     * Discount collection
     * 
     * @var \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[]
     */
    public $discount = array();
    
    /**
     * Get transactionType
     * 
     * @return integer
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }
    
    /**
     * Set transactionType
     * 
     * @param integer $value transactionType
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary
     */
    public function setTransactionType($value)
    {
        $this->transactionType = $value;
        return $this;
    }
    
    /**
     * Get orderNumber
     * 
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
    
    /**
     * Set orderNumber
     * 
     * @param float $value orderNumber
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary
     */
    public function setOrderNumber($value)
    {
        $this->orderNumber = $value;
        return $this;
    }
    
    /**
     * Get discount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[]
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * Add element on discount collection
     * 
     * @param \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Discount[] $value discount
     * 
     * @return \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\DiscountsSummary
     */
    public function setDiscount($value = null)
    {
        $this->discount[] = $value;
        return $this;
    }
}
