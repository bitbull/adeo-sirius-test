<?php
/**
 * Model for Product
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\QuotationWebServiceImplService\Type;

use \Adeo\Sirius\QuotationWebServiceImplService\Type\Base\Product as ProductBase;

class Product
    extends ProductBase
{
}
