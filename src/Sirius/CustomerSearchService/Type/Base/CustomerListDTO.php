<?php
/**
 * Parent model for CustomerListDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class CustomerListDTO
{
    
    /**
     * Customers collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerMainInformationDTO[]
     */
    public $customers = array();
    
    /**
     * MoreResultsAvailable
     * 
     * @var boolean
     */
    public $moreResultsAvailable;
    
    /**
     * DegradedMode
     * 
     * @var boolean
     */
    public $degradedMode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get customers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerMainInformationDTO[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }
    
    /**
     * Add element on customers collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerMainInformationDTO[] $value customers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public function setCustomers($value = null)
    {
        $this->customers[] = $value;
        return $this;
    }
    
    /**
     * Get moreResultsAvailable
     * 
     * @return boolean
     */
    public function getMoreResultsAvailable()
    {
        return $this->moreResultsAvailable;
    }
    
    /**
     * Set moreResultsAvailable
     * 
     * @param boolean $value moreResultsAvailable
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public function setMoreResultsAvailable($value)
    {
        $this->moreResultsAvailable = $value;
        return $this;
    }
    
    /**
     * Get degradedMode
     * 
     * @return boolean
     */
    public function getDegradedMode()
    {
        return $this->degradedMode;
    }
    
    /**
     * Set degradedMode
     * 
     * @param boolean $value degradedMode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public function setDegradedMode($value)
    {
        $this->degradedMode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
