<?php
/**
 * Parent model for AddressMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class AddressMainInformationDTO
{
    
    /**
     * Comment
     * 
     * @var string
     */
    public $comment;
    
    /**
     * Identifier
     * 
     * @var int
     */
    public $identifier;
    
    /**
     * Line1
     * 
     * @var string
     */
    public $line1;
    
    /**
     * Line2
     * 
     * @var string
     */
    public $line2;
    
    /**
     * Line3
     * 
     * @var string
     */
    public $line3;
    
    /**
     * Line4
     * 
     * @var string
     */
    public $line4;
    
    /**
     * PostalCode
     * 
     * @var string
     */
    public $postalCode;
    
    /**
     * City
     * 
     * @var string
     */
    public $city;
    
    /**
     * Province
     * 
     * @var string
     */
    public $province;
    
    /**
     * CountryCode
     * 
     * @var string
     */
    public $countryCode;
    
    /**
     * CountryLabel
     * 
     * @var string
     */
    public $countryLabel;
    
    /**
     * Goneaway
     * 
     * @var boolean
     */
    public $goneaway;
    
    /**
     * MainAddress
     * 
     * @var boolean
     */
    public $mainAddress;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get comment
     * 
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Set comment
     * 
     * @param string $value comment
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setComment($value)
    {
        $this->comment = $value;
        return $this;
    }
    
    /**
     * Get identifier
     * 
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    /**
     * Set identifier
     * 
     * @param int $value identifier
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setIdentifier($value)
    {
        $this->identifier = $value;
        return $this;
    }
    
    /**
     * Get line1
     * 
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }
    
    /**
     * Set line1
     * 
     * @param string $value line1
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setLine1($value)
    {
        $this->line1 = $value;
        return $this;
    }
    
    /**
     * Get line2
     * 
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }
    
    /**
     * Set line2
     * 
     * @param string $value line2
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setLine2($value)
    {
        $this->line2 = $value;
        return $this;
    }
    
    /**
     * Get line3
     * 
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }
    
    /**
     * Set line3
     * 
     * @param string $value line3
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setLine3($value)
    {
        $this->line3 = $value;
        return $this;
    }
    
    /**
     * Get line4
     * 
     * @return string
     */
    public function getLine4()
    {
        return $this->line4;
    }
    
    /**
     * Set line4
     * 
     * @param string $value line4
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setLine4($value)
    {
        $this->line4 = $value;
        return $this;
    }
    
    /**
     * Get postalCode
     * 
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * Set postalCode
     * 
     * @param string $value postalCode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setPostalCode($value)
    {
        $this->postalCode = $value;
        return $this;
    }
    
    /**
     * Get city
     * 
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Set city
     * 
     * @param string $value city
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setCity($value)
    {
        $this->city = $value;
        return $this;
    }
    
    /**
     * Get province
     * 
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    /**
     * Set province
     * 
     * @param string $value province
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setProvince($value)
    {
        $this->province = $value;
        return $this;
    }
    
    /**
     * Get countryCode
     * 
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    
    /**
     * Set countryCode
     * 
     * @param string $value countryCode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setCountryCode($value)
    {
        $this->countryCode = $value;
        return $this;
    }
    
    /**
     * Get countryLabel
     * 
     * @return string
     */
    public function getCountryLabel()
    {
        return $this->countryLabel;
    }
    
    /**
     * Set countryLabel
     * 
     * @param string $value countryLabel
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setCountryLabel($value)
    {
        $this->countryLabel = $value;
        return $this;
    }
    
    /**
     * Get goneaway
     * 
     * @return boolean
     */
    public function getGoneaway()
    {
        return $this->goneaway;
    }
    
    /**
     * Set goneaway
     * 
     * @param boolean $value goneaway
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setGoneaway($value)
    {
        $this->goneaway = $value;
        return $this;
    }
    
    /**
     * Get mainAddress
     * 
     * @return boolean
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }
    
    /**
     * Set mainAddress
     * 
     * @param boolean $value mainAddress
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setMainAddress($value)
    {
        $this->mainAddress = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
