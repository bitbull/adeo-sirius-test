<?php
/**
 * Parent model for SearchCustomersResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class SearchCustomersResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\SearchCustomersResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerSearchService\Type\Base\CustomerListDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
