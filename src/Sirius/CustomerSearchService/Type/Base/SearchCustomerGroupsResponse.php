<?php
/**
 * Parent model for SearchCustomerGroupsResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class SearchCustomerGroupsResponse
{
    
    /**
     * Return
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public $return;
    
    /**
     * Get return
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public function getReturn()
    {
        return $this->return;
    }
    
    /**
     * Set return
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO $value return
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\SearchCustomerGroupsResponse
     */
    public function setReturn(\Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO $value)
    {
        $this->return = $value;
        return $this;
    }
}
