<?php
/**
 * Parent model for CustomerGrouplistDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class CustomerGrouplistDTO
{
    
    /**
     * Groups collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO[]
     */
    public $groups = array();
    
    /**
     * MoreResultsAvailable
     * 
     * @var boolean
     */
    public $moreResultsAvailable;
    
    /**
     * DegradedMode
     * 
     * @var boolean
     */
    public $degradedMode;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get groups
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO[]
     */
    public function getGroups()
    {
        return $this->groups;
    }
    
    /**
     * Add element on groups collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO[] $value groups
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public function setGroups($value = null)
    {
        $this->groups[] = $value;
        return $this;
    }
    
    /**
     * Get moreResultsAvailable
     * 
     * @return boolean
     */
    public function getMoreResultsAvailable()
    {
        return $this->moreResultsAvailable;
    }
    
    /**
     * Set moreResultsAvailable
     * 
     * @param boolean $value moreResultsAvailable
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public function setMoreResultsAvailable($value)
    {
        $this->moreResultsAvailable = $value;
        return $this;
    }
    
    /**
     * Get degradedMode
     * 
     * @return boolean
     */
    public function getDegradedMode()
    {
        return $this->degradedMode;
    }
    
    /**
     * Set degradedMode
     * 
     * @param boolean $value degradedMode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public function setDegradedMode($value)
    {
        $this->degradedMode = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGrouplistDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
