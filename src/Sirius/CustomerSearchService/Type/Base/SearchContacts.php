<?php
/**
 * Parent model for SearchContacts
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class SearchContacts
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerSearchCriteriaDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerSearchCriteriaDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerSearchCriteriaDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\SearchContacts
     */
    public function setArg0(\Adeo\Sirius\CustomerSearchService\Type\Base\CustomerSearchCriteriaDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
