<?php
/**
 * Parent model for LegalEntityMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class LegalEntityMainInformationDTO
{
    
    /**
     * MatchesCriteria
     * 
     * @var boolean
     */
    public $matchesCriteria;
    
    /**
     * CompanyName
     * 
     * @var string
     */
    public $companyName;
    
    /**
     * MasterNumber
     * 
     * @var int
     */
    public $masterNumber;
    
    /**
     * Active
     * 
     * @var boolean
     */
    public $active;
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $classifications = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get matchesCriteria
     * 
     * @return boolean
     */
    public function getMatchesCriteria()
    {
        return $this->matchesCriteria;
    }
    
    /**
     * Set matchesCriteria
     * 
     * @param boolean $value matchesCriteria
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setMatchesCriteria($value)
    {
        $this->matchesCriteria = $value;
        return $this;
    }
    
    /**
     * Get companyName
     * 
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    
    /**
     * Set companyName
     * 
     * @param string $value companyName
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setCompanyName($value)
    {
        $this->companyName = $value;
        return $this;
    }
    
    /**
     * Get masterNumber
     * 
     * @return int
     */
    public function getMasterNumber()
    {
        return $this->masterNumber;
    }
    
    /**
     * Set masterNumber
     * 
     * @param int $value masterNumber
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setMasterNumber($value)
    {
        $this->masterNumber = $value;
        return $this;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set active
     * 
     * @param boolean $value active
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
