<?php
/**
 * Parent model for NaturalPersonMainInformationDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class NaturalPersonMainInformationDTO
{
    
    /**
     * Title2
     * 
     * @var string
     */
    public $title2;
    
    /**
     * ExternalIdentifiers collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public $externalIdentifiers = array();
    
    /**
     * Active
     * 
     * @var boolean
     */
    public $active;
    
    /**
     * MatchesCriteria
     * 
     * @var boolean
     */
    public $matchesCriteria;
    
    /**
     * TitleCode
     * 
     * @var int
     */
    public $titleCode;
    
    /**
     * TitleLabel
     * 
     * @var string
     */
    public $titleLabel;
    
    /**
     * FirstName
     * 
     * @var string
     */
    public $firstName;
    
    /**
     * Name
     * 
     * @var string
     */
    public $name;
    
    /**
     * OtherName
     * 
     * @var string
     */
    public $otherName;
    
    /**
     * CustomerTypeCode
     * 
     * @var int
     */
    public $customerTypeCode;
    
    /**
     * CustomerTypeLabel
     * 
     * @var string
     */
    public $customerTypeLabel;
    
    /**
     * CustomerNumber
     * 
     * @var int
     */
    public $customerNumber;
    
    /**
     * MasterNumber
     * 
     * @var int
     */
    public $masterNumber;
    
    /**
     * Deletable
     * 
     * @var int
     */
    public $deletable;
    
    /**
     * MainContact
     * 
     * @var boolean
     */
    public $mainContact;
    
    /**
     * Segmentations collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\SegmentationInformationOutDTO[]
     */
    public $segmentations = array();
    
    /**
     * Classifications collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public $classifications = array();
    
    /**
     * Communications collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CommunicationMainInformationDTO[]
     */
    public $communications = array();
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get title2
     * 
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }
    
    /**
     * Set title2
     * 
     * @param string $value title2
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setTitle2($value)
    {
        $this->title2 = $value;
        return $this;
    }
    
    /**
     * Get externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[]
     */
    public function getExternalIdentifiers()
    {
        return $this->externalIdentifiers;
    }
    
    /**
     * Add element on externalIdentifiers collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\ExternalIdentifierInformationOutDTO[] $value externalIdentifiers
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setExternalIdentifiers($value = null)
    {
        $this->externalIdentifiers[] = $value;
        return $this;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set active
     * 
     * @param boolean $value active
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setActive($value)
    {
        $this->active = $value;
        return $this;
    }
    
    /**
     * Get matchesCriteria
     * 
     * @return boolean
     */
    public function getMatchesCriteria()
    {
        return $this->matchesCriteria;
    }
    
    /**
     * Set matchesCriteria
     * 
     * @param boolean $value matchesCriteria
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setMatchesCriteria($value)
    {
        $this->matchesCriteria = $value;
        return $this;
    }
    
    /**
     * Get titleCode
     * 
     * @return int
     */
    public function getTitleCode()
    {
        return $this->titleCode;
    }
    
    /**
     * Set titleCode
     * 
     * @param int $value titleCode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setTitleCode($value)
    {
        $this->titleCode = $value;
        return $this;
    }
    
    /**
     * Get titleLabel
     * 
     * @return string
     */
    public function getTitleLabel()
    {
        return $this->titleLabel;
    }
    
    /**
     * Set titleLabel
     * 
     * @param string $value titleLabel
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setTitleLabel($value)
    {
        $this->titleLabel = $value;
        return $this;
    }
    
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set firstName
     * 
     * @param string $value firstName
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name
     * 
     * @param string $value name
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
    
    /**
     * Get otherName
     * 
     * @return string
     */
    public function getOtherName()
    {
        return $this->otherName;
    }
    
    /**
     * Set otherName
     * 
     * @param string $value otherName
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setOtherName($value)
    {
        $this->otherName = $value;
        return $this;
    }
    
    /**
     * Get customerTypeCode
     * 
     * @return int
     */
    public function getCustomerTypeCode()
    {
        return $this->customerTypeCode;
    }
    
    /**
     * Set customerTypeCode
     * 
     * @param int $value customerTypeCode
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setCustomerTypeCode($value)
    {
        $this->customerTypeCode = $value;
        return $this;
    }
    
    /**
     * Get customerTypeLabel
     * 
     * @return string
     */
    public function getCustomerTypeLabel()
    {
        return $this->customerTypeLabel;
    }
    
    /**
     * Set customerTypeLabel
     * 
     * @param string $value customerTypeLabel
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setCustomerTypeLabel($value)
    {
        $this->customerTypeLabel = $value;
        return $this;
    }
    
    /**
     * Get customerNumber
     * 
     * @return int
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    
    /**
     * Set customerNumber
     * 
     * @param int $value customerNumber
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = $value;
        return $this;
    }
    
    /**
     * Get masterNumber
     * 
     * @return int
     */
    public function getMasterNumber()
    {
        return $this->masterNumber;
    }
    
    /**
     * Set masterNumber
     * 
     * @param int $value masterNumber
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setMasterNumber($value)
    {
        $this->masterNumber = $value;
        return $this;
    }
    
    /**
     * Get deletable
     * 
     * @return int
     */
    public function getDeletable()
    {
        return $this->deletable;
    }
    
    /**
     * Set deletable
     * 
     * @param int $value deletable
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setDeletable($value)
    {
        $this->deletable = $value;
        return $this;
    }
    
    /**
     * Get mainContact
     * 
     * @return boolean
     */
    public function getMainContact()
    {
        return $this->mainContact;
    }
    
    /**
     * Set mainContact
     * 
     * @param boolean $value mainContact
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setMainContact($value)
    {
        $this->mainContact = $value;
        return $this;
    }
    
    /**
     * Get segmentations
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\SegmentationInformationOutDTO[]
     */
    public function getSegmentations()
    {
        return $this->segmentations;
    }
    
    /**
     * Add element on segmentations collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\SegmentationInformationOutDTO[] $value segmentations
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setSegmentations($value = null)
    {
        $this->segmentations[] = $value;
        return $this;
    }
    
    /**
     * Get classifications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[]
     */
    public function getClassifications()
    {
        return $this->classifications;
    }
    
    /**
     * Add element on classifications collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\ClassificationInformationOutDTO[] $value classifications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setClassifications($value = null)
    {
        $this->classifications[] = $value;
        return $this;
    }
    
    /**
     * Get communications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CommunicationMainInformationDTO[]
     */
    public function getCommunications()
    {
        return $this->communications;
    }
    
    /**
     * Add element on communications collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CommunicationMainInformationDTO[] $value communications
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setCommunications($value = null)
    {
        $this->communications[] = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
