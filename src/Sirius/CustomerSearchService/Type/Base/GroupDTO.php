<?php
/**
 * Parent model for GroupDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class GroupDTO
{
    
    /**
     * NaturalPersons collection
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO[]
     */
    public $naturalPersons = array();
    
    /**
     * LegalEntityMainInformation
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public $legalEntityMainInformation;
    
    /**
     * GroupAddress
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public $groupAddress;
    
    /**
     * HouseholdNumber
     * 
     * @var int
     */
    public $householdNumber;
    
    /**
     * LegalEntityNumber
     * 
     * @var int
     */
    public $legalEntityNumber;
    
    /**
     * Any
     * 
     * @var \SoapVar
     */
    public $any;
    
    /**
     * Get naturalPersons
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO[]
     */
    public function getNaturalPersons()
    {
        return $this->naturalPersons;
    }
    
    /**
     * Add element on naturalPersons collection
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\NaturalPersonMainInformationDTO[] $value naturalPersons
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setNaturalPersons($value = null)
    {
        $this->naturalPersons[] = $value;
        return $this;
    }
    
    /**
     * Get legalEntityMainInformation
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO
     */
    public function getLegalEntityMainInformation()
    {
        return $this->legalEntityMainInformation;
    }
    
    /**
     * Set legalEntityMainInformation
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO $value legalEntityMainInformation
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setLegalEntityMainInformation(\Adeo\Sirius\CustomerSearchService\Type\Base\LegalEntityMainInformationDTO $value)
    {
        $this->legalEntityMainInformation = $value;
        return $this;
    }
    
    /**
     * Get groupAddress
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO
     */
    public function getGroupAddress()
    {
        return $this->groupAddress;
    }
    
    /**
     * Set groupAddress
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO $value groupAddress
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setGroupAddress(\Adeo\Sirius\CustomerSearchService\Type\Base\AddressMainInformationDTO $value)
    {
        $this->groupAddress = $value;
        return $this;
    }
    
    /**
     * Get householdNumber
     * 
     * @return int
     */
    public function getHouseholdNumber()
    {
        return $this->householdNumber;
    }
    
    /**
     * Set householdNumber
     * 
     * @param int $value householdNumber
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setHouseholdNumber($value)
    {
        $this->householdNumber = $value;
        return $this;
    }
    
    /**
     * Get legalEntityNumber
     * 
     * @return int
     */
    public function getLegalEntityNumber()
    {
        return $this->legalEntityNumber;
    }
    
    /**
     * Set legalEntityNumber
     * 
     * @param int $value legalEntityNumber
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setLegalEntityNumber($value)
    {
        $this->legalEntityNumber = $value;
        return $this;
    }
    
    /**
     * Get any
     * 
     * @return \SoapVar
     */
    public function getAny()
    {
        return $this->any;
    }
    
    /**
     * Set any
     * 
     * @param \SoapVar $value any
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\GroupDTO
     */
    public function setAny(\SoapVar $value)
    {
        $this->any = $value;
        return $this;
    }
}
