<?php
/**
 * Parent model for SearchCustomerGroups
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type\Base;

abstract class SearchCustomerGroups
{
    
    /**
     * Arg0
     * 
     * @var \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGroupSearchCriteriaDTO
     */
    public $arg0;
    
    /**
     * Get arg0
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGroupSearchCriteriaDTO
     */
    public function getArg0()
    {
        return $this->arg0;
    }
    
    /**
     * Set arg0
     * 
     * @param \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGroupSearchCriteriaDTO $value arg0
     * 
     * @return \Adeo\Sirius\CustomerSearchService\Type\Base\SearchCustomerGroups
     */
    public function setArg0(\Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGroupSearchCriteriaDTO $value)
    {
        $this->arg0 = $value;
        return $this;
    }
}
