<?php
/**
 * Model for CustomerGroupSearchCriteriaDTO
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\CustomerSearchService\Type;

use \Adeo\Sirius\CustomerSearchService\Type\Base\CustomerGroupSearchCriteriaDTO as CustomerGroupSearchCriteriaDTOBase;

class CustomerGroupSearchCriteriaDTO
    extends customerGroupSearchCriteriaDTOBase
{
}
