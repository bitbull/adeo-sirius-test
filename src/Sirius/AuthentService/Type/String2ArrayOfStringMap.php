<?php
/**
 * Model for String2ArrayOfStringMap
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap as String2ArrayOfStringMapBase;

class String2ArrayOfStringMap
    extends string2ArrayOfStringMapBase
{
}
