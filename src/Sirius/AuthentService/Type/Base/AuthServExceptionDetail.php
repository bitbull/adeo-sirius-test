<?php
/**
 * Parent model for AuthServExceptionDetail
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class AuthServExceptionDetail
{
    
    /**
     * Code
     * 
     * @var int
     */
    public $code;
    
    /**
     * DetailMessage
     * 
     * @var string
     */
    public $detailMessage = null;
    
    /**
     * Get code
     * 
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code
     * 
     * @param int $value code
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AuthServExceptionDetail
     */
    public function setCode($value)
    {
        $this->code = $value;
        return $this;
    }
    
    /**
     * Get detailMessage
     * 
     * @return string
     */
    public function getDetailMessage()
    {
        return $this->detailMessage;
    }
    
    /**
     * Set detailMessage
     * 
     * @param string $value detailMessage
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AuthServExceptionDetail
     */
    public function setDetailMessage($value = null)
    {
        $this->detailMessage = $value;
        return $this;
    }
}
