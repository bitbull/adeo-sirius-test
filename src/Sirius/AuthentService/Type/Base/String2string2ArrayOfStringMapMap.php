<?php
/**
 * Parent model for String2string2ArrayOfStringMapMap
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class String2string2ArrayOfStringMapMap
{
    
    /**
     * Entry collection
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\[]
     */
    public $entry = array();
    
    /**
     * Key
     * 
     * @var string
     */
    public $key;
    
    /**
     * Value
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap
     */
    public $value;
    
    /**
     * Get entry
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\[]
     */
    public function getEntry()
    {
        return $this->entry;
    }
    
    /**
     * Add element on entry collection
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\[] $value entry
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap
     */
    public function setEntry($value)
    {
        $this->entry[] = $value;
        return $this;
    }
    
    /**
     * Get key
     * 
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }
    
    /**
     * Set key
     * 
     * @param string $value key
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap
     */
    public function setKey($value)
    {
        $this->key = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $value value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap
     */
    public function setValue(\Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $value)
    {
        $this->value = $value;
        return $this;
    }
}
