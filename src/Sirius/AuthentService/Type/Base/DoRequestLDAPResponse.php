<?php
/**
 * Parent model for DoRequestLDAPResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class DoRequestLDAPResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap
     */
    public $out = null;
    
    /**
     * Construct DoRequestLDAPResponse
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap $value out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoRequestLDAPResponse
     */
    public function setOut(\Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
