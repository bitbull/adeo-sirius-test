<?php
/**
 * Parent model for DoAuthentificationToByte
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class DoAuthentificationToByte
{
    
    /**
     * In0
     * 
     * @var string
     */
    public $in0 = null;
    
    /**
     * In1
     * 
     * @var string
     */
    public $in1 = null;
    
    /**
     * In2
     * 
     * @var string
     */
    public $in2 = null;
    
    /**
     * Construct DoAuthentificationToByte
     * 
     * @param string $in2 Value of in2
     * 
     * @return void
     */
    public function __construct($in2 = null)
    {
        $this->in2 = $in2;
    }
    
    /**
     * Get in0
     * 
     * @return string
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param string $value in0
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByte
     */
    public function setIn0($value = null)
    {
        $this->in0 = $value;
        return $this;
    }
    
    /**
     * Get in1
     * 
     * @return string
     */
    public function getIn1()
    {
        return $this->in1;
    }
    
    /**
     * Set in1
     * 
     * @param string $value in1
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByte
     */
    public function setIn1($value = null)
    {
        $this->in1 = $value;
        return $this;
    }
    
    /**
     * Get in2
     * 
     * @return string
     */
    public function getIn2()
    {
        return $this->in2;
    }
    
    /**
     * Set in2
     * 
     * @param string $value in2
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByte
     */
    public function setIn2($value = null)
    {
        $this->in2 = $value;
        return $this;
    }
}
