<?php
/**
 * Parent model for Entry
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class Entry
{
    
    /**
     * Key
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public $key;
    
    /**
     * Value
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public $value;
    
    /**
     * Get key
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public function getKey()
    {
        return $this->key;
    }
    
    /**
     * Set key
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType $value key
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\Entry
     */
    public function setKey(\Adeo\Sirius\AuthentService\Type\Base\AnyType $value)
    {
        $this->key = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType $value value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\Entry
     */
    public function setValue(\Adeo\Sirius\AuthentService\Type\Base\AnyType $value)
    {
        $this->value = $value;
        return $this;
    }
}
