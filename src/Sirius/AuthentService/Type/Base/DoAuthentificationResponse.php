<?php
/**
 * Parent model for DoAuthentificationResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class DoAuthentificationResponse
{
    
    /**
     * Out
     * 
     * @var string2ArrayOfStringMap
     */
    public $out = null;
    
    /**
     * Construct DoAuthentificationResponse
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $value out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationResponse
     */
    public function setOut(\Adeo\Sirius\AuthentService\Type\Base\String2ArrayOfStringMap $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
