<?php
/**
 * Parent model for DoAuthentificationToByteResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class DoAuthentificationToByteResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\Base64Binary
     */
    public $out = null;
    
    /**
     * Construct DoAuthentificationToByteResponse
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\Base64Binary $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\AuthentService\Type\Base\Base64Binary $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\Base64Binary
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\Base64Binary $value out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByteResponse
     */
    public function setOut(\Adeo\Sirius\AuthentService\Type\Base\Base64Binary $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
