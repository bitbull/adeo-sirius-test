<?php
/**
 * Parent model for AnyType2anyTypeMap
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class AnyType2anyTypeMap
{
    
    /**
     * Entry collection
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\[]
     */
    public $entry = array();
    
    /**
     * Key
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public $key;
    
    /**
     * Value
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public $value;
    
    /**
     * Get entry
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\[]
     */
    public function getEntry()
    {
        return $this->entry;
    }
    
    /**
     * Add element on entry collection
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\[] $value entry
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap
     */
    public function setEntry($value)
    {
        $this->entry[] = $value;
        return $this;
    }
    
    /**
     * Get key
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public function getKey()
    {
        return $this->key;
    }
    
    /**
     * Set key
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType $value key
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap
     */
    public function setKey(\Adeo\Sirius\AuthentService\Type\Base\AnyType $value)
    {
        $this->key = $value;
        return $this;
    }
    
    /**
     * Get value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType $value value
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap
     */
    public function setValue(\Adeo\Sirius\AuthentService\Type\Base\AnyType $value)
    {
        $this->value = $value;
        return $this;
    }
}
