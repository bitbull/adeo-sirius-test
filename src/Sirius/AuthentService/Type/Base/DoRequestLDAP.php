<?php
/**
 * Parent model for DoRequestLDAP
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class DoRequestLDAP
{
    
    /**
     * In0
     * 
     * @var string
     */
    public $in0 = null;
    
    /**
     * In1
     * 
     * @var string
     */
    public $in1 = null;
    
    /**
     * Construct DoRequestLDAP
     * 
     * @param string $in1 Value of in1
     * 
     * @return void
     */
    public function __construct($in1 = null)
    {
        $this->in1 = $in1;
    }
    
    /**
     * Get in0
     * 
     * @return string
     */
    public function getIn0()
    {
        return $this->in0;
    }
    
    /**
     * Set in0
     * 
     * @param string $value in0
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoRequestLDAP
     */
    public function setIn0($value = null)
    {
        $this->in0 = $value;
        return $this;
    }
    
    /**
     * Get in1
     * 
     * @return string
     */
    public function getIn1()
    {
        return $this->in1;
    }
    
    /**
     * Set in1
     * 
     * @param string $value in1
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\DoRequestLDAP
     */
    public function setIn1($value = null)
    {
        $this->in1 = $value;
        return $this;
    }
}
