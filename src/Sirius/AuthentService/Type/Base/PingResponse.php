<?php
/**
 * Parent model for PingResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type\Base;

abstract class PingResponse
{
    
    /**
     * Out
     * 
     * @var \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap
     */
    public $out = null;
    
    /**
     * Construct PingResponse
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap $out Value of out
     * 
     * @return void
     */
    public function __construct(\Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap $out = null)
    {
        $this->out = $out;
    }
    
    /**
     * Get out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap
     */
    public function getOut()
    {
        return $this->out;
    }
    
    /**
     * Set out
     * 
     * @param \Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap $value out
     * 
     * @return \Adeo\Sirius\AuthentService\Type\Base\PingResponse
     */
    public function setOut(\Adeo\Sirius\AuthentService\Type\Base\AnyType2anyTypeMap $value = null)
    {
        $this->out = $value;
        return $this;
    }
}
