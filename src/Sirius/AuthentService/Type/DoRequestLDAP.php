<?php
/**
 * Model for DoRequestLDAP
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\DoRequestLDAP as DoRequestLDAPBase;

class DoRequestLDAP
    extends doRequestLDAPBase
{
}
