<?php
/**
 * Model for AuthServExceptionDetail
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\AuthServExceptionDetail as AuthServExceptionDetailBase;

class AuthServExceptionDetail
    extends AuthServExceptionDetailBase
{
}
