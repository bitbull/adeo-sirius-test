<?php
/**
 * Model for String2string2ArrayOfStringMapMap
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\String2string2ArrayOfStringMapMap as String2string2ArrayOfStringMapMapBase;

class String2string2ArrayOfStringMapMap
    extends string2string2ArrayOfStringMapMapBase
{
}
