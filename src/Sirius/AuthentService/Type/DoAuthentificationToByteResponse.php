<?php
/**
 * Model for DoAuthentificationToByteResponse
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByteResponse as DoAuthentificationToByteResponseBase;

class DoAuthentificationToByteResponse
    extends doAuthentificationToByteResponseBase
{
}
