<?php
/**
 * Model for DoAuthentificationToByte
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius\AuthentService\Type;

use \Adeo\Sirius\AuthentService\Type\Base\DoAuthentificationToByte as DoAuthentificationToByteBase;

class DoAuthentificationToByte
    extends doAuthentificationToByteBase
{
}
