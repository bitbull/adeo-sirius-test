<?php
/**
 * Proxy for CustomerListOfValuesApplicationService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\CustomerListOfValuesApplicationService as CustomerListOfValuesApplicationServiceBase;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunities;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristics;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguages;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitles;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfo;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatus;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevels;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntities;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContext;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountries;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentations;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassifications;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByType;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypes;
use \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopes;

class CustomerListOfValuesApplicationService
    extends CustomerListOfValuesApplicationServiceBase
{
    
    /**
     * Method getAllSegmentationsByType
     * 
     * @param string $language Value of language
     * @param int    $type     Value of type
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsByTypeResponse
     */
    public function getAllSegmentationsByType($language, $type)
    {
        $request = new GetAllSegmentationsByType();
        $request->language = $language;
        $request->type = $type;
        return parent::_getAllSegmentationsByType($request);
    }
    
    /**
     * Method getAllUsagePeferenceTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceTypesResponse
     */
    public function getAllUsagePeferenceTypes($language)
    {
        $request = new GetAllUsagePeferenceTypes();
        $request->language = $language;
        return parent::_getAllUsagePeferenceTypes($request);
    }
    
    /**
     * Method getAllCommunities
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunitiesResponse
     */
    public function getAllCommunities($language)
    {
        $request = new GetAllCommunities();
        $request->language = $language;
        return parent::_getAllCommunities($request);
    }
    
    /**
     * Method getAllHousingCharacteristics
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsResponse
     */
    public function getAllHousingCharacteristics($language)
    {
        $request = new GetAllHousingCharacteristics();
        $request->language = $language;
        return parent::_getAllHousingCharacteristics($request);
    }
    
    /**
     * Method getAllLanguages
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLanguagesResponse
     */
    public function getAllLanguages($language)
    {
        $request = new GetAllLanguages();
        $request->language = $language;
        return parent::_getAllLanguages($request);
    }
    
    /**
     * Method getAllTitles
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllTitlesResponse
     */
    public function getAllTitles($language)
    {
        $request = new GetAllTitles();
        $request->language = $language;
        return parent::_getAllTitles($request);
    }
    
    /**
     * Method getAllCountriesWithFormatInfo
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesWithFormatInfoResponse
     */
    public function getAllCountriesWithFormatInfo($language)
    {
        $request = new GetAllCountriesWithFormatInfo();
        $request->language = $language;
        return parent::_getAllCountriesWithFormatInfo($request);
    }
    
    /**
     * Method getAllMaritalStatus
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllMaritalStatusResponse
     */
    public function getAllMaritalStatus($language)
    {
        $request = new GetAllMaritalStatus();
        $request->language = $language;
        return parent::_getAllMaritalStatus($request);
    }
    
    /**
     * Method getAllPhoneTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllPhoneTypesResponse
     */
    public function getAllPhoneTypes($language)
    {
        $request = new GetAllPhoneTypes();
        $request->language = $language;
        return parent::_getAllPhoneTypes($request);
    }
    
    /**
     * Method getAllLeisureLevels
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureLevelsResponse
     */
    public function getAllLeisureLevels($language)
    {
        $request = new GetAllLeisureLevels();
        $request->language = $language;
        return parent::_getAllLeisureLevels($request);
    }
    
    /**
     * Method getAllHousingCharacteristicTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicTypesResponse
     */
    public function getAllHousingCharacteristicTypes($language)
    {
        $request = new GetAllHousingCharacteristicTypes();
        $request->language = $language;
        return parent::_getAllHousingCharacteristicTypes($request);
    }
    
    /**
     * Method getAllBUEntities
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllBUEntitiesResponse
     */
    public function getAllBUEntities($language)
    {
        $request = new GetAllBUEntities();
        $request->language = $language;
        return parent::_getAllBUEntities($request);
    }
    
    /**
     * Method getAllAddressTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressTypesResponse
     */
    public function getAllAddressTypes($language)
    {
        $request = new GetAllAddressTypes();
        $request->language = $language;
        return parent::_getAllAddressTypes($request);
    }
    
    /**
     * Method getAllUsagePeferenceContext
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllUsagePeferenceContextResponse
     */
    public function getAllUsagePeferenceContext($language)
    {
        $request = new GetAllUsagePeferenceContext();
        $request->language = $language;
        return parent::_getAllUsagePeferenceContext($request);
    }
    
    /**
     * Method getAllLeisureTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllLeisureTypesResponse
     */
    public function getAllLeisureTypes($language)
    {
        $request = new GetAllLeisureTypes();
        $request->language = $language;
        return parent::_getAllLeisureTypes($request);
    }
    
    /**
     * Method getAllCountries
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCountriesResponse
     */
    public function getAllCountries($language)
    {
        $request = new GetAllCountries();
        $request->language = $language;
        return parent::_getAllCountries($request);
    }
    
    /**
     * Method getAllCommunicationTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationTypesResponse
     */
    public function getAllCommunicationTypes($language)
    {
        $request = new GetAllCommunicationTypes();
        $request->language = $language;
        return parent::_getAllCommunicationTypes($request);
    }
    
    /**
     * Method getAllClassificationsByType
     * 
     * @param string $language Value of language
     * @param int    $type     Value of type
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsByTypeResponse
     */
    public function getAllClassificationsByType($language, $type)
    {
        $request = new GetAllClassificationsByType();
        $request->language = $language;
        $request->type = $type;
        return parent::_getAllClassificationsByType($request);
    }
    
    /**
     * Method getAllSegmentations
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllSegmentationsResponse
     */
    public function getAllSegmentations($language)
    {
        $request = new GetAllSegmentations();
        $request->language = $language;
        return parent::_getAllSegmentations($request);
    }
    
    /**
     * Method getAllClassifications
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllClassificationsResponse
     */
    public function getAllClassifications($language)
    {
        $request = new GetAllClassifications();
        $request->language = $language;
        return parent::_getAllClassifications($request);
    }
    
    /**
     * Method getAllOptinTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllOptinTypesResponse
     */
    public function getAllOptinTypes($language)
    {
        $request = new GetAllOptinTypes();
        $request->language = $language;
        return parent::_getAllOptinTypes($request);
    }
    
    /**
     * Method getAllAddressExternalCodeTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllAddressExternalCodeTypesResponse
     */
    public function getAllAddressExternalCodeTypes($language)
    {
        $request = new GetAllAddressExternalCodeTypes();
        $request->language = $language;
        return parent::_getAllAddressExternalCodeTypes($request);
    }
    
    /**
     * Method getAllHousingCharacteristicsByType
     * 
     * @param string $language Value of language
     * @param int    $type     Value of type
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllHousingCharacteristicsByTypeResponse
     */
    public function getAllHousingCharacteristicsByType($language, $type)
    {
        $request = new GetAllHousingCharacteristicsByType();
        $request->language = $language;
        $request->type = $type;
        return parent::_getAllHousingCharacteristicsByType($request);
    }
    
    /**
     * Method getAllExternalIdentifierTypes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllExternalIdentifierTypesResponse
     */
    public function getAllExternalIdentifierTypes($language)
    {
        $request = new GetAllExternalIdentifierTypes();
        $request->language = $language;
        return parent::_getAllExternalIdentifierTypes($request);
    }
    
    /**
     * Method getAllCommunicationScopes
     * 
     * @param string $language Value of language
     * 
     * @return \Adeo\Sirius\CustomerListOfValuesApplicationService\Type\GetAllCommunicationScopesResponse
     */
    public function getAllCommunicationScopes($language)
    {
        $request = new GetAllCommunicationScopes();
        $request->language = $language;
        return parent::_getAllCommunicationScopes($request);
    }
}
