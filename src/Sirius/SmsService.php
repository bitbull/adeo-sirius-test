<?php
/**
 * Proxy for SmsService service
 * 
 * @author    David Buros <david.buros@smile.fr>
 * @copyright 2015 Smile
 * @licence   Apache License Version 2.0
 */

namespace Adeo\Sirius;

use \Adeo\Sirius\Base\SmsService as SmsServiceBase;
use \Adeo\Sirius\SmsService\Type\SendSMSRequest;

class SmsService
    extends SmsServiceBase
{
    
    /**
     * Method sendSMS
     *
     * @param \Adeo\Sirius\SmsService\Type\SendSMSRequest $parameters Value of parameters
     * 
     * @return mixed
     */
    public function sendSMS(SendSMSRequest $parameters)
    {
        $response = parent::_sendSMS($parameters);
        return $response;
    }
}
